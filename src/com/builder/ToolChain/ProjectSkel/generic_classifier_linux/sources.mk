################################################################################
# Automatically-generated file. Do not edit!
################################################################################

INO_SRCS := 
ASM_SRCS := 
O_UPPER_SRCS := 
CPP_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
ELF_SRCS := 
C_UPPER_SRCS := 
CXX_SRCS := 
C++_SRCS := 
PDE_SRCS := 
CC_SRCS := 
C_SRCS := 
C_UPPER_DEPS := 
PDE_DEPS := 
C_DEPS := 
AR := 
CC_DEPS := 
AR_OBJ := 
C++_DEPS := 
LINK_OBJ := 
CXX_DEPS := 
ASM_DEPS := 
HEX := 
INO_DEPS := 
SIZEDUMMY := 
S_UPPER_DEPS := 
ELF := 
CPP_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
API/initialization \
API/level0/driver \
API/level1 \
API/level1/chip \
API/level1/decoder \
API/level1/protocols/esp8266 \
API/level2 \
API/level2/Items \
API/level2/Parameters \
API/level2/status \
API/util \
DRIVER/ESP8266 \
EXT_OPERATION \
IOTMAT \
IOTMAT/Speaker \
MCU_UTILITY \
classifier \
core/core \
core/core/libb64 \
. \
level2_autorange \
libraries/Adafruit_GFX_Library \
libraries/Adafruit_ST7735_and_ST7789_Library \
libraries/FS/src \
libraries/Preferences/src \
libraries/SPI/src \
libraries/SPIFFS/src \
libraries/WiFi/src \
libraries/json-streaming-parser \

