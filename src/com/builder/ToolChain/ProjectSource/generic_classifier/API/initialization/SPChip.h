/*
 * SPChip.h
 *
 *  Created on: 19 mag 2018
 *      Author: Luca Gerevini
 */

#ifndef INITIALIZATION_SPCHIP_H_
#define INITIALIZATION_SPCHIP_H_

#include "../util/printMCU.h"
#include "configurations.h"
#include "../util/stringHandler.h"

#define CHIP_TYPE_UNICAST  			0
#define CHIP_TYPE_BROADCAST  		1
#define CHIP_TYPE_BROADCAST_SLOW  	2
#define CHIP_TYPE_MULTICAST  		3

#define NO_ADDRESS  				0
#define SHORT_ADDRESS 				1
#define FULL_ADDRESS  				2

extern char ADDRESSING_TYPES[3][13];

//#define SPI  						0
#define I2C  						1
#define SENSIBUS  					2
#define BUSPIRATE  					3

extern char PROTOCOL_NAMES[4][10];



//includere vari oggetti SPFamily/SPCLusted etc....

//typedef struct{
//	SPFamily* family;
//	//char familyID[FAMILYID_MAXLEN];
//	char i2cAddress[I2CADDRESS_MAXLEN]; /*0x01 (vedere se stringa o uint8)*/
//	char serialNumber[CHIPSERIALNUMBER_MAXLEN];
//	char oscTrimOverride[OSCTRIM_MAXLEN];
//	SPSensingElementOnChip* spSensingElementOnChipList;
//
//	char addressTokens[TOKENS_NUMBER_SENSIBUS][TOKENS_ADDRESS_MAXLEN]; //dimensione? da 6 byte 12 +1 terminatore
//	char addressReadyToSend[ADDRESSINGTYPENAME_MAXLEN]; //dimensione?
//	int chip_type;
//	sp_bool lastVbandGap;
//	sp_bool active;
//}SPChip;

void setSerialNumber(SPChip* spChip, char* serialNumber);
void getShortAddress(SPChip* spChip, char* output);
void getAddress(SPChip* spChip, char* output, int commMod, char* protocolName);
sp_bool SNVerifier(SPChip* spChip, char* ID, int sn_len);


#endif /* INITIALIZATION_SPCHIP_H_ */
