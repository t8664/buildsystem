/*
 * Settings.h
 *
 *  Created on: 10/gen/2017
 *      Author: Marco
 */

#ifndef SETTINGS_H_
#define SETTINGS_H_


#define SETTINGS_CONTACTS				"TWO"
#define SETTINGS_INGAIN					"40"
#define SETTINGS_HARMONIC				"FIRST_HARMONIC"
#define SETTINGS_MODEVI					"VOUT_IIN"
#define SETTINGS_FREQUENCY				 78125
#define SETTINGS_DCBIAS					 0
#define SETTINGS_OUTGAIN				"7"
#define SETTINGS_INPORT					"PORT_HP"
#define SETTINGS_OUTPORT				"PORT_HP"
#define SETTINGS_MEASURE				"CAPACITANCE"
#define SETTINGS_RSENSE 				"500"
#define SETTINGS_FILTER					"64"
#define SETTINGS_FILTER_LENGTH			 64
#define SETTINGS_FILTER_MEASURES		 1
#define SETTINGS_PHASESHIFTMODE			"Quadrants"
#define SETTINGS_PHASESHIFT				 0
#define SETTINGS_IQ					    "IN_PHASE"
#define SETTINGS_ADC_NDATA_INT           1
#define SETTINGS_ADC_NDATA_BYTE          2




#endif /* SETTINGS_H_ */
