/*
 * config.h
 *
 *  Created on: 01 dic 2018
 *      Author: Luca
 */

#ifndef INITIALIZATION_CONFIG_H_
#define INITIALIZATION_CONFIG_H_

#include "../level1/SPProtocol.h"


#define VCC_MODE_NAME_RATIOMETRIC "RATIOMETRIC"

#define VCC_NORMAL 			   0
#define VCC_MODE_RATIOMETRIC   1

#define VCC_DEFAULT		    3300

#define VCC_Th1				1800
#define VCC_Th2				2500
#define VCC_Th3				3300

#define VREF_1			  	1300
#define VREF_2 			 	2000
#define VREF_3  			2800

typedef struct SPConfiguration SPConfiguration;

struct SPConfiguration{
	char driverName[PROTOCOLNAME_MAXLEN];/*{"SPI","I2C","SENSIBUS"}*/
	char addressingType[ADDRESSINGTYPENAME_MAXLEN];/*{"No-Address","Short-Address","Full-Address"}*/
	SPCluster* cluster;
	SPProtocol* protocol;
	int VREF; //da impostare con setVCC
	int VCC_MODE;
	int VCC;
	char description[DESCRIPTION_MAXLEN]; //lunghezza massima?
	/*Optional Parameters for API*/
	SPConfigurationDriver driver;
	char host_controller[HOST_CONTROLLER_NAME_MAXLEN]; /*Android - CC2541 ...*/
	char api_owner[API_OWNER_NAME_MAXLEN]; /*Android - CC2541 ..*/
	char mcu[MCUNAME_MAXLEN]; /*cc2541..*/
	char vccStringValue[12]; //max len RATIOMETRIC

	SPSensingElementOnChip* (*searchSPSensingElementOnChip)(SPConfiguration*, char*);
	void (*setVCC)(SPConfiguration*, char* );
};

SPSensingElementOnChip* searchSeOnChip(SPConfiguration* configuration, char* sensorName);
void setVCC(SPConfiguration* configuration, char* VCCString);

void initSPConfiguration(SPConfiguration* config);

#endif /* INITIALIZATION_CONFIG_H_ */
