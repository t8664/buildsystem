/*
 * configurations.h
 *
 *  Created on: 16/dic/2016
 *      Author: Marco
 */

#ifndef CONFIGURATIONS_H_
#define CONFIGURATIONS_H_

#include "../util/types.h"
#include "../util/SPBool.h"
#include <string.h>
#include <stdio.h>
#include "../../xml.h"


#define SYS_CLOCK			10000000
#define OSC_TRIM			0x09

#define NUM_OF_REGISTERS	39
//#define NUM_OF_CHIPS		10  //remember to update MsgSENSIBUS.h DATA_RCVD_MAXLEN value
//#define NUM_SEON_CHIP		16
//#define NUM_BUFFER 		 	10



/*!SPRegister is a struct which contain information for a SENSIPLUS chip register.
 * char registerName[REGISTERNAME_MAXLEN]:  C string containing the name of the register.
 * uint8 hex : register value in hexadecimal format.
 */
#define REGISTERNAME_MAXLEN		16



typedef struct{
	char registerName[REGISTERNAME_MAXLEN];
	uint8 hex;
}SPRegister;


extern SPRegister registersList[NUM_OF_REGISTERS];

uint8 searchRegisterValue(char* name);


#define SENSORS_NUMBER					4
#define SENSINGELEMENTNAME_MAXLEN		35
#define RSENSE_MAXLEN					6
#define INGAIN_MAXLEN					3
#define OUTGAIN_MAXLEN					2
#define CONTACTS_MAXLEN					5
#define HARMONIC_MAXLEN					16
#define MODEVI_MAXLEN					9
#define MEASURETECNIQUE_MAXLEN			10
#define MEASURETYPE_MAXLEN				12
#define FILTER_MAXLEN					4
#define PHASESHIFTMODE_MAXLEN			10
#define IQ_MAXLEN						15
#define INPORTADC_MAXLEN				4
#define MEASUREUNIT_MAXLEN				5
#define MEASURE_TYPE					4
#define ANALYTENAME_MAXLEN				15
#define ANALYTEMEASUREUNIT_MAXLEN		6
#define PROTOCOLNAME_MAXLEN				9
#define ADDRESSINGTYPENAME_MAXLEN		16
#define CLUSTERID_MAXLEN		    	9 //2BYTE
#define FAMILYNAME_MAXLEN				25
#define FAMILYID_MAXLEN					5
#define DRIVERNAME_MAXLEN 				10
#define HOST_CONTROLLER_NAME_MAXLEN		10
#define API_OWNER_NAME_MAXLEN			10
#define MCUNAME_MAXLEN					10
#define PORTLABEL_MAXLEN				20
#define PORTVALUE_MAXLEN				6
#define CHIPSERIALNUMBER_MAXLEN			15
#define OSCTRIM_MAXLEN					5
#define BROADCAST_MAXLEN 				13
#define MULTICASTDEFAULT_MAXLEN 		13
#define HWVERSION_MAXLEN				5
#define I2CADDRESS_MAXLEN				5
#define MULTICASTCLUSTER_MAXLEN			13
#define PARAM_MAXLEN 					15
#define DESCRIPTION_MAXLEN				15
#define MEASURE_TYPES_MAX_DIM 			11
#define MEASURE_TYPES_MAX_LEN 			10
#define HEX_BYTE_TO_STRING				3
#define TOKENS_NUMBER_SENSIBUS			5
#define TOKENS_ADDRESS_MAXLEN			13


typedef struct{
	char name[SENSINGELEMENTNAME_MAXLEN];/*"ONCHIP_TEMPERATURE","OFFCHIP_VOC","OFFCHIP_BEND"*/
	//eis param
	char rsense[RSENSE_MAXLEN];/*{"50000","5000","500","50"}*/
	char ingain[INGAIN_MAXLEN];/*{"1","12","20","40"}*/
	char outgain[OUTGAIN_MAXLEN];/*{"0","1","2","3","4","5","6","7"}*/
	char contacts[CONTACTS_MAXLEN];/*{"TWO","FOUR"}*/
	sp_double frequency;/**/
	char harmonic[HARMONIC_MAXLEN];/*{"FIRST_HARMONIC","SECOND_HARMONIC","THIRD_HARMONIC"}*/
	char modeVI[MODEVI_MAXLEN];/*{"VOUT_VIN","VOUT_IIN","IOUT_VIN","IOUT_IIN"}*/
	char measureTecnique[MEASURETECNIQUE_MAXLEN];/*{EIS,DIRECT...}*/
	char measureType[MEASURETYPE_MAXLEN];/*{"IN-PHASE", "QUADRATURE","MODULE", "PHASE", "RESISTANCE", "CAPACITANCE", "INDUCTANCE"}*/
	char filter[FILTER_MAXLEN];/*{"1", "4", "8", "16", "32", "64"};*/
	char phaseShiftMode[PHASESHIFTMODE_MAXLEN];/*{"Quadrants", "Coarse", "Fine"};*/
	int phaseShift;
	char iq[IQ_MAXLEN];/*{"IN_PHASE", "QUADRATURE", "ANTI_PHASE", "ANT_QUADRATURE"};*/
	sp_double rangeMin;/**/
	sp_double rangeMax;/**/
	sp_double defaultAlarmThreshold;/**/
	sp_double multipier;/**/
	int dcBiasN;/*[-32, 31]*/
	int dcBiasP;/*[-2048, 2047]*/

	//adc param
	int conversionRate;/*conversion Rate for SENSIPLUS ADC*/
	int NData;/*Number of integer to read in FIFO*/
	char inportADC[INPORTADC_MAXLEN]; /*{"IA", "MUX", "DAC", "PPR"}*/
	char measureUnit[MEASUREUNIT_MAXLEN];/**/

}SPSensingElement;

typedef struct{
	char portLabel[PORTLABEL_MAXLEN];
	char portValue[PORTVALUE_MAXLEN];
	sp_bool isInternal;
	sp_bool noPort;
}SPPort;

typedef struct{
	SPSensingElement* spSensingElement;
	//char sensingElementID[SENSORNAME_MAXLEN]; //corretto mettere SENSORNAME_MAXLEN?
	//char sensingElementName[SENSORNAME_MAXLEN];
	//aggiungere ID/NOME
	//char sensingElementPort[PORT_MAXLEN];/*{"PORT0", "PORT1", "PORT2", "PORT3", "PORT4", "PORT5", "PORT6",
	//"PORT7", "PORT8", "PORT9", "PORT10", "PORT11", "PORT_HP", "PORT_EXT1", "PORT_EXT2", "PORT_EXT3"};*/
	char id[SENSINGELEMENTNAME_MAXLEN];//prima era SENSINGELEMENTNAME_MAXLEN
	SPPort* port;
}SPSensingElementOnFamily;

typedef struct{
	/**Linear Transformation: Y=X*m+n*/
	sp_double m;
	sp_double n;
	sp_double threshold;
}SPCalibrationParameters;

typedef struct{
	//char sensingElementID[SENSINGELEMENTNAME_MAXLEN];
	//sp_bool autoScaleACtive; //controllare se corretto
	SPSensingElementOnFamily* spSensingElementOnFamily;
	SPCalibrationParameters spCalibrationParameters;
}SPSensingElementOnChip;

typedef struct{
	char name[ANALYTENAME_MAXLEN];
	char measureUnit[ANALYTEMEASUREUNIT_MAXLEN];
}SPAnalyte;

typedef struct{
	char name[FAMILYNAME_MAXLEN];/*MEASURING_INSTRUMENT.. VOC...TIC*/
	char id[FAMILYID_MAXLEN];
	//char measureType[MEASURE_TYPE];/*{EIS, POT, ULTRASOUND, ENERGY_SPECTROSCOPY}*/
	char hwVersion[HWVERSION_MAXLEN];/*5*/
	char oscTrim[OSCTRIM_MAXLEN]; /*0x06 (vedere se stringa o uint8)*/
	char broadcastAddress[BROADCAST_MAXLEN]; /*0x0000000001(vedere se stringa o uint8)*/
	char broadcastSlowAddress[BROADCAST_MAXLEN]; /*0x0000000000(vedere se stringa o uint8)*/
	char multicastDefaultAddress[MULTICASTDEFAULT_MAXLEN]; /*0x0000000002(vedere se stringa o uint8)*/
	int sysClock;
	char measureTypesList[HEX_BYTE_TO_STRING][MEASURE_TYPES_MAX_DIM]; //Da rivedere(10 10)
	SPAnalyte* analyteList;
	SPSensingElementOnFamily* spSensingElementOnFamilyList;

	int measureTypeDim;
}SPFamily;

/*CHIP*/
typedef struct{
	SPFamily* family;
	//char familyID[FAMILYID_MAXLEN];
	char i2cAddress[I2CADDRESS_MAXLEN]; /*0x01 (vedere se stringa o uint8)*/
	char serialNumber[CHIPSERIALNUMBER_MAXLEN];
	char oscTrimOverride[OSCTRIM_MAXLEN];
	SPSensingElementOnChip* spSensingElementOnChipList;

	char addressTokens[TOKENS_NUMBER_SENSIBUS][TOKENS_ADDRESS_MAXLEN]; //dimensione? da 6 byte 12 +1 terminatore
	char addressReadyToSend[ADDRESSINGTYPENAME_MAXLEN]; //dimensione?
	int chip_type;

}SPChip;

/*CLUSTER*/
typedef struct{
	char clusterID[CLUSTERID_MAXLEN];
	char multicastAddress[MULTICASTCLUSTER_MAXLEN]; /*0x0000000011(vedere se stringa o uint8)*/
	char broadcastAddress[BROADCAST_MAXLEN]; //aggiunto
	char broadcastSlowAddress[BROADCAST_MAXLEN]; //aggiunto
	char broadcastI2CAddress[I2CADDRESS_MAXLEN]; //aggiunto
	SPChip* multicastChip; //aggiunto
	SPChip* broadcastChip; //aggiunto
	SPChip* broadcastSlowChip; //aggiunto
	SPChip* chipList;
	short int dimChipList;
	//SPChip* chipList; //basta il puntatore per definire un array di chip
	SPFamily* familyOfChips; //aggiunto
}SPCluster;

typedef struct{
	char name[DRIVERNAME_MAXLEN]; /*BLE-USB ..*/
	char param1[PARAM_MAXLEN]; //15
	char param2[PARAM_MAXLEN]; //15
}SPConfigurationDriver;

/*
typedef struct{
	char clusterID[CLUSTERID_MAXLEN];
	char multicastCluster[MULTICASTCLUSTER_MAXLEN]; //lunghezza massima?
	SPChip* chipList;
}SPInstalledChips;
 */

void printSPSensorCalibrationParameters(SPCalibrationParameters* calibrationParams);
void printSPAnalyte(SPAnalyte* analyte);
void printSPSensingElementOnFamily(SPSensingElementOnFamily* element);
void printSPSensingElementOnChip(SPSensingElementOnChip* element);
void printSPFamily(SPFamily* family);
void printSPChip(SPChip* chip);
void printSPChipCluster(SPCluster* cluster);
void initRegistersListRUN5();
void initRegistersListRUN7();

//void printSPConfiguration(SPConfiguration* configuration);


#endif /* CONFIGURATIONS_H_ */
