/*
 * SPDriverESP8266.h
 *
 *  Created on: 22 mag 2018
 *      Author: Luca Gerevini
 */

#ifndef LEVEL0_DRIVER_SPDRIVERESP8266_H_
#define LEVEL0_DRIVER_SPDRIVERESP8266_H_

#include "../../util/types.h"

#if defined (ARDUINO)
#include "../../../DRIVER/ESP8266/SendData.h"
#endif

typedef struct{
	//dim : size of buffer (sequence and dataread are buffer of uint8)
	void (*mcuDriver)(uint8, uint8, uint8*, short, uint8*, short, char*);
}SPDriverESP8266;

void sendData(uint8 header, uint8 command, uint8* addressByte, short dimAddress,
		uint8* data, short dimData, char* out);

#endif /* LEVEL0_DRIVER_SPDRIVERESP8266_H_ */
