/*
 * SPProtocol.h
 *
 *  Created on: 07 apr 2018
 *      Author: Luca Gerevini
 */

#ifndef LEVEL1_SPPROTOCOL_H_
#define LEVEL1_SPPROTOCOL_H_
#include "../util/SPBool.h"
#include "../util/SPDimension.h"
#include "protocols/util/SPProtocolOut.h"
#include "decoder/SPDecoder.h"
#include "../level0/driver/SPDriver.h"
#include "../initialization/SPChip.h"

static const char LOG_MESSAGE_PROTOCOL[] = "SPProtocol";

typedef struct {
	void (*sp_send)(SPDriver*, char*, SPDecoderGenericInstruction*, SPChip*, int);
	int (*getADCDelay)();
	//void (*setAddressingType)(char*, int, int, SPProtocol*, SPprotocolOut*);
	SPCluster* spCluster;
	SPDecoder* spDecoder;
	SPDriver* spDriver;
	int addressingMode;
}SPProtocol;

void sendInstruction(SPProtocol* protocol, SPprotocolOut* output, char* mnemonic, SPCluster* cluster);
//void send(char* recValue, SPDecoderGenericInstruction* instruction, SPChip* spChip);
void activeMulticast();
#endif /* LEVEL1_SPPROTOCOL_H_ */
