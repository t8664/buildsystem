/*
 * SPOffChipPracticalSalinity.h
 *
 */

#if !defined(API_LEVEL1_CHIP_SPOFFCHIPPRACTICALSALINITY_H_)
#define API_LEVEL1_CHIP_SPOFFCHIPPRACTICALSALINITY_H_

#include "../../level2/Parameters/SPMeasurementParameterEIS.h"
#define DEFAULT_K_CELL  0.1937f
#define DEFAULT_FILTER  "1"

typedef struct SPOffChipPracticalSalinity SPOffChipPracticalSalinity;

struct SPOffChipPracticalSalinity{

	SPMeasurementParameterEIS* paramInternalEIS;
	double k_cell;
	double a[6];
	double b[6];
	char filter[2];

};

void initSPOffChipPracticalSalinity(SPOffChipPracticalSalinity* this_c, SPMeasurementParameterEIS* paramEIS);

#endif /* API_LEVEL1_CHIP_SPOFFPRACTICALSALINITY_H_ */
