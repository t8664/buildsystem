/*
 * SPOffChipSpecificConductivity.h
 *
 */

#if !defined(API_LEVEL1_CHIP_SPOFFCHIPSPECIFICCONDUCTIVITY_H_)
#define API_LEVEL1_CHIP_SPOFFCHIPSPECIFICCONDUCTIVITY_H_

#include "../../level2/Parameters/SPMeasurementParameterEIS.h"
#define DEFAULT_K_CELL  0.1937f
#define DEFAULT_FILTER  "1"

typedef struct SPOffChipSpecificConductivity SPOffChipSpecificConductivity;

struct SPOffChipSpecificConductivity{

	SPMeasurementParameterEIS* paramInternalEIS;
	double k_cell;
	char filter[2];


};

void initSPOffChipSpecificConductivity(SPOffChipSpecificConductivity* this_c, SPMeasurementParameterEIS* paramEIS);

#endif /* API_LEVEL1_CHIP_SPOFFCHIPSPECIFICCONDUCTIVITY_H_ */
