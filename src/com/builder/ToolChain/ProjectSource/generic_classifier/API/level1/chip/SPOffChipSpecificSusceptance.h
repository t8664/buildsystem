/*
 * SPOffChipSpecificSusceptance.h
 *
 */

#if !defined(API_LEVEL1_CHIP_SPOFFCHIPSPECIFICSUSCEPTANCE_H_)
#define API_LEVEL1_CHIP_SPOFFCHIPSPECIFICSUSCEPTANCE_H_

#include "../../level2/Parameters/SPMeasurementParameterEIS.h"
#define DEFAULT_K_CELL  0.1937f
#define DEFAULT_FILTER  "1"

typedef struct SPOffChipSpecificSusceptance SPOffChipSpecificSusceptance;

struct SPOffChipSpecificSusceptance{

	SPMeasurementParameterEIS* paramInternalEIS;
	double k_cell;
	char filter[2];


};

void initSPOffChipSpecificSusceptance(SPOffChipSpecificSusceptance* this_c, SPMeasurementParameterEIS* paramEIS);

#endif /* API_LEVEL1_CHIP_SPOFFCHIPSPECIFICSUSCEPTANCE_H_ */
