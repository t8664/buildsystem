/*
 * SPDecoder.h
 *
 *  Created on: 29/dic/2016
 *      Author: Marco Ferdinandi
 *      Property: Sensichips s.r.l.
 */

#ifndef SPDECODER_H_
#define SPDECODER_H_
#include "SPDecoderGenericInstruction.h"
#include "../../util/memoryHandler.h"
#include "../../util/stringHandler.h"
#include <stdio.h>
#include <string.h>
#include "../../initialization/configurations.h"
#include "../../util/SPDimension.h"
#include "../../util/printMCU.h"

#define INSTRUCTION 			1
#define REGISTER_ADDRESS		3
#define MULTIPLE_ADDRESS		4
#define DATA_OR_READ_NUMBER		5


//SPDecoder struct contains a list of SPRegister structured variables;
typedef struct{
	void (*instuction2String)(SPDecoderGenericInstruction*, SPChip*);
}SPDecoder;



void decodeSingleInstruction(SPDecoder* spDecoder, SPDecoderGenericInstruction* spdecInstr, char* line, SPChip* spChip);
void decode2SPI(SPDecoderGenericInstruction* spdecInstr);
void decodeMultipleInstructions(SPDecoder* spDecoder, SPDecoderGenericInstruction* instructions, char* mnemonic, SPCluster* cluster);


#endif /* SPDECODER_H_ */
