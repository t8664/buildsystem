/*
 * SPDecoderGenericInstruction.h
 *
 *  	Created on: 29/dic/2016
 *      Author: Marco Ferdinandi
 *      Property: Sensichips s.r.l.
 */



#ifndef SPDECODERGENERICINSTRUCTION_H_
#define SPDECODERGENERICINSTRUCTION_H_

#include "../../util/types.h"
#include "../../util/SPBool.h"
#include <string.h>
#include <stdio.h>
#include "../../util/memoryHandler.h"


#define MAX_DATI_GENERICINSTRUCTION			7

typedef struct sp_decoder_generic_instruction{
	sp_bool write;
	char* deviceEx;
	char multiple[2];
	uint8 registerEx;
	char dati[MAX_DATI_GENERICINSTRUCTION][MAX_LEN_INSTRUCTION];
	short dim_dati;  //non presente in java
	char instruction[MAX_LEN_INSTRUCTION];
	char decodedInstruction[MAX_LEN_INSTRUCTION];
	int bytesToRead;  //non presente in java

}SPDecoderGenericInstruction;

void init_SPDecoderGenericInstruction(SPDecoderGenericInstruction* dec);
void deallocateSPDecoderGenericInstruction(SPDecoderGenericInstruction* instr);

void printSPDecoderGenericInstruction(SPDecoderGenericInstruction* instr);


#endif /* SPDECODERGENERICINSTRUCTION_H_ */
