/*
 * SPDecoderSensibus.h
 *
 *  Created on: 15 apr 2018
 *      Author: Luca Gerevini
 */

#ifndef LEVEL1_DECODER_SPDECODERSENSIBUS_H_
#define LEVEL1_DECODER_SPDECODERSENSIBUS_H_

#include "SPDecoderGenericInstruction.h"
#include "SPDecoder.h"

//Non so se questa struttura dati sia necessaria.
typedef struct  SPDecoderSensibus{
	SPDecoder* spDecoder; //Classe super
}SPDecoderSensibus;

void instruction2String(SPDecoderGenericInstruction* instruction, SPChip* spChip);

#endif /* LEVEL1_DECODER_SPDECODERSENSIBUS_H_ */
