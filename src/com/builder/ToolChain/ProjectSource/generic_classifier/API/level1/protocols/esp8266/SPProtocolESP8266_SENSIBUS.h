/*
 * SPProtocolESP8266_SENSIBUS.h
 *
 *  Created on: 15 apr 2018
 *      Author: Luca Gerevini
 */

#ifndef LEVEL1_PROTOCOLS_ESP8266_SPPROTOCOLESP8266_SENSIBUS_H_
#define LEVEL1_PROTOCOLS_ESP8266_SPPROTOCOLESP8266_SENSIBUS_H_
#include "../../SPProtocol.h"
#include "../../../util/types.h"
#include "../../../initialization/SPChip.h"
#include "../../../level0/driver/SPDriverESP8266.h"

#if (ARDUINO)
#include "../../../../DRIVER/ESP8266/SendData.h"
#include "../../../util/printMCU.h" //ARDUINO PURPOSE
#else if defined (_LINUX_) || (_WIN32)
#include "../../../DRIVER/Send_Data.h"
#endif

#define MAX_ADDRESS_BYTE	6

typedef struct SPProtocolESP8266_SENSIBUS{
	SPProtocol* spProtocol;
}SPProtocolESP8266_SENSIBUS;

void sp_send(SPDriver* spDriver, char* outString, SPDecoderGenericInstruction* s, SPChip* spChip, int addressingMode);
void send_Data(uint8* out, uint8 protocol, uint8 reg, uint8* address, uint8* data); //Da eliminare
void sendL(uint8* out, uint8* b, int n, long timeout); //Da eliminare

void softTrim();
void setAddressingType(char* output, int modoAttuale, int newAddressType, SPProtocol* protocol, SPprotocolOut* out_port);

int getADCDelayESP8266();

#endif /* LEVEL1_PROTOCOLS_ESP8266_SPPROTOCOLESP8266_SENSIBUS_H_ */
