/*
 * SPProtocolOut.h
 *
 *  Created on: 07 apr 2018
 *      Author: Luca Gerevini
 */

#ifndef LEVEL1_PROTOCOLS_UTIL_SPPROTOCOLOUT_H_
#define LEVEL1_PROTOCOLS_UTIL_SPPROTOCOLOUT_H_

#include "../../../level2/Parameters/SendInstructionInOut.h"

typedef struct{
	char recValues[NUM_OF_CHIPS][MAX_LEN_INSTRUCTION];
	char sentInstructions[NUM_OF_CHIPS][MAX_LEN_INSTRUCTION];
	char mnemonicInstruction[MAX_LEN_INSTRUCTION];
}SPprotocolOut;

#endif /* LEVEL1_PROTOCOLS_UTIL_SPPROTOCOLOUT_H_ */
