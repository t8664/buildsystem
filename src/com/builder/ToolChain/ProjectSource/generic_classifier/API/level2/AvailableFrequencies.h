/*
 * AvailableFrequencies.h
 *
 *  Created on: 09 ago 2018
 *      Author: Luca
 */

#ifndef LEVEL2_AVAILABLEFREQUENCIES_H_
#define LEVEL2_AVAILABLEFREQUENCIES_H_

#include "math.h"

typedef struct{
	int FRD;
	float OSM;
	int DSS_DIVIDER;
	float availableFrequency;

}AvailableFrequencies;

float log_(float x, float base);
void availableFrequencies(float frequencyVal, AvailableFrequencies* frequencyCalc);

#endif /* LEVEL2_AVAILABLEFREQUENCIES_H_ */
