/*
 * SPItems.h
 *
 *  Created on: 03 dic 2019
 *      Author: Luca Gerevini
 */

#ifndef API_LEVEL2_ITEMS_SPITEMS_H_
#define API_LEVEL2_ITEMS_SPITEMS_H_

#include "SPParamItemContacts.h"
#include "SPParamItemFilter.h"
#include "SPParamItemHarmonic.h"
#include "SPParamItemInGain.h"
#include "SPParamItemMeasures.h"
#include "SPParamItemModeVI.h"
#include "SPParamItemOutGain.h"
#include "SPParamItemPhaseShiftMode.h"
#include "SPParamItemPortADC.h"
#include "SPParamItemPOTContacts.h"
#include "SPParamItemPOTType.h"
#include "SPParamItemQI.h"
#include "SPParamItemRSense.h"
#include "SPParamItemTIMER.h"

#endif /* API_LEVEL2_ITEMS_SPITEMS_H_ */
