/*
 * SPParamItem.h
 *
 *  Created on: 10 feb 2019
 *      Author: Luca
 */

#ifndef LEVEL2_ITEMS_SPPARAMITEM_H_
#define LEVEL2_ITEMS_SPPARAMITEM_H_

#include "../../util/SPBool.h"
#include "../Parameters/SPMeasurementParameter.h"

#define LABEL_LENGHT 10

	typedef union u_generic{
		char* string;
		int numeric;
	}generic;

	typedef struct struct_SPParamItem{
		generic label;
		generic value;
	}SPParamItem;


#endif /* LEVEL2_ITEMS_SPPARAMITEM_H_ */
