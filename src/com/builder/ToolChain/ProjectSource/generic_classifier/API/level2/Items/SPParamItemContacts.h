/*
 * SPParamItemContacts.h
 *
 *  Created on: 11 nov 2019
 *      Author: Luca Gerevini
 */

#ifndef API_LEVEL2_ITEMS_SPPARAMITEMCONTACTS_H_
#define API_LEVEL2_ITEMS_SPPARAMITEMCONTACTS_H_

#include "SPParamItem.h"

extern char contactsLabels[2][20];
extern char contactsValues[2][2];

typedef struct struct_SPParamItemContacts SPParamItemContacts;

struct struct_SPParamItemContacts{
	SPParamItem item;
	void (*setContacts)(SPParamItemContacts*, char*);
//	void (*setContacts_1)(SPParamItemContacts*, char*);
	char* (*getContacts)(SPParamItemContacts*);
	char* (*getContacts_1)(SPParamItemContacts*, int); // type -> 0 => restituisce il value || type -> 1 => restituisce la label;
};

void initSPParamItemContacts(SPParamItemContacts* param);




#endif /* API_LEVEL2_ITEMS_SPPARAMITEMCONTACTS_H_ */
