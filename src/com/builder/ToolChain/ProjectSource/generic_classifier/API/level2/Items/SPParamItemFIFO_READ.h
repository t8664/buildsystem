/*
 * SPParamItemFIFO_READ.h
 *
 *  Created on: 12 feb 2019
 *      Author: Luca
 */

#ifndef LEVEL2_PARAMETERS_SPPARAMITEMFIFO_READ_H_
#define LEVEL2_PARAMETERS_SPPARAMITEMFIFO_READ_H_

#define minRUN5   1
#define maxRUN5	 16

typedef struct struct_SPParamItemFIFO_READ{
	int numericalValue;

}SPParamItemFIFO_READ;


#endif /* LEVEL2_PARAMETERS_SPPARAMITEMFIFO_READ_H_ */
