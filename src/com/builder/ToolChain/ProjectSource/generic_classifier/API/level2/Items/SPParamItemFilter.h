/*
 * SPParamItemFilter.h
 *
 *  Created on: 17 feb 2019
 *      Author: Luca
 */

#ifndef LEVEL2_ITEMS_SPPARAMITEMFILTER_H_
#define LEVEL2_ITEMS_SPPARAMITEMFILTER_H_

extern char filterLabelsRUN5[10][20];
extern char filterValuesRUN5[10][6];

typedef struct struct_SPParamItemFilter{
	char labels[6];
	char values[6];

}SPParamItemFilter;



#endif /* LEVEL2_ITEMS_SPPARAMITEMFILTER_H_ */
