/*
 * SPParamItemHarmonic.h
 *
 *  Created on: 22 feb 2019
 *      Author: Luca
 */

#ifndef LEVEL2_ITEMS_SPPARAMITEMHARMONIC_H_
#define LEVEL2_ITEMS_SPPARAMITEMHARMONIC_H_

extern char harmonicLabelsRUN5[3][20];
extern char harmonicValuesRUN5[3][3];

typedef struct struct_SPParamItemHarmonic{
	char labels[16];
	char values[3];

}SPParamItemHarmonic;



#endif /* LEVEL2_ITEMS_SPPARAMITEMHARMONIC_H_ */
