/*
 * SPParamItemInGain.h
 *
 *  Created on: 12 feb 2019
 *      Author: Luca Gerevini
 */

#ifndef LEVEL2_ITEMS_SPPARAMITEMINGAIN_H_
#define LEVEL2_ITEMS_SPPARAMITEMINGAIN_H_

#include "SPParamItem.h"

extern char ingainLabelsRUN5[4][20];
extern char ingainValuesRUN5[4][3];

typedef struct struct_SPParamItemInGain SPParamItemInGain;

struct struct_SPParamItemInGain{
	SPParamItem item;
	void (*setInGain)(SPParamItemInGain*, char*);
	char* (*getInGain)(SPParamItemInGain* );
	char* (*getInGainLabel)(SPParamItemInGain* );
};

void initSPParamItemInGain(SPParamItemInGain* param);

#endif /* LEVEL2_ITEMS_SPPARAMITEMINGAIN_H_ */
