/*
 * SPParamItemMeasures.h
 *
 *  Created on: 22 feb 2019
 *      Author: Luca
 */

#ifndef LEVEL2_ITEMS_SPPARAMITEMMEASURES_H_
#define LEVEL2_ITEMS_SPPARAMITEMMEASURES_H_

#define IN_PHASE		 	0
#define QUADRATURE		 	1
#define CONDUCTANCE  	 	2
#define SUSCEPTANCE  	 	3
#define MODULE  		 	4
#define PHASE  			 	5
#define RESISTANCE  	 	6
#define CAPACITANCE  	 	7
#define INDUCTANCE  	 	8
#define VOLTAGE  		 	9
#define CURRENT  			10
#define DELTA_V_APPLIED  	11
#define CURRENT_APPLIED  	12

extern char unitMeasureLabelsRUN5[13][20];
extern char unitMeasureValuesRUN5[13][4];
extern char measureLabelsRUN5[13][20];
extern int measureValuesRUN5[13];

typedef struct struct_SPParamItemMeasures{
	char measureLabels[16];
	int measureValues;
	char unitMeasureLabels[4];
	char unitMeasureValues[4];

}SPParamItemMeasures;



#endif /* LEVEL2_ITEMS_SPPARAMITEMMEASURES_H_ */
