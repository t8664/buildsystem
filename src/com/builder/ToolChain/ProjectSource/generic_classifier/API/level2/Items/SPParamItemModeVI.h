/*
 * SPParamItemModeVI.h
 *
 *  Created on: 22 feb 2019
 *      Author: Luca
 */

#ifndef LEVEL2_ITEMS_SPPARAMITEMMODEVI_H_
#define LEVEL2_ITEMS_SPPARAMITEMMODEVI_H_

extern char ModeVILabelsRUN5[4][20];
extern char ModeVIValuesRUN5[4][3];

typedef struct struct_SPParamItemModeVI{
	char labels[9];
	char values[3];
} SPParamItemModeVI;


#endif /* LEVEL2_ITEMS_SPPARAMITEMMODEVI_H_ */
