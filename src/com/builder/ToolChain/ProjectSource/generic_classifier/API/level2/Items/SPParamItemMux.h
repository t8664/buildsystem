/*
 * SPParamItemMux.h
 *
 *  Created on: 13 nov 2019
 *      Author: Luca Gerevini
 */

#ifndef API_LEVEL2_ITEMS_SPPARAMITEMMUX_H_
#define API_LEVEL2_ITEMS_SPPARAMITEMMUX_H_

#include "SPParamItem.h"

typedef struct struct_SPParamItemMux SPParamItemMux;

struct struct_SPParamItemMux{
	SPParamItem DF;
	SPParamItem VF;
	SPParamItem DS;
	SPParamItem VS;

	void (*setDF)(SPParamItemMux*, char*);
	void (*setVF)(SPParamItemMux*, char*);
	void (*setDS)(SPParamItemMux*, char*);
	void (*setVS)(SPParamItemMux*, char*);

	char* (*getDF)(SPParamItemMux* );
	char* (*getVF)(SPParamItemMux* );
	char* (*getDS)(SPParamItemMux* );
	char* (*getVS)(SPParamItemMux* );
};

void initSPParamItemMux(SPParamItemMux* param);

#endif /* API_LEVEL2_ITEMS_SPPARAMITEMMUX_H_ */
