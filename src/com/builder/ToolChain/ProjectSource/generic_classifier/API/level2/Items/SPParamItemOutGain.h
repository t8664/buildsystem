/*
 * SPParamItemOutGain.h
 *
 *  Created on: 22 feb 2019
 *      Author: Luca
 */

#ifndef LEVEL2_ITEMS_SPPARAMITEMOUTGAIN_H_
#define LEVEL2_ITEMS_SPPARAMITEMOUTGAIN_H_

extern char outgainLabelsRUN5[8][20];
extern char outgainValuesRUN5[8][4];

typedef struct struct_SPParamItemOutGain{
	char labels[2];
	char values[4];

}SPParamItemOutGain;




#endif /* LEVEL2_ITEMS_SPPARAMITEMOUTGAIN_H_ */
