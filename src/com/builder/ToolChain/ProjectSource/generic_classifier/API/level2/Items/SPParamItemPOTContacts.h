/*
 * SPParamItemPOTContacts.h
 *
 *  Created on: 09 feb 2019
 *      Author: Luca
 */

#ifndef LEVEL2_ITEMS_SPPARAMITEMPOTCONTACTS_H_
#define LEVEL2_ITEMS_SPPARAMITEMPOTCONTACTS_H_

#include "SPParamItem.h"

extern char ContactsLabels[2][20];
extern char ContactsValues[2][2];

typedef struct struct_SPParamItemPOTContacts SPParamItemPOTContacts;

struct struct_SPParamItemPOTContacts{
	SPParamItem item;
	void (*setContacts)(SPParamItemPOTContacts*, char*);
	char* (*getContacts)(SPParamItemPOTContacts* );
};

void init_SPParamItemPOTContacts(SPParamItemPOTContacts* param);

#endif /* LEVEL2_ITEMS_SPPARAMITEMPOTCONTACTS_H_ */
