/*
 * SPParameterPOTType.h
 *
 *  Created on: 10 feb 2019
 *      Author: Luca
 */

#ifndef LEVEL2_ITEMS_SPPARAMITEMPOTTYPE_H_
#define LEVEL2_ITEMS_SPPARAMITEMPOTTYPE_H_

#include "SPParamItem.h"

#define POTENTIOMETRIC 			 			0
#define CURRENT_POT							1
#define LINEAR_SWEEP_VOLTAMMETRY  			2
#define STAIRCASE_VOLTAMMETRY  				3
#define SQUAREWAVE_VOLTAMMETRY  			4
#define NORMAL_PULSE_VOLTAMMETRY 			5
#define DIFFERENTIAL_PULSE_VOLTAMMETRY  	6

extern char TypeLabels[7][20];
extern int TypeValues[7];

//typedef struct struct_SPParamItemPOTType{
//	char labels[20];
//	int values;
//}SPParamItemPOTType;

typedef struct struct_SPParamItemPOTType SPParamItemPOTType;

struct struct_SPParamItemPOTType{
	SPParamItem item;
	void (*setType)(SPParamItemPOTType*, char*);
	int (*getType)(SPParamItemPOTType*);
};

void init_SPParamItemPOTType(SPParamItemPOTType* param);

#endif /* LEVEL2_ITEMS_SPPARAMITEMPOTTYPE_H_ */
