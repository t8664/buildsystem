/*
 * SPParamItemPhaseShiftMode.h
 *
 *  Created on: 24 feb 2019
 *      Author: Luca
 */

#ifndef LEVEL2_ITEMS_SPPARAMITEMPHASESHIFTMODE_H_
#define LEVEL2_ITEMS_SPPARAMITEMPHASESHIFTMODE_H_

#define QUADRANT  	0
#define COARSE  	1
#define FINE  		2

extern char PhaseShiftModeLabelsRUN5[3][20];
extern int PhaseShiftModeValuesRUN5[3];

typedef struct struct_SPParamItemPhaseShiftMode{
	char labels[10];
	int values;
};

#endif /* LEVEL2_ITEMS_SPPARAMITEMPHASESHIFTMODE_H_ */
