/*
 * SPParamItemPortADC.h
 *
 *  Created on: 24 feb 2019
 *      Author: Luca
 */

#ifndef LEVEL2_ITEMS_SPPARAMITEMPORTADC_H_
#define LEVEL2_ITEMS_SPPARAMITEMPORTADC_H_

#define INPORT_IA  "IA"
#define INPORT_MUX  "MUX"
#define INPORT_DAC  "DAC"
#define INPORT_PPR  "PPR"
#define INPORTADC_NUMBER 4

extern char inPortADCLabelsRUN5[INPORTADC_NUMBER][20];
extern char inPortADCValuesRUN5[INPORTADC_NUMBER][3];

typedef struct struct_SPParamItemPortADC{
	char labels[4];
	char values[3];
}SPParamItemPortADC;



#endif /* LEVEL2_ITEMS_SPPARAMITEMPORTADC_H_ */
