/*
 * SPParamItemQI.h
 *
 *  Created on: 24 feb 2019
 *      Author: Luca
 */

#ifndef LEVEL2_ITEMS_SPPARAMITEMQI_H_
#define LEVEL2_ITEMS_SPPARAMITEMQI_H_

extern char I_QLabelsRUN5[4][20];
extern char I_QValuesRUN5[4][3];

typedef struct struct_SPParamItemQI{
	char labels[15];
	char values[3];
}SPParamItemQI;


#endif /* LEVEL2_ITEMS_SPPARAMITEMQI_H_ */
