/*
 * SPParamItemRSense.h
 *
 *  Created on: 10 feb 2019
 *      Author: Luca
 */

#ifndef LEVEL2_ITEMS_SPPARAMITEMRSENSE_H_
#define LEVEL2_ITEMS_SPPARAMITEMRSENSE_H_

#include "SPParamItem.h"

extern char RSenseLabels[4][20];
extern char RSenseValues[4][3];

//typedef struct struct_SPParamItemRsense{
//	char labels[6];
//	char values[3];
//}SPParamItemRSense;

typedef struct struct_SPParamItemRsense SPParamItemRSense;

struct struct_SPParamItemRsense{
	SPParamItem item;
	void (*setRsense)(SPParamItemRSense*, char*);
	char* (*getRsense)(SPParamItemRSense* );
};

void initSPParamItemRsense(SPParamItemRSense* param);

#endif /* LEVEL2_ITEMS_SPPARAMITEMRSENSE_H_ */
