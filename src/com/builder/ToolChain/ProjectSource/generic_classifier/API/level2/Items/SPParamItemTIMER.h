/*
 * SPParamItemTIMER.h
 *
 *  Created on: 24 feb 2019
 *      Author: Luca
 */

#ifndef LEVEL2_ITEMS_SPPARAMITEMTIMER_H_
#define LEVEL2_ITEMS_SPPARAMITEMTIMER_H_

extern char modetimerLabelsRUN5[3][21];
extern char modetimerValuesRUN5[3][21];

typedef struct struct_SPParamItemTIMER{
	char labels[21];
	char values[21];
}SPParamItemTIMER;


#endif /* LEVEL2_ITEMS_SPPARAMITEMTIMER_H_ */
