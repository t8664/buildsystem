/*
 * SPMeasurementOutput.h
 *
 *  Created on: 09 giu 2018
 *      Author: Luca Gerevini
 */

#ifndef LEVEL2_PARAMETERS_SPMEASUREMENTOUTPUT_H_
#define LEVEL2_PARAMETERS_SPMEASUREMENTOUTPUT_H_

#include "../../util/SPBool.h"

typedef struct{
	char out[50];	//definire dimensione
	sp_bool ADCOverflow; //default false
	sp_bool ADCUnderflow; //default false
}SPMeasuramentOutput;

#endif /* LEVEL2_PARAMETERS_SPMEASUREMENTOUTPUT_H_ */
