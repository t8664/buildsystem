/*
 * SPMeasurementParameter.h
 *
 *  Created on: 15/nov/2016
 *
 *  	Property: Sensichips s.r.l.
 *      Author: Marco Ferdinandi
 */

#ifndef SPMEASUREMENTPARAMETER_H_
#define SPMEASUREMENTPARAMETER_H_



#include <string.h>
#include <stdio.h>
#include <errno.h>
#include "../Items/SPParamItemFIFO_READ.h"
#include "../Items/SPParamItemFIFO_DEEP.h"
#include "../Items/SPParamItemFilter.h"
#include "../../util/errorHandler.h"
#include "../../util/SPBool.h"
#include "../../util/types.h"
#include "../../initialization/config.h"



typedef struct SPMeasurementParameter {
	SPConfiguration* spConfiguration;
	SPCluster* temporaryCluster;
	sp_bool sequentialMode;
	sp_bool burstMode;
	SPParamItemFIFO_DEEP spParamItemFIFO_DEEP;
	SPParamItemFIFO_READ spParamItemFIFO_READ;
	SPParamItemFilter spParamItemFilter;
}SPMeasurementParameter;


/**function: searchValue
 * This function search label between keys and return relative value. Values and keys must be
 * a map synchronized on the index.
 *
 * @param keys 			is a set of keys
 * @param values		is a set ov values
 * @param label			label to search in keys
 */
int searchValue(char keys[0][20],int keys_size,int values_length,char label[]);

//int searchIntegerValue(char keys[][20],int keys_size, void* values,int values_length,char label[], char message[]);


/**function: inRange
 * This function verify if the parameter value is in the range [min, max]
 *
 * @param min			double corresponding to the lower bound
 * @param max			double corresponding to the upper bound
 * @param value			integer to be verified
 */
sp_bool inRange(double min, double max, int value);




#endif /* SPMEASUREMENTPARAMETER_H_ */
