/*
 * SPMeasurementParameterADC.h
 *
 *  Created on: 19/dic/2016
 *      Author: Marco Ferdinandi
 *      Property: Sensichips s.r.l.
 */

#ifndef SPMEASUREMENTPARAMETERADC_H_
#define SPMEASUREMENTPARAMETERADC_H_

#include "SPMeasurementParameter.h"
#include "../Items/SPParamItemInGain.h"
#include "../Items/SPParamItemRSense.h"
#include "../Items/SPParamItemPortADC.h"
#include "../Items/SPParamItemMux.h"
#include "SPMeasurementParameterPort.h"

#define N_DATA_MIN					0
#define N_DATA_MAX					16

#define CONVERSION_RATE_MIN			0
#define CONVERSION_RATE_MAX			39000

#define SIZE_INPORT_STRING			3

/*structured variable which is used to contain all parameters relative to the use of the ADC*/
typedef struct struct_SPMeasurementParameterADC{
	//int NData; //default -1 /*Number of integer data to read from SENSIPLUS FIFO*/
	sp_double conversionRate; /*Conversion Rate*/
	char InPort[SIZE_INPORT_STRING]; /*ADC In Port*/
	sp_bool CommandX20;//default -1 /*boolean flag for a particular usage of the SENSIPLUS chip*/
	SPMeasurementParameterPort* spMeasurementParameterPort;
	SPParamItemRSense spParamItemRSense;
	SPParamItemInGain spParamItemInGain;
	SPParamItemMux spParamItemMux;

}SPMeasurementParameterADC;

void setCommandX20(SPMeasurementParameterADC* param, sp_bool val);

int setInPortADC(SPMeasurementParameterADC* param,char* inport);

int setConversionRate(SPMeasurementParameterADC* param, sp_double conversionRate);

int setNData(SPMeasurementParameterADC* param, int NData);
int setFIFO_DEEP(SPMeasurementParameterADC* param, int value);//da valutare dove posizionarla
int setFIFO_READ(SPMeasurementParameterADC* param, int value);//da valutare dove posizionarla (in SPMeasurementParameter.h)

int setSPMeasurementParameterADC(SPMeasurementParameterADC* param, sp_bool CommandX20, char* InPort, int conversionRate, int NData);

sp_bool isValid(SPMeasurementParameterADC* param);

void printSPMeasurementParameterADC(SPMeasurementParameterADC* param);


#endif /* SPMEASUREMENTPARAMETERADC_H_ */
