/*
 * SPMearurementParameterEIS.h
 *
 *  Created on: 16/nov/2016
 *
 *  	Property: Sensichips s.r.l.
 *      Author: Marco Ferdinandi
 */

#ifndef SPMEARUREMENTPARAMETEREIS_H_
#define SPMEARUREMENTPARAMETEREIS_H_


#define MEASURE_LENGTH				9
#define CONTACT_LENGTH				2

#define EIS_NUM_OUTPUT			    10//9
#define POT_NUM_OUTPUT			    2

#define SIZE_STRING_CONTACTS 		2





#include "SPMeasurementParameterQI.h"
#include "../Items/SPParamItemContacts.h"
#include "../Items/SPParamItemMeasures.h"

typedef struct struct_SPMeasurementParameterEIS SPMeasurementParameterEIS;

struct struct_SPMeasurementParameterEIS{
	int measure;
	//char contacts[SIZE_STRING_CONTACTS];
	int dcBiasP;
	int dcBiasN;
	SPParamItemContacts Contacts;
	SPMeasurementParameterQI* QIparam;

	char* (*toString)(SPMeasurementParameterEIS*, char* out);

};



//*********************************FUNCTIONS****************************************

void setMeasure(SPMeasurementParameterEIS* EISparam, char* measure);
void setContacts(SPMeasurementParameterEIS* EISparam, char* contacts);
void setDCBiasP(SPMeasurementParameterEIS* EISparam, int DCBiasP);
void setDCBiasN(SPMeasurementParameterEIS* EISparam, int DCBiasN);


void setSPMeasurementParameterEIS(SPMeasurementParameterEIS* EISparam,char* contacts, char* InGain, char* Harmonic, char* ModeVI, sp_double Frequency, int DCBiasP,
		int DCBiasN, char* OutGain, char* InPort, char* OutPort, char* Measure, char* RSense, char* Filter, char* PhaseShiftMode, int PhaseShift, char* I_Q);


char* printSPMeasurementParameterEIS(SPMeasurementParameterEIS* EISparam, char* out);



#endif /* SPMEARUREMENTPARAMETEREIS_H_ */
