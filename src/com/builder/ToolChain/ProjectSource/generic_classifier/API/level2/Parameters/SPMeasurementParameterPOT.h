/*
 * SPMeasurementParameterPOT.h
 *
 *  Created on: 09 feb 2019
 *      Author: Luca
 */

#ifndef LEVEL2_PARAMETERS_SPMEASUREMENTPARAMETERPOT_H_
#define LEVEL2_PARAMETERS_SPMEASUREMENTPARAMETERPOT_H_

#include "../Items/SPParamItemPOTContacts.h"
#include "../Items/SPParamItemPOTType.h"
#include "../Items/SPParamItemRSense.h"
#include "SPMeasurementParameterADC.h"
#include "../../initialization/configurations.h"

extern int Vref_minus;
extern int Vref_plus;

typedef struct struct_SPMeasurementParameterPOT SPMeasurementParameterPOT;

struct struct_SPMeasurementParameterPOT{
	int initialPotential;//16 bit  per tutti gli int
	int finalPotential;
	int step;
	int pulsePeriod;
	int pulseAmplitude;
	sp_bool alternativeSignal;
	SPParamItemPOTContacts spParamItemPOTContacts;
	SPParamItemPOTType spParamItemPOTType;
	SPParamItemRSense spParamItemRSense;
	SPMeasurementParameterADC* ADCparam;

	void (*setRsense)(SPMeasurementParameterPOT*, char*);
	char* (*getRsense)(SPMeasurementParameterPOT*);
	void (*setContacts)(SPMeasurementParameterPOT*, char*);
	char* (*getContacts)(SPMeasurementParameterPOT*);
	void (*setType)(SPMeasurementParameterPOT*, char*);
	int (*getType)(SPMeasurementParameterPOT*);
	void (*setInGain)(SPMeasurementParameterPOT*, char*);
	char* (*getInGain)(SPMeasurementParameterPOT*);
};

void init_SPMeasurementParameterPOT(SPMeasurementParameterPOT* param, SPConfiguration* config);
int setInitialPotential(SPMeasurementParameterPOT* spParamPOT, int initialPotential);
int setFinalPotential(SPMeasurementParameterPOT* spParamPOT, int finalPotential);
int setStep(SPMeasurementParameterPOT* spParamPOT, int step);
int setPulsePeriod(SPMeasurementParameterPOT* spParamPOT, int pulsePeriod);
int setPulseAmplitude(SPMeasurementParameterPOT* spParamPOT, int pulseAmplitude);
//int setPOTContacts(SPMeasurementParameterPOT* spParamPOT, char* contacts);
//int setType(SPMeasurementParameterPOT* spParamPOT, char* type);
//int setPOTRsense(SPMeasurementParameterPOT* spParamPOT, char* rsense);
//int setPOTInGain(SPParamItemInGain* param, char* ingain);


#endif /* LEVEL2_PARAMETERS_SPMEASUREMENTPARAMETERPOT_H_ */
