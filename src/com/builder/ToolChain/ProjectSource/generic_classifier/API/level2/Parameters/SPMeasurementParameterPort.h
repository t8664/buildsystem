/*
 * SPMeasurementParameterPort.h
 *
 *  Created on: 17/nov/2016
 *  	Property: Sensichips s.r.l.
 *      Author: Marco Ferdinandi
 */

#ifndef SPMEASUREMENTPARAMETERPORT_H_
#define SPMEASUREMENTPARAMETERPORT_H_


#include "SPMeasurementParameter.h"


#define MAX_INTERNAL_INDEX 	12
#define PORT_NUMBER			26
#define SIZE_STRING_PORT 	 6

#define PORT_EXT1_1         16
#define PORT_EXT2_1  		17
#define PORT_EXT3_1  		18
#define PORT_SHORT  		24
#define PORT_OPEN  			25


extern char portLabels[PORT_NUMBER][20];
extern char portValues[PORT_NUMBER][6];
extern char NOPORT[7];

/*! @brief 	struct_SPMeasurementPArameterPort is the 'struct' that is used in the library to encapsulate
 * 			all necessary informations to refer to a SENSIPLUS chip port. The initialization of a SPMeasurementParameterPort
 * 			structured variable MUST be done with the setPort function.
 *
 */
typedef struct struct_SPMeasurementParameterPort{
	//char port[SIZE_STRING_PORT];/*!< Array of char which contains an encoded string associated to the port name.*/
	SPPort* port;
	SPMeasurementParameter* spMeasurementParameter;
}SPMeasurementParameterPort;


/*********************************************************************
 * @fn      setPort
 *
 * @brief  This function allows to set a SENSIPLUS port (external/internal) searching the name (char pointer passed as input parameter)
 * 			in an inner coded hash table with the port names as keys and their encoded versions as values.
 *
 *
 * @param	SPMeasurementParameterPort *param - struct variable which is initialized in this function.
 *
 * @param   char *port - string corresponding to port name. Possible values: "PORT0", "PORT1", "PORT2",
 * 			"PORT3", "PORT4", "PORT5", "PORT6", "PORT7", "PORT8", "PORT9", "PORT10", "PORT11", "PORT_HP",
 * 			"PORT_EXT1", "PORT_EXT2", "PORT_EXT3".
 *
 * @return  int - 0 EXIT_SUCCESS or -1 EXIT_FAIL
 */
void setPort(SPPort* param, char* port);
/**********************************************************************/

sp_bool portIsInternal(SPMeasurementParameterPort* param);

#endif /* SPMEASUREMENTPARAMETERPORT_H_ */
