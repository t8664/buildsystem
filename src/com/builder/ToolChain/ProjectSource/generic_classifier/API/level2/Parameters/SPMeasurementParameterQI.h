/*
 * SPMeasurementParameterQI.h
 *
 *  Created on: 17/nov/2016
 *  	Property: Sensichips s.r.l.
 *      Author: Marco Ferdinandi
 */

#ifndef SPMEASUREMENTPARAMETERQI_H_
#define SPMEASUREMENTPARAMETERQI_H_


#include "SPMeasurementParameterPort.h"
#include "SPMeasurementParameterADC.h"
#include "../Items/SPItems.h"
#include "../../util/printMCU.h"
#include <stddef.h>

//MACRO SPMeasurementParameterQI
#define QUADRANT		0
#define COARSE			1
#define FINE			2

#define QI_NUM_OUTPUT		4

//define array length
#define PHASE_SHIFT_MODE_LENGTH     3
#define FILTER_LENGTH 				8
#define Q_I_LENGTH 					4
#define OUTGAIN_LENGTH 				8
#define MODEVI_LENGTH				4
#define HARMONIC_LENGTH				3
#define INGAIN_LENGTH				4
#define RSENSE_LENGTH				4

#define SIZE_STRING_RSENSE 		3
#define SIZE_STRING_INGAIN		3
#define SIZE_STRING_HARMONIC	3
#define SIZE_STRING_MODEVI		3
#define SIZE_STRING_OUTGAIN		4
#define SIZE_STRING_QI			3
#define SIZE_STRING_FILTER		4
#define SYS_CLOCK  10000000


/*! @brief 	struct_SPMeasurementParameterQI is the 'struct' that is used in the library to encapsulate
 * 			all necessary informations for . The initialization of a SPMeasurementParameterQI variabile MUST
 * 			be done with the setSPMeasurementParameterQI function.
 *
 */

//Aggiornate le grandezze dei campi con le macro di SensingElement in Configuration.h
typedef struct struct_SPMeasurementParameterQI{
	char rsense[SIZE_STRING_RSENSE];/*!<Array of char which contains an encoded string associated to the rsense parameter */
	char ingain[SIZE_STRING_INGAIN];/*!<Array of char which contains an encoded string associated to the ingain parameter */
	char harmonic[SIZE_STRING_HARMONIC];/*!<Array of char which contains an encoded string associated to the harmonic parameter */
	char modeVI[SIZE_STRING_MODEVI];/*!<Array of char which contains an encoded string associated to the modeVI parameter */
	float frequency;/*!<long data type variable which indicates the signal frequency */
	char outgain[SIZE_STRING_OUTGAIN];/*!<Array of char which contains an encoded string associated to the outgain parameter*/
	char q_i[SIZE_STRING_QI];/*!<Array of char which contains an encoded string associated to the I_Q parameter*/
	char filter[SIZE_STRING_FILTER];/*!<Array of char which contains an encoded string associated to the filter parameter*/
	SPMeasurementParameterPort* inport;/*!< SPMeasurementParameterPort variable indicate the INPUT port parameter*/
	SPMeasurementParameterPort* outport;/*!< SPMeasurementParameterPort variable indicate the OUTPUT port parameter*/
	//char inport[SIZE_STRING_PORT];
	//char outport[SIZE_STRING_PORT];
	int phaseShift;/*!< int data type variable which indicate the phaseShiftValue*/
	int phaseShiftMode; /*!< int data type variable which indicate the phaseShiftMode*/
	SPMeasurementParameterADC* spMeasurementParameterADC;

}SPMeasurementParameterQI;


//*********************************FUNCTIONS****************************************

/*! @fn setSPMeasurementParameterQI
 *
 * 	@brief		this function initializes a SPMeasurementParameterQI variable with the parameters passed as input.
 * 				It makes controls on input parameter values and search the encoded  version for the strings
 * 				(which indicate the parameter name value). Searching is made with the use of an hash table where
 * 				keys are  the parameters name and values are the encoded version.
 *
 * 	@param		SPMeasurementParameter QI* param -
 * 	@param		char* filter 		 - Number of read values to use to average the output result. Possible values: {"1", "4", "8", "16", "32", "64"}.
 * 	@param		char* rsense	     - RSense value. Possible values: {"50000","5000","500","50"}.
 * 	@param		char* phaseShiftMode - Possible values:{"Quadrants", "Coarse", "Fine"}.
 * 	@param 		int phaseShift 		 - Possible values: [0-31]
 * 	@param		char* q_i 			 - Possible values: {"IN_PHASE", "QUADRATURE", "ANTI_PHASE", "ANT_QUADRATURE"}.
 * 	@param		char* inport 		 - Possible values: {"PORT0", "PORT1", "PORT2", "PORT3", "PORT4", "PORT5", "PORT6", "PORT7",
 * 										 "PORT8", "PORT9", "PORT10", "PORT11", "PORT_HP", "PORT_EXT1", "PORT_EXT2", "PORT_EXT3"}.
 * 	@param		char* outport 		 - Possible values: {"PORT0", "PORT1", "PORT2", "PORT3", "PORT4", "PORT5", "PORT6", "PORT7",
 * 										 "PORT8", "PORT9", "PORT10", "PORT11", "PORT_HP", "PORT_EXT1", "PORT_EXT2", "PORT_EXT3"}.
 * 	@param		char* harmonic 		 - Possible values: {"FIRST_HARMONIC","SECOND_HARMONIC","THIRD_HARMONIC"}.
 * 	@param		char* modevi 		 - Possible values: {"VOUT_VIN","VOUT_IIN","IOUT_VIN","IOUT_IIN"};
 * 	@param		char* ingain 		 - Possible values: {"1","12","20","40"};
 * 	@param		char* outgain 		 - Possible values: {"0","1","2","3","4","5","6","7"};
 * 	@param		long frequency		 - All values are accepted. Values are elaborated and traduced in possible ones by using an appropriate
 * 									   algorithm. (CURRENT VERSION ACCEPT EACH VALUE WITHOUT ANY CONTROL).
 *
 *
 */
void setSPMeasurementParameterQI(SPMeasurementParameterQI* param, char* filter, char* rsense, char* phaseShiftMode, int phaseShift,
		char* q_i, char* inport, char* outport, char* harmonic, char* modevi, char* ingain, char* outgain, float frequency);


/**function: compare_SPMeasurementParameterQI
 * This function compare two struct SPMeasurementParameterQI variables
 *
 * @param param1		variable 1 o struct type SPMeasurementQI
 * @param param2		variable 2 o struct type SPMeasurementQI
 *
 * return:				true if param1 and param2 are exactly the same,
 * 						false otherwise
 */
sp_bool compare_SPMeasurementParameterQI(SPMeasurementParameterQI param1, SPMeasurementParameterQI param2);

void setPhaseShiftQuadrants(SPMeasurementParameterQI* param,char* phaseShiftMode, int phaseShift, char* q_i);
void setPhaseShiftMode(SPMeasurementParameterQI* param, char* phaseShiftMode);
void setQI(SPMeasurementParameterQI* param, char* q_i);
void setFilter(SPMeasurementParameterQI* param, char* filter);
void setRSense(SPMeasurementParameterQI* param, char* rsense);
void setInPort(SPMeasurementParameterQI* param, char* inport);
void setOutPort(SPMeasurementParameterQI* param, char* outport);
void setHarmonic(SPMeasurementParameterQI* param, char* harmonic);
void setModeVI(SPMeasurementParameterQI* param, char* modevi);
void setInGain(SPMeasurementParameterQI* param, char* ingain);
void setOutGain(SPMeasurementParameterQI* param, char* outgain);




/*Function: 		setFrequency
 *description: 		this function calculate the nearest available frequency to the 'frequency' input
 *					parameter and set it.
 *
 *returns:			integer corresponding to the frequency which has been setted in the SPMeasurementParameterQI.
 *
 */
void setFrequency(SPMeasurementParameterQI* param, float frequency);

long calculateFrequency(float frequency);

void printSPMeasurementParameterQI(SPMeasurementParameterQI* param);

//**********************************************************************************






#endif /* SPMEASUREMENTPARAMETERQI_H_ */
