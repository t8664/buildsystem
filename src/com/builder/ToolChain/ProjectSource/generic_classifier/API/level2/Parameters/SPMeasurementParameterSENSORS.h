/*
 * SPMeasurementParameterSENSORS.h
 *
 *  Created on: 24/gen/2017
 *      Author: Marco Ferdinandi
 *      Property: Sensichips s.r.l.
 */

#ifndef SPMEASUREMENTPARAMETERSENSORS_H_
#define SPMEASUREMENTPARAMETERSENSORS_H_


/*Includes */
#include "SPMeasurementParameterEIS.h"
//#include "SPMeasurementParameterADC.h"
//#include "../../util/errorHandler.h"
#include "../../util/stringHandler.h"

#include "../../level1/chip/SPOffChipSpecificConductivity.h"


#define SENSORS_PORT_NUMBER							4

#define SENSORS_TYPE_NUMBER							9
#define SENSOR_TYPE_CHARNUMBER						35//25

#define SENSOR_POTENTIOMETRIC						0
#define SENSOR_CURRENT								1
#define SENSOR_ONCHIP_TEMPERATURE					2
#define SENSOR_ONCHIP_VOLTAGE						3
#define SENSOR_OFFCHIP_ESLI							4
#define SENSOR_OFFCHIP_BEND							5
#define SENSOR_OFFCHIP_VOC							6
#define SENSOR_OFFCHIP_CARBONDIOXIDE				7
#define SENSOR_OFFCHIP_FIREDETECTOR					8


typedef struct str_spmeasurementparameter_sensor{
	char sensorName[SENSOR_TYPE_CHARNUMBER];
	char filter[SIZE_STRING_FILTER]; //dovrebbe essere il filtro passato come stringa quindi "1 16 32 64 ....256...."
	SPMeasurementParameterEIS* paramInternalEIS;
	SPMeasurementParameterADC* spMeasurementParameterADC;

	char port[SIZE_STRING_PORT];
	char rsense[SIZE_STRING_RSENSE];
	char ingain[SIZE_STRING_INGAIN];
	char sensorTypeName[SENSOR_TYPE_CHARNUMBER];


}SPMeasurementParameterSENSOR;

/*********************************************************************
 * @fn      setSensorType
 *
 * @brief
 *
 *	set the Sensor type name by using a string passed as input
 *
 * @param	SPMeasurementParameterSENSORS *param - struct variable containing all information for a SENSOR
 * @param   char *st - string corresponding to the sensor type name
 *
 * @return  int - 0 EXIT_SUCCESS or -1 EXIT_FAIL
 */
int setSensorType(SPMeasurementParameterSENSOR* param,char* st);



/*********************************************************************
 * @fn      setSensorInGain
 *
 * @brief
 *
 *	set the inGain parameter by using a string passed as input
 *
 * @param	SPMeasurementParameterSENSORS *param - struct variable containing all information for a SENSOR
 * @param   char *ingain - string corresponding to the ingain parameter
 *
 * @return  int - 0 EXIT_SUCCESS or -1 EXIT_FAIL
 */
int setSensorInGain(SPMeasurementParameterSENSOR* param, char* ingain);


/*********************************************************************
 * @fn      setSensorRsense
 *
 * @brief
 *
 *	set the Rsense parameter by using a string passed as input
 *
 * @param	SPMeasurementParameterSENSORS *param - struct variable containing all information for a SENSOR
 * @param   char *rsense - string corresponding to the rsense parameter
 *
 * @return  int - 0 EXIT_SUCCESS or -1 EXIT_FAIL
 */
int setSensorRsense(SPMeasurementParameterSENSOR* param, char* rsense);


/*********************************************************************
 * @fn      setSensorPort
 *
 * @brief
 *
 *	set the Port which is used for the SENSOR parameter by using a string passed as input
 *
 * @param	SPMeasurementParameterSENSORS *param - struct variable containing all information for a SENSOR
 * @param   char *port - string corresponding to the port parameter
 *
 * @return  int - 0 EXIT_SUCCESS or -1 EXIT_FAIL
 */
int setSensorPort(SPMeasurementParameterSENSOR* param, char* port);

/*********************************************************************
 * @fn      setSensorParameter
 *
 * @brief
 *
 *	set the Sensor Parameter struct variable by using all input parameters
 *
 * @param	SPMeasurementParameterSENSORS *param - struct variable containing all information for a SENSOR
 * @param   char *name - string corresponding to the sensor type name
 * @param   char *ingain - string corresponding to the ingain parameter
 * @param   char *rsense - string corresponding to the rsense parameter
 * @param   char *port - string corresponding to the port parameter
 *
 * @return  int - 0 EXIT_SUCCESS or -1 EXIT_FAIL
 */
int setSensorParameter(SPMeasurementParameterSENSOR* param, char* name, char* rsense, char* ingain, char* port);


//int setSensorADC_CommandX20(SPMeasurementParameterSENSOR* param,spbool CommandX20);
//int setSensorADC_InPort(SPMeasurementParameterSENSOR* param,char* InPort);
//int setSensorADC_ConversionRate(SPMeasurementParameterSENSOR* param,long conversionRate);
//int setSensorADC_NData(SPMeasurementParameterSENSOR* param,int NData);
//int setSensorADC(SPMeasurementParameterSENSOR* param,spbool CommandX20, char* InPort, int conversionRate, int NData);
void printSPMeasurementParameterSENSOR(SPMeasurementParameterSENSOR* param);

void init_SPMeasurementParameterSENSORS(SPMeasurementParameterSENSOR* sensorParam, SPMeasurementParameterADC* adcParam, char* sensorName);



#endif /* SPMEASUREMENTPARAMETERSENSORS_H_ */
