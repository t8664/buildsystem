/*
 * SendInstructionInOut.h
 *
 *  Created on: 07/nov/2016
 *      Author: Marco Ferdinandi
 */

#ifndef SENDINSTRUCTIONINOUT_H_
#define SENDINSTRUCTIONINOUT_H_

#include "../../initialization/configurations.h"
#include <stdio.h>

#if defined (__linux__)||(_WIN32)
#include "malloc.h"
#elif __IAR_SYSTEMS_ICC__
#include "OSAL.h"
#endif

#define MAX_LEN_INSTRUCTION		70 //Da ricontrollare
#define MAX_LEN_LOG				25 //DA CONTROLLARE (aggiunti io)
#define MAX_LENEXEPTIONMESSAGE	25 //DA CONTROLLARE (aggiunti io)

typedef struct str_sendInstructionInOut{
	char instructionToSend[MAX_LEN_INSTRUCTION];
	char recValues[NUM_OF_CHIPS][MAX_LEN_INSTRUCTION];
	char exeptionMessage[1];/*Lunghezza massima? NON so se serve*/
	char log[1];/*Lunghezza massima? NON so se serve*/
	SPCluster* temporaryCluster;
}SendInstructionInOut;


void init_SendInstructionInOut(SendInstructionInOut* ref, SPCluster* temporaryCluster);


#endif /* SENDINSTRUCTIONINOUT_H_ */
