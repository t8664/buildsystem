/*
 * SPMeasurement.h
 *
 *  Created on: 07 apr 2018
 *      Author: Luca Gerevini
 */

#ifndef LEVEL2_SPMEASUREMENT_H_
#define LEVEL2_SPMEASUREMENT_H_

#include "../util/SPBool.h"
#include "Parameters/SPMeasurementParameterEIS.h"
#include "Parameters/SendInstructionInOut.h"
#include "../level1/decoder/SPDecoderGenericInstruction.h"
#include "../level1/SPProtocol.h"
#include "../level1/protocols/util/SPProtocolOut.h"

static const char LOG_MESSAGE_MEASUREMENT[] = "SPMeasurement";

typedef struct str_SPMeasurement{
	//SPProtocol* spProtocol;
	//void (*setEIS)(void*, SPMeasurementQIStatus*);
	//void (*getSingleEIS)(void*,sp_double[][EIS_NUM_OUTPUT],SPMeasurementParameterEIS* , SPMeasurementQIStatus*);
	SPProtocol* spProtocol;
	SPConfiguration* spConfiguration;
	SPMeasurementParameter* lastParameter;
	int measureIndexToSave;
}SPMeasurement;


void sendSequence(SPMeasurement* spMeasurement, SendInstructionInOut* ref);

#endif /* LEVEL2_SPMEASUREMENT_H_ */
