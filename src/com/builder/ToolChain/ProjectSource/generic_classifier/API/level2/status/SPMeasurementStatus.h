/*
 * SPMeasurementStatus.h
 *
 *  Created on: 22 feb 2019
 *      Author: Luca
 */

#ifndef LEVEL2_STATUS_SPMEASUREMENTSTATUS_H_
#define LEVEL2_STATUS_SPMEASUREMENTSTATUS_H_

#include "../SPMeasurement.h"

typedef struct str_spmeasurementstatus{
	SPProtocol* spMnemonic;
	SPMeasurementParameter* spParameter;
	SPMeasurement* spMeasurement;
	sp_double lastValue[NUM_OF_CHIPS][EIS_NUM_OUTPUT]; //buffer
	int counter;
	sp_bool firstMeasure;
	int numOutput;
	int dimBuffer;
	int ADCPipelineMaxDim;
	sp_bool pipelineFull;
	int lastPosADC;

}SPMeasurementStatus;

void updateStatus(SPMeasurementStatus* status, sp_double measures[][EIS_NUM_OUTPUT], short int num_of_chips);

#endif /* LEVEL2_STATUS_SPMEASUREMENTSTATUS_H_ */
