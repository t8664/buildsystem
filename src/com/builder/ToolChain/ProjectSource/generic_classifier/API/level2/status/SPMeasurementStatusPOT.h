/*
 * SPMeasurementStatusPOT.h
 *
 *  Created on: 14 feb 2019
 *      Author: Luca
 */

#ifndef LEVEL2_STATUS_SPMEASUREMENTSTATUSPOT_H_
#define LEVEL2_STATUS_SPMEASUREMENTSTATUSPOT_H_

#include "../../util/types.h"
#include "../../util/SPBool.h"
#include "../../initialization/config.h"
#include "../../initialization/configurations.h"
#include "../SPMeasurement.h"
#include "../Parameters/SPMeasurementParameterPOT.h"
#include "SPMeasurementStatus.h"
#include "math.h"
#include "../../util/SPDelay.h"

typedef struct SPMeasurementStatusPOT SPMeasurementStatusPOT;

struct SPMeasurementStatusPOT{
	sp_double FinalPotential;
	sp_double InitialPotential;
	sp_double Step;
	sp_double Stimulus;
	sp_double PulseAmplitude;
	sp_double PulsePeriod;
	sp_double RSense;
	sp_double InGain;
	sp_double kADC;
	int TypeVoltammetry;
	int count;
	int totStepUp;
	int totStepDown;
	sp_bool up;
	sp_bool alternativeSignal;
	SPConfiguration* spConfiguration;
	sp_bool cycleFinished;
	sp_bool maintainStimulus;
	int restartStimulusCounter;
	SPMeasurementStatus status;
};


void initSPMeasurementStatusPOT(SPMeasurementStatusPOT* param, SPMeasurement* spMeasurement, SPConfiguration* spConfiguration,
		SPMeasurementParameterPOT* spParameter, int numOutput, SPProtocol* spProtocol, char* measureLogFile);
sp_bool updateStimulus(SPMeasurementStatusPOT* param);
sp_bool restartStimulus(SPMeasurementStatusPOT* param);
void resetStimulus(SPMeasurementStatusPOT* param);
void readingDelay(SPMeasurementStatusPOT* param);
sp_double normalizeVoltage(SPMeasurementStatusPOT* param, sp_double ADC);
void getStim2Bit(SPMeasurementStatusPOT* param, char* out);

#endif /* LEVEL2_STATUS_SPMEASUREMENTSTATUSPOT_H_ */
