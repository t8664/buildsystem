/*
 * SPMeasurementQIStatus.h
 *
 *  Created on: 23/dic/2016
 *      Author: Marco Ferdinandi
 *      Property: Sensichips s.r.l.
 */

#ifndef SPMEASUREMENTQISTATUS_H_
#define SPMEASUREMENTQISTATUS_H_

#include "../Parameters/SPMeasurementParameterQI.h"
#include "../Parameters/SPMeasurementParameterEIS.h"
#include "../../initialization/configurations.h"
#include "SPMeasurementStatus.h"
#include <stdlib.h>
#include "../../util/memoryHandler.h"
#include "../../util/stringHandler.h"
#include "../../initialization/Settings.h"
#include "../../util/printMCU.h"

#define pigreco 3.1415926


typedef struct str_spmeasurementstatusqi{
	char instructionI_Prepared[MAX_LEN_INSTRUCTION];
	char instructionQ_Prepared[MAX_LEN_INSTRUCTION];
	short measure_type;
	sp_double outGain;
	sp_double inGain;
	sp_double Rsense;
	int conversionRate;
	sp_double k;
	int FRD;
	long frequency;
	float OSM;
	SPMeasurementStatus status;
}SPMeasurementStatusQI;

void setSPMeasurementQIStatus(SPMeasurementStatusQI* qistatus, SPMeasurementParameterQI* qiparam, int FRD, long frequency, short measure_type, int numOutput, float OSM);

#endif /* SPMEASUREMENTQISTATUS_H_ */
