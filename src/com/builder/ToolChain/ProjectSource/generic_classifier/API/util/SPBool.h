/*
 * SPBool.h
 *
 *  Created on: 22/dic/2016
 *      Author: Marco
 */

#ifndef SPBOOL_H_
#define SPBOOL_H_

#if defined (__linux__)||(_WIN32)||(ARDUINO)
   typedef int sp_bool;
	#define true 1
	#define false 0

	#define __bool_true_false_are_defined 1
#endif

//#ifndef __cplusplus
////typedef int sp_bool;
//#define sp_bool _Bool
//#define true 1
//#define false 0
//
//#define __bool_true_false_are_defined 1
//
//#endif /* !__cplusplus */



#endif /* SPBOOL_H_ */
