/*
 * SPDelay.h
 *
 *  Created on: 16 feb 2019
 *      Author: Luca
 */

#ifndef UTIL_SPDELAY_H_
#define UTIL_SPDELAY_H_


void SpDelay(long milliseconds);


#endif /* UTIL_SPDELAY_H_ */
