/*
 * SPInitialization.h
 *
 *  Created on: 07/nov/2016
 *      Author: Marco Ferdinandi
 *      Property: Sensichips s.r.l.
 */

#ifndef SPINITIALIZATION_H_
#define SPINITIALIZATION_H_


struct SPInitialization{
	char hw_version[5];
	char interface_type[5];
	int vcc;
	int sys_clock;
	unsigned char osc_trim;
};

struct SPInitialization spInit;


void SPInitialization_init(char hw_version[], char interface_type[], int vcc, int sys_clock, unsigned char osc_trim);
void SPInitialization_print();




#endif /* SPINITIALIZATION_H_ */
