/*
 * errorHandler.h
 *
 *  Created on: 19/dic/2016
 *      Author: Marco Ferdinandi
 *      Property: Sensichips s.r.l.
 */

#ifndef ERRORHANDLER_H_
#define ERRORHANDLER_H_


#define EXIT_SUCCESS		0
#define EXIT_FAIL			-1


#define PORT_PARAM_ERROR 			0
#define QI_PARAM_ERROR				1
#define ADC_PARAM_ERROR				3
#define EIS_PARAM_ERROR				2



void printError(char* msg, int code);


#endif /* ERRORHANDLER_H_ */
