/*
 * types.h
 *
 *  Created on: 22/dic/2016
 *      Author: Marco
 */

#ifndef TYPES_H_
#define TYPES_H_

#if defined (__linux__)||(_WIN32)||(ARDUINO)
   typedef unsigned char uint8;
#elif defined __IAR_SYSTEMS_ICC__
#include <hal_types.h>
#endif

#if defined (__linux__)||(_WIN32)||(ARDUINO)
   typedef short sp_short;
   typedef double sp_double;
#elif __IAR_SYSTEMS_ICC__
   typedef int sp_short;
   typedef float sp_double;
#endif

#ifndef NULL
#define NULL   ((void *) 0)
#endif

#endif /* TYPES_H_ */
