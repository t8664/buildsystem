#ifndef SENSIBUS_H
#define SENSIBUS_H


#include "MsgSENSIBUS.h"
#include "StatusSENSIBUS.h"

// Definisco le direttive e le Macro:
#define MODE_INPUT(pin)          pinMode(pin,INPUT)
#define MODE_OUTPUT(pin)         pinMode(pin,OUTPUT_OPEN_DRAIN)
#define WRITE_LOW(pin)           digitalWrite(pin, LOW)
#define WRITE_HIGH(pin)          digitalWrite(pin, HIGH)
#define READ(pin)                digitalRead(pin)


#define SENSIBUS_SPEED          	           0xDC
#define SENSIBUS_ADDRESS         			   0xD8

#define SENSIBUS_VALUE_NO_ADDRESS         	   0xDC
#define SENSIBUS_VALUE_SHORT_ADDRESS           0xEC
#define SENSIBUS_VALUE_FULL_ADDRESS        	   0xCC

#define SYS_CLOCK                10000000 


// ******************** Inizio SensiBus.h *******************

// utilizzando le Direttive, individuo il microcontrollore e default pin: STM o Arduino o esp8266
#ifdef ESP8266
#define   DATI          D3
#define   SENSIBUS_VCC  D2

#else // Suitable for ESP32
#define   SENSIBUS_VCC  27
#define   DATI          21//14
#define   DONTCARE      22//12
#define   LED_BUILTIN   32
#endif

// COMMENT THIS DEFINITION FOR CLOCK_TICK
// REMEMBER TO COMPILE WITH 160MHz option for clock_tick
#define DELAYMICROSECONDS

#ifdef DELAYMICROSECONDS

class CommSENSIBUS {
private:
  uint8_t PIN_DATI;
  
  float TS;
  float TS_2_Write;
  float TS_2_Read;
  float TS_4;
  float TS_8;
  float TS_16;
  float DELAY_BETWEEN_BIT;
  float DELAY_BETWEEN_BYTE;
  float DELAY_AFTER_START;

  int TS_START_COM;
  int TS_START_COM_2;
  
	byte read();      
	void write(byte);

public:
  CommSENSIBUS(uint8_t pin, byte newSPEED = 0xFF); // Value for SENSIBUS_SET register
  void  setTS(byte newSPEED);
  bool  startCom();         // Return TRUE if at least one chip is ALIVE
  void  transaction(MsgSENSIBUS &SBFromUSB, StatusSENSIBUS &SBStatus);
  int   getTS_START_COM();
  void  onlyForTest();
};




// ******************** Fine SensiBus.h *******************

#else 

class CommSENSIBUS {
private:
  uint8_t PIN_DATI;

  int CkT;
  int CkT_3_4;
  int CkT_2;
  int CkT_2_read;
  int CkT_4;
  int CkT_RiseTime;

  int CkT_START_COM;
  int CkT_START_COM_2;
  int CkT_START_COM_4;
  int CkT_START_COM_8;

  int TkZero32x;
  
  byte read();      
  void write(byte);

public:
  CommSENSIBUS(uint8_t pin, byte newSPEED = 0xFF); // Value for SENSIBUS_SET register
  void  setSPEED(byte newSPEED);
  byte  adaptSPEED(byte newSPEED);
  bool  startCom();         // Return TRUE if at least one chip is ALIVE
  void  transaction(MsgSENSIBUS &SBFromUSB, StatusSENSIBUS &SBStatus);
  float elapsedStartComResponse();
  void  setTkZero32x(int b);
};

#endif

#endif

// ******************** Inizio Appunti **********************
// Dalla Doc.: If the pin is configured as an INPUT, writing a HIGH value with digitalWrite() 
// will enable an internal 20K pullup resistor.

// Per inviare byte su singolo PIN: https://www.arduino.cc/en/Tutorial/BitMask

// Per leggere lo stato di un PIN: https://www.arduino.cc/en/Reference.PulseInLong
// Oppure da qui: https://github.com/adafruit/DHT-sensor-library/blob/master/DHT.cpp

// OPEN-DRAIN, PULL-UP STM32: http://embeddedsystemengineering.blogspot.it/2016/01/arm-cortex-m3-stm32f103-tutorial-gpio.html
// ******************** Fine Appunti ************************






