/*
 * initDriver.h
 *
 *  Created on: 23 mag 2018
 *      Author: Luca
 */

#ifndef ESP8266_SENDDATA_H_
#define ESP8266_SENDDATA_H_

#include "../../API/util/types.h"

void sendData(uint8 header, uint8 command, uint8* addressByte, short dimAddress,
		uint8* data, short dimData, char* out);



#endif /* ESP8266_SENDDATA_H_ */
