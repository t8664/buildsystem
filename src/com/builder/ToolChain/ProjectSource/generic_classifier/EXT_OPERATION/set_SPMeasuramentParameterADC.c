/*
 * set_SPMeasuramentParameterADC.cpp
 *
 *  Created on: 05 ago 2018
 *      Author: Luca
 */
#include "../EXT_OPERATION/set_SPMeasuramentParameterADC.h"

//#include "Arduino.h"

static void set_spParamItemFIFO_DEEP(uint8 spParamItemFIFO_DEEP);
static void set_InPortADC(uint8 inPort);
static void set_spParamItemFIFO_READ(uint8 spParamItemFIFO_READ);
static void set_spParamItemInGain(uint8 spParamItemInGain);

void set_SPMeasuramentParameterADC(uint8* parameter){

	//leggo 1 byte
	set_InPortADC(parameter[0]);
	set_spParamItemFIFO_DEEP(parameter[0]);

	//leggo 2 byte
	set_spParamItemFIFO_READ(parameter[1]);
	set_spParamItemInGain(parameter[1]);

	//print_f("set_SPMeasuramentParameterADC");

	if(spParamEIS.QIparam->frequency < 50.0 && spParamEIS.QIparam->frequency != 0){
		setConversionRate(&spParamADC,spParamEIS.QIparam->frequency);
	}else{
		sp_double convRate = 50.0;
		setConversionRate(&spParamADC,convRate);
	}
}

void set_spParamItemFIFO_DEEP(uint8 spParamItemFIFO_DEEP){
	uint8 FIFO_DEEP_MASK = 0x0F; //00001111
	int value = spParamItemFIFO_DEEP & FIFO_DEEP_MASK;

	setFIFO_DEEP(&spParamADC, value);
}

void set_InPortADC(uint8 inPort){
	uint8 INPORT_MASK = 0x30; //00110000
	int value = (inPort & INPORT_MASK) >> 4;

	if(value >= 0 && value < INPORTADC_NUMBER){
		setInPortADC(&spParamADC, inPortADCLabelsRUN5[value]);
	} else {
		//Errore
	}
}

void set_spParamItemFIFO_READ(uint8 spParamItemFIFO_READ){
	uint8 FIFO_READ_MASK = 0x0F; //00001111
	int value = spParamItemFIFO_READ & FIFO_READ_MASK;

	setFIFO_READ(&spParamADC, value);
}

void set_spParamItemInGain(uint8 spParamItemInGain){
	uint8 IN_GAIN_MASK = 0xF0; //11110000
	int value = (spParamItemInGain & IN_GAIN_MASK) >> 4;

	char inGainLabels[20];
	strcpy(inGainLabels, ingainLabelsRUN5[value]);
	spParamADC.spParamItemInGain.setInGain(&spParamADC.spParamItemInGain, ingainLabelsRUN5[value]);

	//setInGainItem(&spParamADC.spParamItemInGain, inGainLabels);

}
