/*
 * set_SPMeasuramentParameterADC.h
 *
 *  Created on: 05 ago 2018
 *      Author: Luca
 */

#ifndef SET_SPMEASURAMENTPARAMETERADC_H_
#define SET_SPMEASURAMENTPARAMETERADC_H_

#include "../API/util/types.h"
#include "../variables.h"
#include "../API/level2/Items/SPItems.h"

extern void set_SPMeasuramentParameterADC(uint8* parameter);



#endif /* SET_SPMEASURAMENTPARAMETERADC_H_ */
