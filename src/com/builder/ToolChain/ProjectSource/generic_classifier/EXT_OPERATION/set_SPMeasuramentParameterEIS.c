/*
 * set_sensingElement.cpp
 *
 *  Created on: 03 ago 2018
 *      Author: Luca
 */

#include "../EXT_OPERATION/set_SPMeasuramentParameterEIS.h"

//#include "Arduino.h"

#define PHASESHIFT_MIN		0
#define PHASESHIFT_MAX		31


static void set_Rsense(uint8 rSense);
static void set_InGain(uint8 inGain);
static void set_Harmonic(uint8 harmonic);
static void set_ModeVI(uint8 modeVI);
static void set_OutGain(uint8 outGain);
static void set_InPort(uint8 inPort);
static void set_QI(uint8 qi);
static void set_Contacts(uint8 contacts);
static void set_OutPort(uint8 outPort);
static void set_DCBiasN(uint8 dcBiasN);
static void set_PhaseShiftMode(uint8 phaseShiftMode);
static void set_Measure(uint8 measure);
static void set_PhaseShift(uint8 phaseShift);
static void set_Filter(uint8 filter);
static void set_DCBiasP(uint8* dcBiasP);
static void set_Frequency(uint8* frequency);
static void get_RenewBuffer(uint8 renewBuffer);
static void set_InstanceID(uint8 ID);


void set_SPMeasuramentParamenterEIS(uint8* parameters){

	//leggo il 1 byte
	set_Rsense(parameters[0]);
	set_InGain(parameters[0]);
	set_Harmonic(parameters[0]);
	set_ModeVI(parameters[0]);

	//leggo il 2 byte
	set_OutGain(parameters[1]);
	set_InPort(parameters[1]);

	//leggo il 3 byte
	set_QI(parameters[2]);
	set_Contacts(parameters[2]);
	set_OutPort(parameters[2]);

	//leggo il 4 byte
	set_DCBiasN(parameters[3]);

	//leggo il 5 byte
	set_Measure(parameters[4]);
	set_Filter(parameters[4]);
	get_RenewBuffer(parameters[4]);

	//leggo il 6 byte
	set_PhaseShiftMode(parameters[5]);
	set_PhaseShift(parameters[5]);

	//leggo il 7-8 byte
	set_DCBiasP(&parameters[6]);

	//leggo il 9-10-11-12 byte
	set_Frequency(&parameters[8]);

	//leggo 13 byte
	set_InstanceID(parameters[12]);

}

void set_Rsense(uint8 rSense){
	uint8 RSENSE_MASK = 0xC0;
	int value = (rSense & RSENSE_MASK) >> 6;

	if(value >= 0 && value <= 3){
		//setRSense(spParamEIS.QIparam, rSenseLabel[value]);
		strcpy(spParamEIS.QIparam->rsense, RSenseValues[value]);
		spParamADC.spParamItemRSense.setRsense(&spParamADC.spParamItemRSense, RSenseLabels[value]);
	} else {
		//Errore
	}
}

void set_InGain(uint8 inGain){   // c'� un problema qua??? sembra che il vettore ingainValuesRUN5 venga letto al contrario
	uint8 INGAIN_MASK = 0x30;
	int value = (inGain & INGAIN_MASK) >> 4;

	if(value >= 0 && value <= 3){
		//setInGain(spParamEIS.QIparam, ingainLabels[value]);
		strcpy(spParamEIS.QIparam->ingain, ingainValuesRUN5[value]);
		spParamADC.spParamItemInGain.setInGain(&spParamADC.spParamItemInGain, ingainLabelsRUN5[value]);
	} else {
		//Errore
	}
}

void set_Harmonic(uint8 harmonic){
	uint8 HARMONIC_MASK = 0x0C;
	int value = (harmonic & HARMONIC_MASK) >> 2;

	if(value >= 0 && value <= 2){
		//setHarmonic(spParamEIS.QIparam, harmonicLabels[value-1]);
		strcpy(spParamEIS.QIparam->harmonic, harmonicValuesRUN5[value]);
	} else {
		//Errore
	}
}

void set_ModeVI(uint8 modeVI){
	uint8 MODEVI_MASK = 0x03;
	int value = (modeVI & MODEVI_MASK);

	if(value >= 0 && value <= 3){
		//setModeVI(spParamEIS.QIparam, modeVILabels[value]);
		strcpy(spParamEIS.QIparam->modeVI, ModeVIValuesRUN5[value]);
	} else {
		//Errore
	}
}

void set_OutGain(uint8 outGain){
	uint8 OUTGAIN_MASK = 0xE0;
	int value = (outGain & OUTGAIN_MASK) >> 5;

	if(value >= 0 && value <= 7){
		//setOutGain(spParamEIS.QIparam, outgainLabels[value]);
		strcpy(spParamEIS.QIparam->outgain, outgainValuesRUN5[value]);
	} else {
		//Errore
	}
}

void set_InPort(uint8 inPort){
	uint8 INPORT_MASK = 0x1F; 	    // 00011111
	int value = (inPort & INPORT_MASK);  //restituisce 12 al posto di 11

	if(value >= 0 && value < PORT_NUMBER){
//		print_f("\nPORT: %s\n", portLabels[value]);
		setInPort(spParamEIS.QIparam, portLabels[value]);
		//stampa debug
	//	print_f("%s\n", spParamEIS.QIparam->inport->port->portLabel);
	//	print_f("%s\n", spParamEIS.QIparam->inport->port->isInternal);
		//strcpy(spParamEIS.QIparam->inport->port->portValue, portValues[value]);
		//strcpy(spParamEIS.QIparam->inport->port->portLabel, portLabels[value]);
	} else {
		//Errore
	}
}

void set_QI(uint8 qi){
	uint8 QI_MASK = 0xC0;
	int value = (qi & QI_MASK) >> 6;

	if(value >= 0 && value <= 3){
		strcpy(spParamEIS.QIparam->q_i, I_QValuesRUN5[value]);
	} else {
		//Errore
	}
}

void set_Contacts(uint8 contacts){
	uint8 CONTACTS_MASK = 0x20;
	int value = (contacts & CONTACTS_MASK) >> 5;

	if(value >= 0 && value <= 1){
		//setContacts(&spParamEIS, contactsLabels[value]);
		spParamEIS.Contacts.setContacts(&spParamEIS.Contacts, contactsLabels[value]);
		//strcpy(spParamEIS.contacts, contactsValues[value]);
	} else {
		//Errore
	}
}

void set_OutPort(uint8 outPort){
	uint8 INPORT_MASK = 0x1F;
	int value = (outPort & INPORT_MASK);

	if(value >= 0 && value < PORT_NUMBER){
		setOutPort(spParamEIS.QIparam, portLabels[value]);
		//stampa debug
	//	print_f("%s\n", spParamEIS.QIparam->outport->port->portLabel);
	//	print_f("%s\n", spParamEIS.QIparam->outport->port->isInternal);
		//strcpy(spParamEIS.QIparam->outport->port->portValue, portValues[value]);
		//strcpy(spParamEIS.QIparam->outport->port->portLabel, portLabels[value]);
	} else {
		//Errore
	}
}

void set_DCBiasN(uint8 dcBiasN){
	int value = dcBiasN;
	value = value - 32;
	setDCBiasN(&spParamEIS, value);
}

void set_PhaseShiftMode(uint8 phaseShiftMode){
	uint8 PHASESHIFTMODE_MASK = 0x60;
	int value = (phaseShiftMode & PHASESHIFTMODE_MASK) >> 5;

	if(value >= 0 && value <= 2){
		spParamEIS.QIparam->phaseShiftMode = PhaseShiftModeValuesRUN5[value];
	}
}

void set_Measure(uint8 measure){
	uint8 MEASURE_MASK = 0x78;
	int value = (measure & MEASURE_MASK) >> 3;
	spParamEIS.measure = measureValuesRUN5[value];
	spMeasurementRUNX[instanceID].spMeasurement->measureIndexToSave = measureValuesRUN5[value];
}

void get_RenewBuffer(uint8 renewBuffer){
	uint8 RENEW_BUFFER_MASK = 0x80; //10000000
	int value = (renewBuffer & RENEW_BUFFER_MASK) >> 7;

	if(value){
		for(int i = 0; i < NUM_OF_MEASUREMENT; i++){
			qistatus[i].status.firstMeasure = true;
		}
		//reset chace
		for(int i = 0; i < NUM_OF_INSTRUCTIONS; i++){
			strcpy(chace[i], "\0");

		}
	}

}

void set_PhaseShift(uint8 phaseShift){
	uint8 PHASESHIFT_MASK = 0x0F;
	int value = phaseShift & PHASESHIFT_MASK;

	if( value >= PHASESHIFT_MIN && value <= PHASESHIFT_MAX){
		spParamEIS.QIparam->phaseShift = value;
	} else {
		//Errore
	}
}

void set_Filter(uint8 filter){
	uint8 FILTER_MASK = 0x07;
	int value = filter & FILTER_MASK;

	if(value >= 0 && value <= 7){
		strcpy(spParamEIS.QIparam->filter, filterValuesRUN5[value]);

	} else {
		//Errore
	}
}

void set_DCBiasP(uint8* dcBiasP){
	int value = dcBiasP[0] << 8;
	value = value | dcBiasP[1];
	value = value - 2048;
	setDCBiasP(&spParamEIS, value);
}

void set_Frequency(uint8* frequency){
	union freq{
		float value;
		uint8 appo[4];
	} number;

	number.appo[3] = frequency[0];
	number.appo[2] = frequency[1];
	number.appo[1] = frequency[2];
	number.appo[0] = frequency[3];

	setFrequency(spParamEIS.QIparam, number.value);
}

void set_InstanceID(uint8 ID){
	instanceID = ID;
}

