/*
 * set_sensingElement.h
 *
 *  Created on: 03 ago 2018
 *      Author: Luca
 */

#ifndef SET_SPMEASURAMENTPARAMETEREIS_H_
#define SET_SPMEASURAMENTPARAMETEREIS_H_

#include "../API/util/types.h"
#include "../variables.h"
#include "../API/level2/Items/SPItems.h"
#include "../API/level2/Parameters/SPMeasurementParameterPort.h"

extern void set_SPMeasuramentParamenterEIS(uint8* parameters);


#endif /* SET_SPMEASURAMENTPARAMETEREIS_H_ */
