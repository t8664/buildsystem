/*
 * set_SPMeasuramentParameterPOT.c
 *
 *  Created on: 01 mar 2019
 *      Author: Luca
 */


#include "set_SPMeasuramentParameterPOT.h"

static void set_InitialPotential(uint8* initialPotential);
static void set_FinalPotential(uint8* finalPotential);
static void set_Step(uint8* step);
static void set_PulsePeriod(uint8* pulsePeriod);
static void set_PulseAmplitude(uint8* pulseAmplitude);
static void set_AlternativeSignal(uint8 alternativeSignal);
static void set_Contacts(uint8 contacts);
static void set_RSense(uint rSense);
static void set_PotType(uint8 potType);
static void get_RenewBuffer(uint8 renewBuffer);
static void set_InGain(uint8 inGain);
static void set_Port(uint8 port);
static void set_InstanceID(uint8 ID);

void set_SPMeasuramentParamenterPOT(uint8* parameters){

	//leggo il byte 1-2
	set_InitialPotential(&parameters[0]);

	//leggo il byte 3-4
	set_FinalPotential(&parameters[2]);

	//leggo il byte 5-6
	set_Step(&parameters[4]);

	//leggo il byte 7-8
	set_PulsePeriod(&parameters[6]);

	//leggo il byte 9-10
	set_PulseAmplitude(&parameters[8]);

	//leggo il byte 11
	set_AlternativeSignal(parameters[10]);
	set_Contacts(parameters[10]);
	set_RSense(parameters[10]);
	set_PotType(parameters[10]);
	get_RenewBuffer(parameters[10]);

	//leggo il byte 12
	set_InGain(parameters[11]);
	set_Port(parameters[11]);

	//leggo il byte 13
	set_InstanceID(parameters[12]);
}

void set_InitialPotential(uint8* initialPotential){

	union potential{
		short int value;
		uint8 byte[2];
	} number;

	number.byte[1] = initialPotential[0];
	number.byte[0] = initialPotential[1];

	spParamPOT.initialPotential = number.value;

}

void set_FinalPotential(uint8* finalPotential){

	union potential{
		short int value;
		uint8 byte[2];
	} number;

	number.byte[1] = finalPotential[0];
	number.byte[0] = finalPotential[1];

	spParamPOT.finalPotential = number.value;

}

void set_Step(uint8* step){

	union potStep{
		short int value;
		uint8 byte[2];
	} number;

	number.byte[1] = step[0];
	number.byte[0] = step[1];

	spParamPOT.step = number.value;
}

void set_PulsePeriod(uint8* pulsePeriod){

	union potPeriod{
		short int value;
		uint8 byte[2];
	} number;

	number.byte[1] = pulsePeriod[0];
	number.byte[0] = pulsePeriod[1];

	spParamPOT.pulsePeriod = number.value;

}


void set_PulseAmplitude(uint8* pulseAmplitude){

	union potAmplitude{
		short int value;
		uint8 byte[2];
	} number;

	number.byte[1] = pulseAmplitude[0];
	number.byte[0] = pulseAmplitude[1];

	spParamPOT.pulseAmplitude = number.value;
}


void set_AlternativeSignal(uint8 alternativeSignal){

	uint8 ALTERNATIVE_SIGNAL_MASK = 0x01;	//00000001

	sp_bool value = alternativeSignal & ALTERNATIVE_SIGNAL_MASK;

	spParamPOT.alternativeSignal = value;

}


void set_Contacts(uint8 contacts){

	uint8 CONTACTS_MASK = 0x02;	//00000010

	int value = (contacts & CONTACTS_MASK) >> 1;

	//setPOTContacts(&spParamPOT, ContactsLabels[value]);
	spParamPOT.setContacts(&spParamPOT, ContactsLabels[value]);
}

void set_RSense(uint rSense){

	uint8 RSENSE_MASK = 0x0C;	//00001100

	int value = (rSense & RSENSE_MASK) >> 2;
	//setPOTRsense(&spParamPOT, RSenseLabels[value]);
	spParamPOT.setRsense(&spParamPOT, RSenseLabels[value]);

	 /************************************** TEST ************************************************************/
	//spParamPOT.testSetRsense2 = &testSetRsense2;
	//init_SPParamItemRsense(&spParamPOT.spParamItemRSense2);
	//spParamPOT.setRsense(&spParamPOT, RSenseLabels[value]);
	//spParamPOT.spParamItemRSense2.setRsense(&spParamPOT.spParamItemRSense2, RSenseLabels[value]);
	/*********************************************************************************************************/
}
void set_PotType(uint8 potType){

	uint8 POT_TYPE_MASK = 0x70;	//01110000

	int value = (potType & POT_TYPE_MASK) >> 4;
	spParamPOT.setType(&spParamPOT, TypeLabels[value]);
}

void get_RenewBuffer(uint8 renewBuffer){
	uint8 RENEW_BUFFER_MASK = 0x80; //10000000

	int value = (renewBuffer & RENEW_BUFFER_MASK) >> 7;
	if(value){
		for(int i = 0; i < NUM_OF_MEASUREMENT; i++){
			qistatus[i].status.firstMeasure = true;
		}
		//reset chace
		for(int i = 0; i < NUM_OF_INSTRUCTIONS; i++){
			strcpy(chace[i], "\0");

		}
	}
}

void set_InGain(uint8 inGain){
	uint8 INGAIN_MASK = 0x03;  // 00000011

	int value = (inGain & INGAIN_MASK);
	spParamPOT.setInGain(&spParamPOT, ingainLabelsRUN5[value]);
	//setInGainItem(&spParamPOT.ADCparam->spParamItemInGain, ingainLabelsRUN5[value]);
}

void set_Port(uint8 port){

	uint8 PORT_MASK = 0xFC;  // 11111100

	int value = (port & PORT_MASK) >> 2;

	setPort(spParamPOT.ADCparam->spMeasurementParameterPort->port, portLabels[value]);

}

void set_InstanceID(uint8 ID){
	instanceID = ID;

}
