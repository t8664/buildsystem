/*
 * set_Sensor.cpp
 *
 *  Created on: 02 dic 2018
 *      Author: Luca
 */

#include "set_Sensor.h"


char sensorNAME[20][25] = {"ONCHIP_DARK", "ONCHIP_LIGHT", "ONCHIP_TEMPERATURE", "ONCHIP_VOLTAGE", "ONCHIP_VOC_PORT0",
		"ONCHIP_VOC_PORT1", "ONCHIP_VOC_PORT2", "ONCHIP_VOC_PORT3", "ONCHIP_VOC_PORT4", "ONCHIP_ALUMINUM_OXIDE",
		"ONCHIP_UREA", "ONCHIP_VOC_PORT7", "ONCHIP_VOC_PORT8", "ONCHIP_VOC_PORT9", "ONCHIP_VOC_PORT10", "ONCHIP_POLYMIDE",
		"OFFCHIP_HP_DC", "OFFCHIP_HP_AC", "OFFCHIP_AU_GRAPHENE_EXT3", "OFFCHIP_HUMIDITY"};

static void setPortValue(char* param, char* port);
static void setSensorEIS(SPMeasurementParameterSENSOR* param, SPSensingElement* se, SPSensingElementOnChip* seOnChip);

void set_Sensor1(uint8* param, SPMeasurementParameterSENSOR* sensor){
	uint8 FIRST_MEASURE_MASK = 0x01;

	int sensorNameMask = 31; //00011111
	int sensorCode = param[0] & sensorNameMask;
	char sensorName[24];
	strcpy(sensorName, sensorNAME[sensorCode]);
	strcpy(sensor->sensorName, sensorName);

	instanceID = (param[1] >> 1);

	SPSensingElementOnChip* seOnChip = spConfiguration.searchSPSensingElementOnChip(&spConfiguration, sensorName);
	SPSensingElement* sensingElement = (seOnChip->spSensingElementOnFamily->spSensingElement);

	if(strcmp(sensingElement->measureTecnique, "DIRECT") == 0){
		strcpy(sensor->port, seOnChip->spSensingElementOnFamily->port->portValue);
		spMeasurementRUNX[instanceID].spMeasurement->measureIndexToSave = -1;
		strcpy(sensor->filter, sensingElement->filter);
		sensor->paramInternalEIS = NULL;
		qistatus[instanceID].status.dimBuffer = atoi(sensor->filter);
		//		for(int i = 0; i < NUM_OF_INSTRUCTIONS; i++){
		//			chace[i][0] = '\0';
		//		}
	} else if (strcmp(sensingElement->measureTecnique, "EIS") == 0){
		sensor->paramInternalEIS = &spParamEIS;
		setSensorEIS(sensor, sensingElement, seOnChip);
		int measureIndextToSave = param[0] >> 5;
		spMeasurementRUNX[instanceID].spMeasurement->measureIndexToSave = measureIndextToSave;
		strcpy(sensor->filter, sensingElement->filter);
	}

	if(param[1] & FIRST_MEASURE_MASK){//renewBuffer
		for(int i = 0; i < NUM_OF_MEASUREMENT; i++){
			qistatus[instanceID].status.firstMeasure = true;
		}
		//reset chace
		for(int i = 0; i < NUM_OF_INSTRUCTIONS; i++){
			strcpy(chace[i], "\0");

		}
	}

}

void set_Sensor(uint8* param){
	uint8 FIRST_MEASURE_MASK = 0x01;

	int sensorNameMask = 31; //00011111
	int sensorCode = param[0] & sensorNameMask;  //2 & 31 = 2
	char sensorName[24];
	strcpy(sensorName, sensorNAME[sensorCode]);
	strcpy(spParamSENSOR.sensorName, sensorName);
//	print_f("\nsensorName: %s\n", spParamSENSOR.sensorName);
	instanceID = (param[1] >> 1); //shift di 1 bit a dx

	SPSensingElementOnChip* seOnChip = spConfiguration.searchSPSensingElementOnChip(&spConfiguration, sensorName);
	SPSensingElement* sensingElement = (seOnChip->spSensingElementOnFamily->spSensingElement);
	if(strcmp(sensingElement->measureTecnique, "DIRECT") == 0){
		strcpy(spParamSENSOR.port, seOnChip->spSensingElementOnFamily->port->portValue);
		//debug
//		print_f("\nport: %s\n", spParamSENSOR.port);

		spMeasurementRUNX[instanceID].spMeasurement->measureIndexToSave = -1;
		strcpy(spParamSENSOR.filter, sensingElement->filter);
		//debug
		print_f("\nfilter(set_Sensor): %s\n", spParamSENSOR.filter);

		spParamSENSOR.paramInternalEIS = NULL;
		qistatus[instanceID].status.dimBuffer = atoi(spParamSENSOR.filter);
		//		for(int i = 0; i < NUM_OF_INSTRUCTIONS; i++){
		//			chace[i][0] = '\0';
		//		}
	} else if (strcmp(sensingElement->measureTecnique, "EIS") == 0){
		spParamSENSOR.paramInternalEIS = &spParamEIS;
//		print_f("\nATTENZIONE!!!! Contacts: %s\n", sensingElement->contacts);
//		print_f("\ningain: %s\n", sensingElement->ingain);
//		print_f("\nrsense: %s\n", sensingElement->rsense);
//		print_f("\nspMeasurementParameterPort : %s", seOnChip->spSensingElementOnFamily->port->portLabel);

		setSensorEIS(&spParamSENSOR, sensingElement, seOnChip);
		//print_f("\nInPort: %s\n", spParamSENSOR.paramInternalEIS->QIparam->spMeasurementParameterADC->InPort);
		int measureIndextToSave = param[0] >> 5;
		spMeasurementRUNX[instanceID].spMeasurement->measureIndexToSave = measureIndextToSave;
		strcpy(spParamSENSOR.filter, sensingElement->filter);
	}

	if(param[1] & FIRST_MEASURE_MASK){//renewBuffer
		for(int i = 0; i < NUM_OF_MEASUREMENT; i++){
			qistatus[instanceID].status.firstMeasure = true;
		}

		//reset chace
		for(int i = 0; i < NUM_OF_INSTRUCTIONS; i++){
			strcpy(chace[i], "\0");

		}
	}
	// BUG RUN6 corretto con doppia lettura
	if(strcmp(spConfiguration.cluster->familyOfChips->hwVersion, "RUN5") == 0 ||
			strcmp(spConfiguration.cluster->familyOfChips->hwVersion, "RUN7") == 0) {
		spParamSENSOR.spMeasurementParameterADC->spMeasurementParameterPort->spMeasurementParameter->spParamItemFIFO_DEEP.numericalValue = 1;
		spParamSENSOR.spMeasurementParameterADC->spMeasurementParameterPort->spMeasurementParameter->spParamItemFIFO_READ.numericalValue = 1;
	} else if(strcmp(spConfiguration.cluster->familyOfChips->hwVersion, "RUN6") == 0){
		spParamSENSOR.spMeasurementParameterADC->spMeasurementParameterPort->spMeasurementParameter->spParamItemFIFO_DEEP.numericalValue = 8;
		spParamSENSOR.spMeasurementParameterADC->spMeasurementParameterPort->spMeasurementParameter->spParamItemFIFO_READ.numericalValue = 1;
	}
}

void setSensorEIS(SPMeasurementParameterSENSOR* param, SPSensingElement* se, SPSensingElementOnChip* seOnChip){
	setContacts(param->paramInternalEIS, se->contacts);
	setRSense(param->paramInternalEIS->QIparam, se->rsense);
	setDCBiasP(param->paramInternalEIS, se->dcBiasP);
	setDCBiasN(param->paramInternalEIS, se->dcBiasN);
	setFrequency(param->paramInternalEIS->QIparam, se->frequency);
	setHarmonic(param->paramInternalEIS->QIparam, se->harmonic);
	setOutPort(param->paramInternalEIS->QIparam, seOnChip->spSensingElementOnFamily->port->portLabel);//port11 ==> ONCHIP_POLYMIDE
	setInPort(param->paramInternalEIS->QIparam, seOnChip->spSensingElementOnFamily->port->portLabel);
	setInGain(param->paramInternalEIS->QIparam, se->ingain);
	setOutGain(param->paramInternalEIS->QIparam, se->outgain);
	setModeVI(param->paramInternalEIS->QIparam, se->modeVI);
	setMeasure(param->paramInternalEIS, se->measureType);
	setPhaseShiftQuadrants(param->paramInternalEIS->QIparam, se->phaseShiftMode, se->phaseShift, se->iq);
	setFilter(param->paramInternalEIS->QIparam, se->filter);
}

void setPortValue(char* param, char* port){

	sp_bool portFound = false;

	if(strcmp(port, NOPORT) != 0){
		int i=0;
		while(i<PORT_NUMBER && !portFound){
			if(strcmp(portLabels[i], port) != 0){
				i++;
			}else{
				portFound = true;
			}
		}
		if(portFound){
			strcpy(param, portValues[i]);
		}
	}

	return;
}
