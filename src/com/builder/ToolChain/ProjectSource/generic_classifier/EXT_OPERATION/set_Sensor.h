/*
 * set_Sensor.h
 *
 *  Created on: 02 dic 2018
 *      Author: Luca
 */

#ifndef SET_SENSOR_H_
#define SET_SENSOR_H_

#include "../variables.h"
#include "../API/level2/Parameters/SPMeasurementParameterPort.h"

void set_Sensor(uint8* param);
void set_Sensor1(uint8* param, SPMeasurementParameterSENSOR* sensor);

#endif /* SET_SENSOR_H_ */
