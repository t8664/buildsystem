/*
 * softResetMCU.cpp
 *
 *  Created on: 08 ott 2018
 *      Author: Luca
 */

#include "../EXT_OPERATION/softResetMCU.h"

void softResetMCU(){
	char output[30];
	char speed[5] = "0xFF";

	//Serial.prinln();
	//set_speed(speed); //verificare che 0xFF � la velocit� di default
	instanceID = 0;
	setAddressingType(output, spMeasurement[instanceID].spProtocol->addressingMode, FULL_ADDRESS, spMeasurement[instanceID].spProtocol, &appoProtocol);
	//spCluster.dimChipList = 0;
	for(int i = 0; i < NUM_OF_MEASUREMENT; i++){
		qistatus[i].status.firstMeasure = true;
		qistatus[i].status.counter = 0;
		//spMeasurementRUNX[i].spMeasurement->measureIndexToSave = -1;
		spMeasurementRUNX[i].spMeasurement->measureIndexToSave=-1;
	}

	for(int i = 0; i < NUM_OF_INSTRUCTIONS; i++){
		strcpy(chace[i], "\0");
	}
}



