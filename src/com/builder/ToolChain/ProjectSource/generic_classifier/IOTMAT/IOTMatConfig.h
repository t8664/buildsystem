#ifndef _IOT_MAT_CONFIG_H_
#define _IOT_MAT_CONFIG_H_

//screen
#define TFT_CS     16
#define TFT_RST    9
#define TFT_DC     17
#define TFT_SCLK 5   // set these to be whatever pins you like!
#define TFT_MOSI 23   // set these to be whatever pins you like!
#define TFT_HEIGHT 160
#define TFT_WIDTH  128

#define TEXT_COLOR ST7735_GREEN
#define TEXT_FOCUS ST7735_RED  // is blu
#define TEXT_COLOR_GREY	0x630C
#define TEXT_SIZE 1
#define TIME_SELECTION 2000   //3500

//PIN
#define R_PIN   39
#define L_PIN   35
#define OK_PIN  34
#define MENU_PIN 33
// BEEP PIN
#define SPEAKER_PIN 25
#define TONE_PIN_CHANNEL 0


//#define IOT_MAT_DEBUG
#ifdef IOT_MAT_DEBUG
	#define DEBUG_SERIAL(msg) Serial.println(msg);
#else
	#define DEBUG_SERIAL(msg)
#endif


// +++++++++ LAYOUTS +++++++++++


// ********* RLC  ***********
#define RLC_PRIM_X 30
#define RLC_PRIM_Y 75

#define RLC_SEC_1ST_X 5
#define RLC_SEC_1ST_Y 130

#define RLC_SEC_2ST_X 70
#define RLC_SEC_2ST_Y 130

// +++++++++ MENU CONST +++++++++++

#define SIZE_DRAWMENU 4
#define SIZE_PASSUSB 0
#define SIZE_PASSWIFI 0
#define SIZE_DRAWRLC_SEL 8 //10
#define SIZE_DRAWRLC_CONST 2

#define SIZE_DRAWRLC_P 10
#define SIZE_DRAWRLC_S 10
#define SIZE_DRAWRLC_F 6
#define SIZE_DRAWRLC_CT 2
#define SIZE_DRAWRLC_V 8
#define SIZE_DRAWRLC_B 1
#define SIZE_DRAWRLC_SM 3
#define SIZE_DRAWRLC_G 14
#define SIZE_DRAWRLC_AR 2

#define MULTIPLIER_SIZE 	17
#define POSITIVE_INFINITY	1.0/0.0
#define NEGATIVE_INFINITY	-1.0/0.0
#define OVERFLOW_U_BOUND	4294967040.0     //1000000000.0
#define OVERFLOW_L_BOUND	0.000000004
#define LOWER_LIMIT			0.000000000000000000000001
#define UPPER_LIMIT			1000000000000000000000000.0









#endif
