/*
 * Menu.cpp
 *
 *  Created on: 25 set 2019
 *      Author: user
 */
#include "Menu.hpp"
#include "../task.h"
#include <stdlib.h>
extern "C"{
#include "../variables.h"
#include "guiParameters.h"
}


Menu::Menu() : Adafruit_ST7735(TFT_CS, TFT_DC, TFT_MOSI, TFT_SCLK, TFT_RST){

	_initVars();

	this->WiFi=NULL;
	this->server=NULL;
	_wifi=false;

	this->persistentConf=NULL;

}

Menu::Menu(WiFiClass *WiFi, WiFiServer *server, Preferences *persistentConf) : Adafruit_ST7735(TFT_CS, TFT_DC, TFT_MOSI, TFT_SCLK, TFT_RST){

	_initVars();

	this->WiFi=WiFi;
	this->server=server;
	_wifi=false;

	this->persistentConf=persistentConf;
}

void Menu::drawMenu(){

	if(!needUpdate || (millis()-lastUpTime)<150) return;

	needFullRefresh=(lastMenu==0) ? false:true;

	if(needFullRefresh) {
		this->fillScreen(ST7735_BLACK);
		lastMenu=0;
		_drawHeader();

		  //navigator
		  this->drawTriangle(112, 150, 117, 155, 122, 150, ST7735_GREEN);
		  this->drawTriangle(5, 155, 10, 150, 15, 155, ST7735_GREEN);
		  this->drawCircle(65,152,3,ST7735_RED);
	}

	for(int i=0; i<SIZE_DRAWMENU; i++){
		if(i==menuFocus){
			this->setCursor(2, (50+(20*i)));
			this->setTextSize(TEXT_SIZE);
			this->setTextColor(TEXT_COLOR);
			this->print("> ");
			this->setTextColor(TEXT_COLOR,TEXT_FOCUS);
			this->println(menu[i]);
		}else{
			this->setCursor(2, (50+(20*i)));
			this->setTextSize(TEXT_SIZE);
			this->setTextColor(TEXT_COLOR);
			this->print("> ");
			this->setTextColor(TEXT_COLOR, ST7735_BLACK);
			this->println(menu[i]);
		}
	}

	lastUpTime=millis();
	needUpdate=false;

}
void Menu::drawPassTh(uint32_t count){

	if(!needUpdate || (millis()-lastUpTime)<150) return;

	needFullRefresh=(lastMenu==1) ? false:true;
	if(needFullRefresh){
		lastMenu=1;
		this->fillScreen(ST7735_BLACK);
		needFullRefresh=false;
		this->setCursor(30, 10);
		this->setTextSize(TEXT_SIZE);
		this->setTextColor(TEXT_COLOR);
		this->setTextColor(TEXT_COLOR,TEXT_FOCUS);
		this->println("PASS-THROUGH");
		this->setTextSize(TEXT_SIZE);
		this->setTextColor(TEXT_COLOR);
		this->setCursor(40, 50);
		this->println("N. Msgs:");
	}
	this->setTextColor(ST7735_WHITE, ST7735_BLACK);
	this->setCursor(50, 80);
	this->println(count);

	lastUpTime=millis();
	needUpdate=false;

}

//test purpose
void Menu::forceUpdate(){

	needUpdate=true;

}

void Menu::poll(){

	if(millis()-lastBtnCheck< 150) return;

	lastBtnCheck=millis();
	//per ora test con semplice switch
	if(digitalRead(R_PIN) == LOW){
		sp.tone(200, 20);
		delay(23);
		sp.update();

		menuFocus++;
		lastBtnPressed=millis();
		//menuFocus=(menuFocus==sizeMenu)  ?  0 : menuFocus;
	}else if(digitalRead(L_PIN) == LOW){
		sp.tone(200, 20);
		delay(23);
		sp.update();

		menuFocus--;
		lastBtnPressed=millis();
		//menuFocus=(menuFocus<0)  ?  sizeMenu-1 : menuFocus;
	}else if(digitalRead(MENU_PIN) == HIGH ){
		//logica short press and long press
		menuFocus=0;
		currentMenu=0;
		lastBtnPressed=millis();
		//return;
	}else if(digitalRead(OK_PIN) == LOW ){ //solo dimostrativo
		/*if(menuFocus==2){
			needUpdate=true;
			drawWiFi();
			sp.tone(300, 30);
			delay(33);
			sp.update();
			return;
		}else
		 if(menuFocus==3)
		{
			//deep sleep
			//esp_sleep_enable_timer_wakeup(5000000); //5s
			esp_sleep_enable_ext0_wakeup(GPIO_NUM_33, 1);
			//esp_wifi_stop();
			//esp_bt_controller_disable();
		      startWrite();
		     this->writeCommand(ST77XX_DISPOFF);//0x01
		      digitalWrite(TFT_DC, 1);
		      delay(150);
		      endWrite();
			esp_deep_sleep_start();
		}*/

		//Button pressed
		if (buttonActive == false) {
			buttonActive = true;
			buttonTimer = millis(); //lastBtnPressed
		}

		if ((millis() - buttonTimer > longPressTime) && (longPressActive == false)) { //lastBtnPressed
			longPressActive = true;
			// switch autorange
			sp.tone(200, 20);
			delay(250);
			sp.update();
			check_autorange = true; //!guiParam.check_autorange;
			show_autorange = true;
			needUpdate=true;
			//Serial.printf("\nAutoRange: %d\n", guiParam.check_autorange);
		}

	}else{
	//Button not pressed
		if (buttonActive == true) {
			if (longPressActive == true) {
				longPressActive = false;
			}else {
				lastBtnPressed=millis(); // test
				_changeInternalState();

				sp.tone(200, 20);
				delay(23);
				sp.update();
			}
			buttonActive = false;
		}
		return;
	}

	_updateInternalState();

	needUpdate=true;

	//TEST SPEAKER
//	sp.tone(200, 20);
//	delay(23);
//	sp.update();
}


void Menu::drawWiFi(){


	if (_wifi==false){
		_wifi=WiFi->softAP("SensiplusNet", NULL,  1, 0, 1);
	}

  if (_wifi){
    // Start the TCP server
	 //Serial.println("Server started");
    server->begin();
   // this->_wifi=true;

	if(!needUpdate || (millis()-lastUpTime)<150) return;

	needFullRefresh=(lastMenu==2) ? false:true;

	if(needFullRefresh) {
		this->fillScreen(ST7735_BLACK);
		lastMenu=2;
	}

	_drawHeader();

	this->setCursor(0, (70));
	this->setTextSize(TEXT_SIZE);
	this->setTextColor(TEXT_COLOR,TEXT_FOCUS);
	this->print("SSID:");
	this->setTextColor(TEXT_COLOR);
	this->print(" ");this->println(ssid);
	this->println();
	this->setTextColor(TEXT_COLOR,TEXT_FOCUS);
	this->println("PWD: ");
	this->println();
	this->setTextColor(TEXT_COLOR,TEXT_FOCUS);
	this->print("IP:  ");
	this->setTextColor(TEXT_COLOR);
	this->print(" ");this->println(WiFi->softAPIP());

  }else
  {
		this->setCursor(2, (90));
		this->setTextSize(TEXT_SIZE);
		this->setTextColor(TEXT_COLOR);
		this->print("ERROR ");
  }

	lastUpTime=millis();
	needUpdate=false;
}

void Menu ::initBeep(){
	sp.tone(300, 200);
	delay(202);
	sp.tone(600,30);
	sp.update();
	delay(33);
	sp.update();

}

bool Menu::onWiFi(){
	return this->_wifi;
}


void Menu::drawRLC(){
	uint8_t index=menuFocus;
	//uint8_t displayed=0; //DA SOSTITUIRE CON LA CONFIGURAZIONE ATTUALE
	char printMe[10]={0};
	bool mycheck = false;
	//bool update_g;
	if(millis()-lastMeasure>500){
		_getMeasure(result);
		lastMeasure=millis();
		needUpdate=true;
	}
	if(millis()-lastBtnPressed>TIME_SELECTION)
		index=SIZE_DRAWRLC_SEL+1;

	if(!needUpdate || (millis()-lastUpTime)<150) return;

		needFullRefresh=(lastMenu==3) ? false:true;
		if(needFullRefresh){
			lastMenu=3;
			this->fillScreen(ST7735_BLACK);
			_drawHeader();
		}

//		Serial.print("\nDEBUG0\n");

		// Refresh parziali con delay dopo aver premuto/selezionato un tasto
		if(check1){
	    	this->fillRect(40, 45, 110, 14, ST7735_BLACK); // first
	    	delay(150);
	    	this->fillRect(40, 75, 120, 7, ST7735_BLACK);  // second
	    	//delay(50);
	    	this->fillRect(40, 85, 120, 7, ST7735_BLACK); // temp
	    	//delay(100);
	    	this->fillRect(40, 95, 120, 7, ST7735_BLACK); // humidity

	    	check1 = false;
		}else if(check2){
			this->fillRect(40, 75, 120, 7, ST7735_BLACK);  // second
			//delay(50);
			this->fillRect(40, 85, 120, 7, ST7735_BLACK); // temp
			//delay(50);
			this->fillRect(40, 95, 120, 7, ST7735_BLACK); // humidity

			check2 = false;
		}

		uint8_t i=0;

		//PRIM PARAMETERS
		this->setTextSize(TEXT_SIZE+1); //+1
	//	this->print("   ");
		strncpy(printMe, SelectableParams[i][ currentConf[i]], 10);

		if(index==i){
			this->setTextColor(ST7735_WHITE, TEXT_FOCUS);
		}else
			this->setTextColor(ST7735_WHITE, ST7735_BLACK);


		this->setCursor(5, (45));
		this->println(SelectableParams[i][ currentConf[i] ]);
		if(currentConf[i] == Vdc){ //Vdc
				this->setTextSize(TEXT_SIZE);
				if(index==i){
					this->fillRect(16, 45, 13, 8, TEXT_FOCUS);
					this->setCursor(16, (53));
					this->println("dc");
					this->fillRect(28, 53, 1, 8, TEXT_FOCUS);
				}else{
					this->fillRect(16, 45, 13, 8, ST7735_BLACK);
					this->setCursor(16, (53));
					this->println("dc");
					this->fillRect(28, 53, 1, 8, ST7735_BLACK);
				}
		}
		this->setTextColor(ST7735_WHITE, ST7735_BLACK);
		this->setTextSize(TEXT_SIZE+1);
		this->setCursor(40, (45)); //45

//		Serial.print("\nDEBUG1\n");

/*
		if(abs(result[0].value) < 10){
			this->println(result[0].value, 4);
		}else if (abs(result[0].value) < 100){
			this->println(result[0].value, 3);
		}else if (abs(result[0].value) < 1000){
			this->println(result[0].value, 2);
		}else //if (abs(result[0].value) < 10000)
			this->println(result[0].value, 1);
*/
		this->println(result[i].value, result[i].digitToPrint);
		this->setTextSize(TEXT_SIZE); //test
		this->setCursor(114, (52)); //105 e 45 //116 e 51
//		Serial.print("\nDEBUG2\n");
		this->print(result[i].prefix);
		this->setCursor(120, (52)); //105 e 45 //116 e 51
//		Serial.print("\nDEBUG3\n");
		this->println(UnitsParams[i][ currentConf[i] ]); //test
		//this->println(result[0].unitM);
	    i++;


	    //SEC PARAMETERS
	    this->setTextSize(TEXT_SIZE);
    	if(index==i)
			this->setTextColor(ST7735_WHITE, TEXT_FOCUS);
    	else
			this->setTextColor(ST7735_WHITE, ST7735_BLACK);

  //  	Serial.print("\nDEBUG4\n");
    	this->setCursor(10, RLC_PRIM_Y);      //this->setCursor(RLC_PRIM_X, RLC_PRIM_Y);
		this->println(SelectableParams[i][ param2 ]); // cambiare currentConf[i]  //guiParam.secondOut
/*
		//implementare il fillrect ogni volta che cambia label per resettare le cifre
		if(check2){
	    	this->fillRect(40, 75, 150, 7, ST7735_BLACK);
	    	check2 = false;
		}
		*/
//		Serial.print("\nDEBUG5\n");
		this->setTextColor(ST7735_WHITE, ST7735_BLACK);
		this->setCursor(RLC_PRIM_X+10, RLC_PRIM_Y); //+25
		this->println(result[i].value, result[i].digitToPrint);
		//this->println(result[i].value, 4);
		this->setCursor(85, RLC_PRIM_Y); //109
	//	Serial.print("\nDEBUG6\n");
		this->print(result[i].prefix);
		if(unit2 == THETA_DEG || unit2 == THETA_RAD){ //rad // deg
			this->setCursor(85, (RLC_PRIM_Y)); //109
		}else{
			this->setCursor(91, (RLC_PRIM_Y)); //115
		}
//		Serial.print("\nDEBUG7\n");
		this->println(UnitsParams[i][ unit2 ]); //test

//		Serial.print("\nDEBUG8\n");

		i++;
		//CONST PARAMETERS
		this->setTextColor(ST7735_WHITE, ST7735_BLACK);
	    for(int j=0; j<SIZE_DRAWRLC_CONST; j++){
	    	this->setCursor(10, RLC_PRIM_Y+(j+1)*10);//RLC_PRIM_X
			this->println(ConstParams[j][0]);
			this->setTextColor(ST7735_WHITE, ST7735_BLACK);
			this->setCursor(RLC_PRIM_X+10, RLC_PRIM_Y+(j+1)*10);//+25
			if(j == 0){
				this->println(result[i+j].value, 1);
			}else{
				this->println((int)((result[i+j].value)*100)); //rivedere
			}
			if(j == 0){
				this->setCursor(RLC_PRIM_X+35, RLC_PRIM_Y+(j+1)*8);//50
				this->print((char)9); //� 9  - 247
				this->cursor_y = RLC_PRIM_Y+(j+1)*10;
				this->print("C");
			}else{
				this->setCursor(RLC_PRIM_X+23, RLC_PRIM_Y+(j+1)*10);//+40  //+25
				this->print("%");
			}
			//this->setCursor(RLC_PRIM_X+25, RLC_PRIM_Y+(i-1)*10);
	    }

	   /*
	    // check box autorange
	    if(guiParam.show_autorange == true){
	    	this->setTextColor(ST7735_WHITE, ST7735_BLACK);
	    	this->setCursor(5, (115));  //5
	    	bool k = guiParam.check_autorange;
	    	//this->printf("AUTORANGE: %s", arParLabels[k]);
	    	this->print("AUTORANGE: ");
	    	if(k)
	    		this->setTextColor(ST7735_GREEN, ST7735_BLACK);
	    	else
	    		this->setTextColor(ST7735_BLUE, ST7735_BLACK);

	    	this->printf("%s", arParLabels[k]);
	    	delay(1000);
	    	guiParam.show_autorange = false;
	    	mycheck = true;
//	    	Serial.print("\nautorange\n");

	    }
	    */
	//    Serial.print("\nDEBUG9\n");

	    // check box autorange
	    	    if(show_autorange == true){
	    	    	this->setTextColor(ST7735_WHITE, ST7735_BLACK);
	    	    	this->setCursor(40, (115));  //5
	    	//    	Serial.print("\nAUTORANGEEEEEEEEE\n");
	    	    	this->print("AUTORANGE");
	    	    	delay(300);
	    	    	this->fillRect(40, 115, 90, 7, ST7735_BLACK);
	    	    	delay(300);
	    	    	this->setCursor(40, (115));
	    	    	this->print("AUTORANGE");
	    	    	delay(300);
	    	    	this->fillRect(40, 115, 90, 7, ST7735_BLACK);
	    	    	delay(300);
	    	    	this->setCursor(40, (115));
	    	    	this->print("AUTORANGE");
	    	    	delay(300);
	    	    	show_autorange = false;
	    	    	mycheck = true;
	    	//    	Serial.print("\nautorange\n");
	    	    }


// update dei valori di rsense e ingain (ottenuti con l'autorange) nella GUI
	    if(update_g == true){

	//		Serial.printf("\ningain_rsense_index: %d\n", ingain_rsense_index);
	//		Serial.printf("\nr_sense (autorange): %s\n", r_sense);
	//		Serial.printf("\nin_gain (autorange): %s\n", in_gain);

			switch(ingain_rsense_index){
				case 0: //"1_50"
					currentConf[5] = 0; //x1
					break;
				case 1:	//"20_50"
					currentConf[5] = 3; //x20
					break;
				case 2:	//"40_50",
					currentConf[5] = 4; //x40
					break;
				case 3:	//"40_500",
					currentConf[5] = 8; //x400
					break;
				case 4:	//"40_5000",
					currentConf[5] = 12; //x4000
					break;
				case 5:	//"40_50000",
					currentConf[5] = 13; //x40000
					break;
				default:
					break;
			}
			update_g = false;
			check_autorange = false;
			_storeConfiguration();
	    }

	    if(show_autorange == false && mycheck == true){
	    	//delay(1000);
	    	this->fillRect(40, 115, 90, 7, ST7735_BLACK);
	    	check1 = true;
	    	mycheck = false;
	    	update_g = true;
	//    	Serial.print("\nfilled\n");
	    }


	    for(;i<SIZE_DRAWRLC_SEL-3;i++){
	    	if(index==i)
				this->setTextColor(TEXT_COLOR, TEXT_FOCUS);
	    	else
				this->setTextColor(TEXT_COLOR, ST7735_BLACK);

	    	this->setCursor(RLC_SEC_1ST_X, RLC_SEC_1ST_Y+(i-2)*10);
			this->println(SelectableParams[i][ currentConf[i] ]);
	    }

	//    Serial.print("\nDEBUG10\n");

	    for(;i<SIZE_DRAWRLC_SEL;i++){
	    	if(index==i)
				this->setTextColor(TEXT_COLOR, TEXT_FOCUS);
	    	else
				this->setTextColor(TEXT_COLOR, ST7735_BLACK);

	    	this->setCursor(RLC_SEC_2ST_X, RLC_SEC_2ST_Y+(i-5)*10);
			this->println(SelectableParams[i][ currentConf[i] ]);
	    }



	lastUpTime=millis();
	needUpdate=false;
}

void Menu::_drawHeader(){
	 int buffidx=0;
	  for (int row=0; row<24; row++) { // For each scanline...
	    for (int col=0; col<124; col++) { // For each pixel...
	      //To read from Flash Memory, pgm_read_XXX is required.
	      //Since image is stored as uint16_t, pgm_read_word is used as it uses 16bit address
	      this->drawPixel(col+1, row, pgm_read_word(logo24x124_black+buffidx));
	      buffidx++;
	    } // end pixel
	  }
}

void Menu::_getMeasure(resultMeasure result[]){

/*
			result[0].value = (float)(random(0,9999)/10000.0);
			result[0].value = batch_eis_measure().outEIS[0][0];
			result[1].value = (float)(random(0,9999)/10000.0);
			result[2].value = (float)random(0,99);
			result[3].value = (float)random(0,99);
*/

//	MEASURE_TIME_STAMP = millis();
//	Serial.printf("\nINIZIO_MEASURE_TIME_STAMP: %llu\n", MEASURE_TIME_STAMP);

//	struct outValues out = customOutput(batch_eis_measure());
	struct outValues out = customOutput(new_batch_eis_measure(check_autorange)); //new_batch_eis_measure()

//	Serial.printf("\nMEASURE_TIME_STAMP(customOutput): %llu\n", (millis() - MEASURE_TIME_STAMP));

	result[0] = adaptMeasure(out.firstOut[0][guiParam.firstOut], _type[0], guiParam.firstOut); // batch_eis_measure().outEIS[0][0];
	result[1] = adaptMeasure(out.secondOut[0][guiParam.secondOut], _type[1], guiParam.secondOut);
	result[2].value = (float)(out.outTEMP[0][0]); //(float)(temp_measure());
	result[3].value = (out.outHUMIDITY[0][7]); //(float)

//	Serial.printf("\nMEASURE_TIME_STAMP(adaptMeasures): %llu\n", (millis() - MEASURE_TIME_STAMP));

/*
	result[0].value = out.firstOut[0][guiParam.firstOut]; // batch_eis_measure().outEIS[0][0];
	result[1].value = out.secondOut[0][guiParam.secondOut];
	result[2].value = (float)(out.outTEMP[0][0]); //(float)(temp_measure());
	result[3].value = (out.outHUMIDITY[0][7]); //(float)
*/

}

void Menu::_updateInternalState(){
/*
	switch(currentMenu){
		case 0:
			menuFocus=(menuFocus==SIZE_DRAWMENU)  ?  0 : menuFocus;
			break;
		case 1:
			menuFocus=(menuFocus==SIZE_PASSUSB)  ?  0 : menuFocus;
			break;
		case 2:
			menuFocus=(menuFocus==SIZE_PASSWIFI)  ?  0 : menuFocus;
			break;
		case 3:
			menuFocus=(menuFocus==SIZE_DRAWRLC)  ?  0 : menuFocus;
			break;
		default:
			menuFocus=0;
			break;
	}*/
	if(menuFocus>=0)
		menuFocus=(menuFocus==menuSize[currentMenu])  ?  0 : menuFocus;
	else
		menuFocus=menuSize[currentMenu]-1;

}

void Menu::updateDisplay(){

	switch(currentMenu){
		case 0:
			this->drawMenu();
			break;
		case 1:
			this->drawPassTh(0);
			break;
		case 2:
			this->drawWiFi();
			break;
		case 3:
			this->drawRLC();
			break;
		default:
			this->drawMenu();
			break;
	}
}

void Menu::_changeInternalState(){

	switch(currentMenu){
		case 0:
			if(menuFocus==3)
				_deepSleepOn();
			currentMenu=menuFocus+1;
			break;
		case 3:
			currentConf[menuFocus]++;

			if(menuFocus == 1){								// fare un vettore con valori 4, 3, 4, 3 , 0 ecc
				//Serial.print("\nCIS - CASE 3\n");
				switch(guiParam.firstOut){
					case 1: //Cs
					//	Serial.print("\nCIS - CASE 3 - CASE 1\n");
						currentConf[menuFocus]=(currentConf[menuFocus] == 3 )  ?  0 : currentConf[menuFocus];
						break;
					case 2: //Lp
					//	Serial.print("\nCIS - CASE 3 - CASE 2\n");
						currentConf[menuFocus]=(currentConf[menuFocus] == 4 )  ?  0 : currentConf[menuFocus];
						break;
					case 3: //Ls
					//	Serial.print("\nCIS - CASE 3 - CASE 3\n");
						currentConf[menuFocus]=(currentConf[menuFocus] == 3 )  ?  0 : currentConf[menuFocus];
						break;
					case 4: //Rp
					//	Serial.print("\nCIS - CASE 3 - CASE 4\n");
						currentConf[menuFocus]=(currentConf[menuFocus] > 1 )  ?  0 : currentConf[menuFocus]; //==1
						break;
					case 5: //Rs
					//	Serial.print("\nCIS - CASE 3 - CASE 4\n");
						currentConf[menuFocus]=(currentConf[menuFocus] > 1 )  ?  0 : currentConf[menuFocus]; //==1
						break;
					case 6: //Z
					//	Serial.print("\nCIS - CASE 3 - CASE 5\n");
						currentConf[menuFocus]=(currentConf[menuFocus] == 2 )  ?  0 : currentConf[menuFocus];
						break;
					case 7: //G
					//	Serial.print("\nCIS - CASE 3 - CASE 6\n");
						currentConf[menuFocus]=(currentConf[menuFocus] > 1 )  ?  0 : currentConf[menuFocus]; //==1
						break;
					case 8: //Y
					//	Serial.print("\nCIS - CASE 3 - CASE 7\n");
						currentConf[menuFocus]=(currentConf[menuFocus] == 2 )  ?  0 : currentConf[menuFocus];
						break;
					case 9: //Vdc
					//	Serial.print("\nCIS - CASE 3 - CASE 8\n");
						currentConf[menuFocus]=(currentConf[menuFocus] > 1 )  ?  0 : currentConf[menuFocus]; //==1
						break;
					default: //Cp
					//	Serial.print("\nCIS - CASE 3 - DEFAULT\n");
						currentConf[menuFocus]=(currentConf[menuFocus] == 4 )  ?  0 : currentConf[menuFocus];
						break;
				}
				needUpdate=true;
			//	Serial.printf("\ncurrentConf[menuFocus](switch): %d\n", currentConf[menuFocus]);

			}else{
				//currentConf[menuFocus]++; //CASO PARAMETRO 1: il primo elemento di currentConf ++,
				currentConf[menuFocus]=(currentConf[menuFocus]==selectableSize[menuFocus])  ?  0 : currentConf[menuFocus];
			//	Serial.printf("\ncurrentConf[menuFocus]: %d\n", currentConf[menuFocus]);
			}
			needUpdate=true;
			// INSERIRE QUI LE FUNZIONI PER IL SETTAGGIO PARAM??
			setParameter();
			_storeConfiguration();
			break;
		default:
			break;

	}
}
void Menu::_deepSleepOn(){

	//TODO: aggiungere controllo e disattivazione wifi bt
	//deep sleep
	//esp_sleep_enable_timer_wakeup(5000000); //5s
		esp_sleep_enable_ext0_wakeup(GPIO_NUM_33, 1);
		//esp_wifi_stop();
		//esp_bt_controller_disable();
      startWrite();
      this->writeCommand(ST77XX_DISPOFF);//0x01
      //digitalWrite(TFT_DC, 1);
      delay(150);
      endWrite();
      esp_deep_sleep_start();
}

void Menu::_storeConfiguration(){
	if(persistentConf != NULL){
		//persistentConf.begin("configuration", false);
		//persistentConf->clear();
		persistentConf->putBytes("par", &currentConf, SIZE_DRAWRLC_SEL);
		//persistentConf->end();

	}
}

void Menu::loadConfiguration(){
	if(persistentConf != NULL)
		persistentConf->getBytes("par", &currentConf,SIZE_DRAWRLC_SEL);

	// TEST RECUPERO PARAMETRI DALL'ULTIMA CONFIG
	guiParam.firstOut = guiParam.firstCustomOUT[currentConf[0]];
//	guiParam.secondOut = guiParam.secondCustomOUT[currentConf[1]];
//	param2 = currentConf[1];
	frq 		= guiParam.fParLabelsValues[currentConf[2]]; // test recupero ultima freq (da fare per tutti i parametri su gui?)
	out_gain 	= guiParam.ogParValues[currentConf[3]];
	BiasP 		= guiParam.bParValues[currentConf[4]];

	switch(currentConf[5]){   //poi fare una funzioncina

		case 1:	//x10  	r=500 i=1
			in_gain = guiParam.igParValues[0];
			r_sense = guiParam.rParValues[1];
			break;
		case 2:	//x12  	r=50 i=12
			in_gain = guiParam.igParValues[1];
			r_sense = guiParam.rParValues[0];
			break;
		case 3:	//x20  	r=50 i=20
			in_gain = guiParam.igParValues[2];
			r_sense = guiParam.rParValues[0];
			break;
		case 4:	//x40	r=50 i=40
			in_gain = guiParam.igParValues[3];
			r_sense = guiParam.rParValues[0];
			break;
		case 5:	//x100	r=5000 i=1
			in_gain = guiParam.igParValues[0];
			r_sense = guiParam.rParValues[2];
			break;
		case 6:	//x120	r=500 i=12
			in_gain = guiParam.igParValues[1];
			r_sense = guiParam.rParValues[1];
			break;
		case 7:	//x200	r=500 i=20
			in_gain = guiParam.igParValues[2];
			r_sense = guiParam.rParValues[1];
			break;
		case 8:	//x400	r=500 i=40
			in_gain = guiParam.igParValues[3];
			r_sense = guiParam.rParValues[1];
			break;
		case 9:	//x1000	r=50000 i=1
			in_gain = guiParam.igParValues[0];
			r_sense = guiParam.rParValues[3];
			break;
		case 10: //x1200	r=5000 i=12
			in_gain = guiParam.igParValues[1];
			r_sense = guiParam.rParValues[2];
			break;
		case 11: //x2000	r=5000 i=20
			in_gain = guiParam.igParValues[2];
			r_sense = guiParam.rParValues[2];
			break;
		case 12: //x4000	r=5000 i=40
			in_gain = guiParam.igParValues[3];
			r_sense = guiParam.rParValues[2];
			break;
		case 13: //x40000	r=50000 i=40
			in_gain = guiParam.igParValues[3];
			r_sense = guiParam.rParValues[3];
			break;
		default: //x1	r=50 i=1
			in_gain = guiParam.igParValues[0];
			r_sense = guiParam.rParValues[0];
			break;
	}

	contacts = guiParam.ctParValues[currentConf[6]];
	//guiParam.check_autorange = currentConf[7];
}

void Menu::_initVars(){
	this->initR(INITR_GREENTAB);
	this->setTextWrap(false); // Allow text to run off right edge
	this->fillScreen(ST7735_BLACK);
	this->menuFocus=0;
	this->needUpdate=true;
	this->lastUpTime=0;
	this->lastMeasure=0;
	this->needFullRefresh=true;
	this->lastBtnCheck=0;
	this->lastBtnPressed=0;
	this->buttonTimer=0;

	this->buttonActive = false;
	this->longPressActive = false;
	this->longPressTime = 2000;

	this->update_g = false;
	this->show_autorange = false; //default
	this->check_autorange = false; //default


	this->lastMenu=4; //3
	this->sp.begin();
	this->sp.setVolume(1);
	//*ssid="SensiplusNet";
	//this->currentConf={0};
	this->currentMenu=3; //4
	this->writeCommand(ST77XX_DISPON);
}



void Menu::setParameter(){ //i valori di default dei parametri (indice 0) devono essere settati in _initVars

	switch(menuFocus){

		case 0: // primo parametro

			switch(currentConf[menuFocus]){
				case 1:	//Cs
					guiParam.firstOut = guiParam.firstCustomOUT[Cs];
					param2 = D;
					unit2 = D;
					guiParam.secondOut = D;
					break;
				case 2:	//Lp
					guiParam.firstOut = guiParam.firstCustomOUT[Lp];
					param2 = D;
					unit2 = D;
					guiParam.secondOut = D;
					break;
				case 3:	//Ls
					guiParam.firstOut = guiParam.firstCustomOUT[Ls];
					param2 = D;
					unit2 = D;
					guiParam.secondOut = D;
					break;
				case 4:	//Rp
					guiParam.firstOut = guiParam.firstCustomOUT[R_p];
					param2 = X;
					unit2 = X;
					guiParam.secondOut = X;
					break;
				case 5:	//Rs
					guiParam.firstOut = guiParam.firstCustomOUT[R_s];
					param2 = X;
					unit2 = X;
					guiParam.secondOut = X;
					break;
				case 6:	// |Z| impedenza
					guiParam.firstOut = guiParam.firstCustomOUT[Z];
					param2 = THETA_DEG;
					unit2 = THETA_DEG;
					guiParam.secondOut = THETA_DEG;
					break;
				case 7:	//G conduttanza
					guiParam.firstOut = guiParam.firstCustomOUT[G];
					param2 = B;
					unit2 = B;
					guiParam.secondOut = B;
					break;
				case 8:	// |Y| (reciproco di |Z|) ammettenza
					guiParam.firstOut = guiParam.firstCustomOUT[Y];
					param2 = THETA_DEG;
					unit2 = THETA_DEG;
					guiParam.secondOut = THETA_DEG;
					break;
				case 9:	//Vdc
					guiParam.firstOut = guiParam.firstCustomOUT[Vdc];//to implement
					param2 = Idc;
					unit2 = Idc;
					guiParam.secondOut = Idc;
					break;
				default: //Cp
					guiParam.firstOut = guiParam.firstCustomOUT[Cp];
					param2 = D;
					unit2 = D;
					guiParam.secondOut = D;
					break;
			}
			currentConf[1] = 0; //imposto sempre la 2a label a 0
			//guiParam.secondOut = 0; //default
			check1 = true;
			break;

		case 1: //secondo parametro (D, Q, ....)  {"D  ", "Q  ", "G ", "Rs ", "Rp ", "X  ", "B  ", "Ph ", "Idc"}
			switch(guiParam.firstOut){

				case 0://Cp case
		//	    	Serial.print("\nCase1 - case 0\n");

					switch(currentConf[menuFocus]){
						case 1:	//Q
			//				Serial.print("\nCase1- case0 - case1\n");
							guiParam.secondOut = guiParam.secondCustomOUT[Q];
							param2 = Q;
							unit2 = Q;
							break;
						case 2:	//G
			//				Serial.print("\nCase1- case0 - case2\n");
							guiParam.secondOut = guiParam.secondCustomOUT[G_2]; //G
							param2 = G_2;
							unit2 = G_2;
							break;
						case 3:	//Rp
			//				Serial.print("\nCase1- case0 - case3\n");
							guiParam.secondOut = guiParam.secondCustomOUT[Rp];
							param2 = Rp;
							unit2 = Rp;
							break;
						default: //D
			//				Serial.print("\nCase1- case0 - default\n");
							guiParam.secondOut = guiParam.secondCustomOUT[D];
							param2 = D;
							unit2 = D;
							break;
					}
					break;

				case 1: //Cs case
	//				Serial.print("\nCase1- case1\n");
					switch(currentConf[menuFocus]){
						case 1:	//Q
		//					Serial.print("\nCase1- case1 - case1\n");
							guiParam.secondOut = guiParam.secondCustomOUT[Q];
							param2 = Q;
							unit2 = Q;
							break;
						case 2:	//Rs
		//					Serial.print("\nCase1- case1 - case2\n");
							guiParam.secondOut = guiParam.secondCustomOUT[Rs];
							param2 = Rs;
							unit2 = Rs;
							break;
						default: //D
		//					Serial.print("\nCase1- case1 - default\n");
							guiParam.secondOut = guiParam.secondCustomOUT[D];
							param2 = D;
							unit2 = D;
							break;
					}
					break;

				case 2: //Lp case
		//			Serial.print("\nCase1- case2\n");
					switch(currentConf[menuFocus]){
						case 1:	//Q
		//					Serial.print("\nCase1- case2 - case1\n");
							guiParam.secondOut = guiParam.secondCustomOUT[Q];
							param2 = Q;
							unit2 = Q;
							break;
						case 2:	//G
		//					Serial.print("\nCase1- case2 - case2\n");
							guiParam.secondOut = guiParam.secondCustomOUT[G_2]; //G
							param2 = G_2;
							unit2 = G_2;
							break;
						case 3:	//Rp
		//					Serial.print("\nCase1- case2 - case3\n");
							guiParam.secondOut = guiParam.secondCustomOUT[Rp];
							param2 = Rp;
							unit2 = Rp;
							break;
	//					case 4: //Rdc
		//					break;
						default: //D
		//					Serial.print("\nCase1- case2 - default\n");
							guiParam.secondOut = guiParam.secondCustomOUT[D];
							param2 = D;
							unit2 = D;
							break;
					}
					break;

				case 3: //Ls case
	//				Serial.print("\nCase1- case3\n");
					switch(currentConf[menuFocus]){
						case 1:	//Q
	//						Serial.print("\nCase1- case3 - case1\n");
							guiParam.secondOut = guiParam.secondCustomOUT[Q];
							param2 = Q;
							unit2 = Q;
							break;
						case 2:	//Rs
		//					Serial.print("\nCase1- case3 - case2\n");
							guiParam.secondOut = guiParam.secondCustomOUT[Rs];
							param2 = Rs;
							unit2 = Rs;
							break;
	//					case 3: //Rdc
	//						break;
						default: //D
	//						Serial.print("\nCase1- case3 - default\n");
							guiParam.secondOut = guiParam.secondCustomOUT[D];
							param2 = D;
							unit2 = D;
							break;
					}
					break;

				case 4: //Rp case
	//				Serial.print("\nCase1- case4\n");
					switch(currentConf[menuFocus]){
						default:
							guiParam.secondOut = guiParam.secondCustomOUT[X];
							param2 = X;
							unit2 = X;
					break;
					}
					break;

				case 5: //Rs case
	//				Serial.print("\nCase1- case4\n");
					switch(currentConf[menuFocus]){
						default:
							guiParam.secondOut = guiParam.secondCustomOUT[X];
							param2 = X;
							unit2 = X;
					break;
					}
					break;

				case 6: //Z case
	//				Serial.print("\nCase1- case5\n");
					switch(currentConf[menuFocus]){
						case 1:	//theta_r
	//						Serial.print("\nCase1- case5 - case1\n");
							guiParam.secondOut = guiParam.secondCustomOUT[THETA_RAD];
							param2 = THETA_RAD;
							unit2 = THETA_RAD;
							break;
						default: //theta_d
	//						Serial.print("\nCase1- case5 - default\n");
							guiParam.secondOut = guiParam.secondCustomOUT[THETA_DEG];
							param2 = THETA_DEG;
							unit2 = THETA_DEG;
							break;
					}
					break;

				case 7: //G case
	//				Serial.print("\nCase1- case6\n");
					switch(currentConf[menuFocus]){
						default:
							guiParam.secondOut = guiParam.secondCustomOUT[B];
							param2 = B;
							unit2 = B;
							break;
					}
					break;

				case 8: //Y case
	//				Serial.print("\nCase1- case7\n");
					switch(currentConf[menuFocus]){
						case 1:	//theta_r
	//						Serial.print("\nCase1- case7 - case1\n");
							guiParam.secondOut = guiParam.secondCustomOUT[THETA_RAD];
							param2 = THETA_RAD;
							unit2 = THETA_RAD;
							break;
						default: //theta_d
	//						Serial.print("\nCase1- case7 - default\n");
							guiParam.secondOut = guiParam.secondCustomOUT[THETA_DEG];
							param2 = THETA_DEG;
							unit2 = THETA_DEG;
							break;
					}
					break;

				case 9: //Vdc
	//				Serial.print("\nCase1- case8\n");
					switch(currentConf[menuFocus]){
						default:
							guiParam.secondOut = guiParam.secondCustomOUT[Idc];//to implement
							param2 = Idc;
							unit2 = Idc;
							break;
					}
					break;
			}
			//check1 = true;
			check2 = true;
			break;

		case 2:  //frequenza

			switch(currentConf[menuFocus]){

				case 1: //setta per 100Hz
					frq = guiParam.fParLabelsValues[1];
					break;
				case 2:	//setta per 10kHz
					frq = guiParam.fParLabelsValues[2];
					break;
				case 3:	//setta per 100kHz
					frq = guiParam.fParLabelsValues[3];
					break;
				case 4:	//setta per 1MHz
					frq = guiParam.fParLabelsValues[4];
					break;
				case 5:	//setta (F_DEFAULT)
					frq = guiParam.fParLabelsValues[5];
					break;
				default: //setta (10Hz)
					frq = guiParam.fParLabelsValues[0];
					break;
			}
			check1 = true;
			break;

		case 3:  // OutGain --> V: ?

			switch(currentConf[menuFocus]){  //fare funzione

				case 1:	// outgain: 1  --> V: ?
					out_gain = guiParam.ogParValues[1];
					break;
				case 2:	// outgain: 2  --> V: ?
					out_gain = guiParam.ogParValues[2];
					break;
				case 3:	// outgain: 3  --> V: ?
					out_gain = guiParam.ogParValues[3];
					break;
				case 4:	// outgain: 4  --> V: ?
					out_gain = guiParam.ogParValues[4];
					break;
				case 5:	// outgain: 5  --> V: ?
					out_gain = guiParam.ogParValues[5];
					break;
				case 6:	// outgain: 6  --> V: ?
					out_gain = guiParam.ogParValues[6];
					break;
				case 7:	// outgain: 7  --> V: ?
					out_gain = guiParam.ogParValues[7];
					break;
				default: // outgain: 0  --> V: ?
					out_gain = guiParam.ogParValues[0];
					break;
			}
			check1 = true;
			break;

		case 4: // B: 0V
					// implementare bias
			break;

		case 5: //inGain X rsense  G: x1....

			switch(currentConf[menuFocus]){  //fare funzione

				case 1:	//x10  	r=500 i=1
					in_gain = guiParam.igParValues[0];
					r_sense = guiParam.rParValues[1];
					break;
				case 2:	//x12  	r=50 i=12
					in_gain = guiParam.igParValues[1];
					r_sense = guiParam.rParValues[0];
					break;
				case 3:	//x20  	r=50 i=20
					in_gain = guiParam.igParValues[2];
					r_sense = guiParam.rParValues[0];
					break;
				case 4:	//x40	r=50 i=40
					in_gain = guiParam.igParValues[3];
					r_sense = guiParam.rParValues[0];
					break;
				case 5:	//x100	r=5000 i=1
					in_gain = guiParam.igParValues[0];
					r_sense = guiParam.rParValues[2];
					break;
				case 6:	//x120	r=500 i=12
					in_gain = guiParam.igParValues[1];
					r_sense = guiParam.rParValues[1];
					break;
				case 7:	//x200	r=500 i=20
					in_gain = guiParam.igParValues[2];
					r_sense = guiParam.rParValues[1];
					break;
				case 8:	//x400	r=500 i=40
					in_gain = guiParam.igParValues[3];
					r_sense = guiParam.rParValues[1];
					break;
				case 9:	//x1000	r=50000 i=1
					in_gain = guiParam.igParValues[0];
					r_sense = guiParam.rParValues[3];
					break;
				case 10: //x1200	r=5000 i=12
					in_gain = guiParam.igParValues[1];
					r_sense = guiParam.rParValues[2];
					break;
				case 11: //x2000	r=5000 i=20
					in_gain = guiParam.igParValues[2];
					r_sense = guiParam.rParValues[2];
					break;
				case 12: //x4000	r=5000 i=40
					in_gain = guiParam.igParValues[3];
					r_sense = guiParam.rParValues[2];
					break;
				case 13: //x40000	r=50000 i=40
					in_gain = guiParam.igParValues[3];
					r_sense = guiParam.rParValues[3];
					break;
				default: //x1	r=50 i=1
					in_gain = guiParam.igParValues[0];
					r_sense = guiParam.rParValues[0];
					break;
			}
			check1 = true;
			break;

		case 6: //2CONTACTS/4CONTACTS

			switch(currentConf[menuFocus]){

				case 1: //4CONTACTS
					contacts = guiParam.ctParValues[1];
					break;
				default: //2CONTACTS
					contacts = guiParam.ctParValues[0];
					break;
			}
			check1 = true;
			break;

		case 7: //SLOW/MEDIUM/FAST  --> InFilter espresso in seconds
/*
			switch(currentConf[menuFocus]){

				case 1:	//AUTORANGE_ON
					guiParam.check_autorange = guiParam.arParLabelsValues[1];
					break;
				default: //AUTORANGE_FF
					guiParam.check_autorange = guiParam.arParLabelsValues[0];
					break;
			}
*/
			break;

		default:
			;
			break;

	}

}

struct outValues Menu::customOutput(struct outValues old){

//	Serial.printf("\nCONDUCTANCE: %f\n", old.outEIS[0][CONDUCTANCE]);

	SPComplex zeta, rec;
	zeta.re = (old.outEIS[0][MODULE])*cos(old.outEIS[0][PHASE]); // sarebbe la Rs
	zeta.im = (old.outEIS[0][MODULE])*sin(old.outEIS[0][PHASE]); // mi serve per calcolare Cs
	complexReciprocal(&zeta, &rec);

//	avail_frq = frq;
	AvailableFrequencies frqClc;
	availableFrequencies(frq, &frqClc);
	avail_frq = frqClc.availableFrequency;
//	Serial.printf("\navail_frq (customOutput): %ld\n", avail_frq);

// Calcolo Cs e Ls (circuito serie)
	sp_double q, d, C_s, c_s, l_s;
	C_s = (1.0*pow(10,12) / (2*pigreco*avail_frq*zeta.im)); //Cs
//	Serial.printf("\nc_s(pre op): %f\n", C_s);
	c_s = (C_s > 0) ? C_s : 0; //Cs finale
	l_s	= (C_s < 0) ? abs(C_s) : 0; //Ls finale

	//	switch(guiParam.firstOut) fare lo switch in base al parametro primario selezionato (Cp mode, Cs mode ecc...)
	switch(guiParam.firstOut){
		case 0: //Cp Mode
			q = (2*pigreco*avail_frq*old.outEIS[0][CAPACITANCE]*old.outEIS[0][RESISTANCE]);
			d = (1.0 / q);
			break;
		case 1: //Cs Mode
			d = (2*pigreco*avail_frq*c_s*rec.re); //2*pigreco*avail_frq*Cs*Rs
			q = (1.0 / d);
			break;
		case 2: //Lp Mode
			q = (old.outEIS[0][RESISTANCE] / (2*pigreco*avail_frq*(old.outEIS[0][INDUCTANCE])));
			d = (1.0 / q);
			break;
		case 3: //Ls Mode
			q = (2*pigreco*avail_frq*l_s) / zeta.re;
			d = (1.0 / q);
			break;
		default:
			q = 0.0;
			d = 0.0;
			break;
	}

//	Serial.printf("\nQ_value: %f\n", q);
//	Serial.printf("\nD_value: %f\n", d);

	//first Parameter
	outValues.firstOut[0][Cp] 	= 	old.outEIS[0][CAPACITANCE]; //CAPACITANCE ->Cp
	outValues.firstOut[0][Cs] 	= 	c_s;	 // to verify..chiedere come ottenerla
	outValues.firstOut[0][Lp] 	=  	old.outEIS[0][INDUCTANCE];  // to test Lp
	outValues.firstOut[0][Ls] 	= 	l_s; // to test
	outValues.firstOut[0][R_p]  = 	old.outEIS[0][RESISTANCE]; //Resistenza (Rp) -> chiedere se va bene la formula //out[i][RESISTANCE] = 1.0/rec.re;
																									   //out[i][RESISTANCE] = 1.0/out[i][CONDUCTANCE];
	outValues.firstOut[0][R_s]  = 	zeta.re;
	outValues.firstOut[0][Z]  	= 	abs(old.outEIS[0][MODULE]);
	outValues.firstOut[0][G]  	= 	old.outEIS[0][CONDUCTANCE]; //rec.re
	outValues.firstOut[0][Y]  	= 	(1.0 / (old.outEIS[0][MODULE]));
	outValues.firstOut[0][Vdc]	= 	0; // to implement..

	//second Parameter
	outValues.secondOut[0][D]  		= 	d; // to verify..
	outValues.secondOut[0][Q]  		= 	q; // to verify..
	outValues.secondOut[0][G_2] 	= 	old.outEIS[0][CONDUCTANCE]; //rec.re == G
	outValues.secondOut[0][Rs] 		= 	zeta.re; // to verify..chiedere come ottenerla Rs = zeta.re = zetaMod*cos(zetaPha);
	outValues.secondOut[0][Rp] 		= 	old.outEIS[0][RESISTANCE]; // = 1.0/rec.re;
	outValues.secondOut[0][X] 		= 	zeta.im; //zeta.im = zetaMod*sin(zetaPha); //chiedere
	outValues.secondOut[0][B]		= 	old.outEIS[0][SUSCEPTANCE]; // to verify.. dovrebbe essere la SUSCETTANZA! == rec.im
	outValues.secondOut[0][THETA_DEG]	= 	((old.outEIS[0][PHASE]) * 180) / pigreco;
	outValues.secondOut[0][THETA_RAD]	= 	old.outEIS[0][PHASE]; // to verify..
	outValues.secondOut[0][Idc]		= 	0; // to implement..



/*
//TEST VALORI

	//test Cp (CAPACITANCE)
			Serial.printf("\nCAPACITANCE: %f\n", old.outEIS[0][CAPACITANCE]);
	//test Cs (CAPACITANCE)
			Serial.printf("\nc_s: %f\n", c_s);
	//test Lp (INDUCTANCE)
			Serial.printf("\nINDUCTANCE: %f\n", old.outEIS[0][INDUCTANCE]);
	//test Ls (CAPACITANCE)
			Serial.printf("\nl_s: %f\n", l_s);
	//test Rp
	//	Serial.printf("\nRp (1.0/rec.re;): %f\n",  1.0/rec.re); //1/G
		Serial.printf("\nRp (RESISTANCE):  %f\n", old.outEIS[0][RESISTANCE]);
	// Stessi numeri, 1.0/rec.re = old.outEIS[0][RESISTANCE]
		Serial.printf("\nRs (RESISTANCE):  %f\n", zeta.re);
		Serial.printf("\nZ (IMPEDENZA):  %f\n", old.outEIS[0][MODULE]);
		Serial.printf("\nG (CONDUCTANCE): %f\n", old.outEIS[0][CONDUCTANCE]);

		Serial.printf("\nY (AMMETTENZA):  %f\n", (1.0 / (old.outEIS[0][MODULE])));
 	 //test B (suscettanza)
	//	Serial.printf("\nSUSCEPTANCE (rec.im): %f\n", rec.im);
		Serial.printf("\nSUSCEPTANCE (out.suceptance): %f\n", old.outEIS[0][SUSCEPTANCE]);
	// Stessi numeri, rec.im = old.outEIS[0][SUSCEPTANCE]

*/

	//test G
	//	Serial.printf("\nG (rec.re): %f\n",  rec.re);
	//	Serial.printf("\nG ((1/zetaMod)*cos(zetaPha) = CONDUCTANCE): %f\n", old.outEIS[0][CONDUCTANCE]);
	// Stessi numeri, rec.re = old.outEIS[0][CONDUCTANCE]

	/*
	//Cp Mode
	q = (2*pigreco*avail_frq*old.outEIS[0][CAPACITANCE]*old.outEIS[0][RESISTANCE]);
	d = (1.0 / q);
	G  	= 	rec.re;

	//Cs Mode
	d = (2*pigreco*avail_frq*c_s*rec.re); //2*pigreco*avail_frq*Cs*Rs
	q =(1.0 / d);

	//Lp Mode
	q = Rp / (2*pigreco*avail_frq*Lp);
	d = (1.0 / q);
	G = 1/Rp;

	//Ls Mode
	q = (2*pigreco*avail_frq*Ls) / Rs;
	d = (1.0 / q);
*/

	return outValues;
}



struct resultMeasure Menu::adaptMeasure(double raw, char *type, int indexToReduce){

	resultMeasure measureAdapted;

	double k = 1000;
	double coeff = 1;
	double value = 0;
	multiplier = 1;


//	Serial.printf("\nraw: %f\n", raw);

	if (strcmp(type, "secondOut") == 0){
		if(indexToReduce == D || indexToReduce == Q){ // to avoid "ovf"
			if((abs(raw) >= OVERFLOW_U_BOUND || abs(raw) < OVERFLOW_L_BOUND) && raw != POSITIVE_INFINITY && raw != NEGATIVE_INFINITY){ //raw <= -DBL_MAX || raw >= DBL_MAX ||
					//Serial.printf("\nflagInfinityValue is true\n");
					//Serial.printf("\nindexToReduce: %d\n", indexToReduce);
					//Serial.printf("\nraw_value: %f\n", raw);
					if(abs(raw) > 1){
						measureAdapted.value = raw / 1000000000000.0;
						measureAdapted.prefix = "E+12";
					}else{
						measureAdapted.value = raw * 1000000000000.0;
						measureAdapted.prefix = "E-12";
					}

					if(abs(measureAdapted.value) < 10){
						measureAdapted.digitToPrint = 4;
					}else if (abs(measureAdapted.value) < 100){
						measureAdapted.digitToPrint = 3;
					}else if (abs(measureAdapted.value) < 1000){
						measureAdapted.digitToPrint = 2;
					}else //if (abs(result[0].value) < 10000)
						measureAdapted.digitToPrint = 1;

					return measureAdapted;
			 }
		}
	}

	//inizializzo l'indice del prefisso moltiplicatore a seconda del parametro da stampare
	if( strcmp(type, "firstOut") == 0){

		multiplier_index = initial_multiplier_index_1[indexToReduce];

	}else if (strcmp(type, "secondOut") == 0){

		multiplier_index = initial_multiplier_index_2[indexToReduce];

		if(indexToReduce == D || indexToReduce == Q || indexToReduce == THETA_DEG || indexToReduce == THETA_RAD){ // D, Q or THETA

			if(abs(raw) < 10){
					measureAdapted.digitToPrint = 4;
			}else if (abs(raw) < 100){
					measureAdapted.digitToPrint = 3;
			}else if (abs(raw) < 1000){
					measureAdapted.digitToPrint = 2;
			}else //if (abs(result[0].value) < 10000)
					measureAdapted.digitToPrint = 1;

			measureAdapted.value = raw;
			measureAdapted.prefix = MULTIPLIER_PREFIX[multiplier_index];
			return measureAdapted;
		}
	}

    bool flagInfinityValue = false;

    if(raw == POSITIVE_INFINITY || raw == NEGATIVE_INFINITY || abs(raw) < LOWER_LIMIT || abs(raw) > UPPER_LIMIT){
            flagInfinityValue = true;
     }

    if(!flagInfinityValue) {

		value = raw * multiplier;
		value = abs(value);
		if (value < 1 && value!=0) { // && value >= LOWER_LIMIT
			do {
				multiplier_index--;
				value = value * k;
				coeff = coeff * k;
	//			Serial.printf("\nmultiplier_index--\n");
			} while (value < 1);


		} else if (value >= 1000 && value!=0) {  //>1     // && value <= UPPER_LIMIT
			k = 1 / k;
			do {
				multiplier_index++;
				value = value * k;
				coeff = coeff * k;
	//			Serial.printf("\nmultiplier_index++\n");
	//			Serial.printf("\nvalue: %f\n", value);
	//			Serial.printf("\ncoeff: %f\n", coeff);
			} while (value >= 1000); // try < 1  // >= 1000 oppure 1

		}

		multiplier = multiplier * coeff;
    //}
		raw *= multiplier;

		if(abs(raw) < 10){
			measureAdapted.digitToPrint = 4;
		}else if (abs(raw) < 100){
			measureAdapted.digitToPrint = 3;
		}else if (abs(raw) < 1000){
			measureAdapted.digitToPrint = 2;
		}else //if (abs(result[0].value) < 10000)
			measureAdapted.digitToPrint = 1;

		//multiplier_index_1 � il MULTIPLIER_PREFIX da stampare
		measureAdapted.value = raw;
		measureAdapted.prefix = MULTIPLIER_PREFIX[multiplier_index];

    }else if(abs(raw) < LOWER_LIMIT){ //per il problema conduttanza e prefisso (to test)
    	Serial.printf("\nthe value exceeds lower limit\n");
    	//Serial.printf("\nraw_value: %f\n", raw);
    	measureAdapted.digitToPrint = 4;
		measureAdapted.value = 0.0;
		measureAdapted.prefix = MULTIPLIER_PREFIX[multiplier_index];
    }else if(abs(raw) > UPPER_LIMIT){
    	Serial.printf("\nthe value exceeds upper limit\n");
    	//Serial.printf("\nraw_value: %f\n", raw);
    	measureAdapted.digitToPrint = 4;
		measureAdapted.value = 0.0;
		measureAdapted.prefix = MULTIPLIER_PREFIX[multiplier_index];
    }

	return measureAdapted;
}
