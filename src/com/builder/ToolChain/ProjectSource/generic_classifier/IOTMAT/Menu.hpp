/*
 * Menu.hpp
 *
 *  Created on: 25 set 2019
 *      Author: user
 */

#ifndef _IOTMAT_MENU_HPP_
#define _IOTMAT_MENU_HPP_

#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h>
//#include<Fonts/Roboto_Mono_Medium_7.h>
#include "Speaker/Speaker.h"
#include "IOTMatConfig.h"
#include <WiFi.h>
#include <Preferences.h>  /* store/load configuration */
//#include <string.h>
#include "Logo.hpp"
#include "float.h"
//#include "esp_wifi.h"
//#include "esp_bt.h"




//Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_MOSI, TFT_SCLK, TFT_RST); // @suppress("Abstract class cannot be instantiated")

//TODO:: gestione bottoni: alla pressione del tasto centrale si scorre tra i possibili parametri
			// dopo un po' scompare la visualizzazione della selezione
			//no menu
			//alla pressione contemporanea dei tasti su/giu si abilita il wifi con relativa schermata
			//Alla chiusura delle connessioni passthru wifi e usb si ritorna alla misura locale...
	struct resultMeasure{
		char unitM[4];
		const char *prefix;
		int digitToPrint;
	//	const char *primaryUnit[SIZE_DRAWRLC_P] = {"F", "F", "H", "H", "Ohm", "Ohm", "S", "S", "V"};
	//	const char *secondaryUnit[SIZE_DRAWRLC_S] = {"", "", "S", "Ohm", "Ohm", "Ohm", "S", "rad", "A"};
		double value;
	};
//TODO: restructure as class



class Menu:protected Adafruit_ST7735{

private:
	//TODO: rendere menu facilmente gestibile
	const char *menu[4] = { "PassThrough","Enable WiFi", "RLC Meter","Deep Sleep" };
	const char *primaryParLabels[SIZE_DRAWRLC_P] = { "Cp", "Cs", "Lp", "Ls", "Rp", "Rs", "Z ", "G ", "Y ", "V" }; //Vdc
	const char *secondaryParLabels[SIZE_DRAWRLC_S] = {"D  ", "Q  ", "G ", "Rs ", "Rp ", "X  ", "B  ", "Ph ", "Ph ", "Idc"}; //parametri secondari
	const char *tParLabels[1] ={"T"}; //fisso non modificabile
	const char *rhParLabels[1] ={"RH"};//fisso non mpodificabile
	const char *fParLabels[SIZE_DRAWRLC_F] = {"F: 10Hz   ", "F: 100Hz  ", "F: 1kHz   ", "F: 100kHz ", "F: 1MHz   ", "F: 78125Hz" };

	const char  *arParLabels[2] = {"OFF", "ON "};

	/*
	const float fparLabelsValues[SIZE_DRAWRLC_F] = {10.0f,  100.0f, 1000.0f, 100000.0f, 1000000.0f };
	spParamEIS.Qiparam->frequency = fparLabelsValues[ currentConf[2 ]];
	*/

	const char *ctParLabels[SIZE_DRAWRLC_CT]  = { "2CONTACTS", "4CONTACTS" };
	//const char *vParLabels[SIZE_DRAWRLC_V]  = { "V: 0.128V", "V: 0.526V", "V: 2.800V"};
	const char *vParLabels[SIZE_DRAWRLC_V]  = { "V: 478mV ", "V: 527mV ", "V: 631mV ", "V: 724mV ",
												"V: 900mV ", "V: 1170mV", "V: 1730mV", "V: 3360mV" }; // mappatura OutGain

	const char *bParLabels[SIZE_DRAWRLC_B]= { "B: 0  "}; //biasP mappatura in B
	/*
	const char *bParLabels[SIZE_DRAWRLC_B]= { "B: 0    ", "B: -2048", "B: -1024", "B: -512 ", "B: -256 ", "B: -128 ",
			"B: -64  ", "B: -32  ", "B: -16  ", "B: -8   ", "B: -4   ", "B: -2   ", "B: -1   ", "B: 2   ", "B: 4   ",
			"B: 8   ", "B: 16  ", "B: 32  ", "B: 64  ", "B: 128 ", "B: 256 ", "B: 512 ", "B: 1024", "B: 2047"}; //biasP mappatura in B
	*/

	const char *smParLabels[SIZE_DRAWRLC_SM] = { "SLOW  ", "MEDIUM", "FAST  " };

	const char *igParLabels[SIZE_DRAWRLC_G] = {"G: x1    ", "G: x10   ", "G: x12   ", "G: x20   ", "G: x40   ",
											  	  	       "G: x100  ", "G: x120  ", "G: x200  ", "G: x400  ",
														  "G: x1000 ", "G: x1200 ", "G: x2000 ", "G: x4000 ",
														   	   	   	   	   	   	   	   	   	    "G: x40000" };
	bool update_g;// = false;
	bool show_autorange;// = false; //default
	bool check_autorange;// = false; //default


	// da spostare nella struttura result
	const char *primaryUnits[SIZE_DRAWRLC_P] = {"F", "F", "H", "H", &ohm, &ohm, &ohm, "S", "S", "V"};
	const char *secondaryUnits[SIZE_DRAWRLC_S] = {"", "", "S  ", &ohm, &ohm, &ohm, "S  ", "deg", "rad", "A  "};
	char ohm = ((char)233);


	 const char *MULTIPLIER_PREFIX[MULTIPLIER_SIZE] = {  "y",  "z",   "a",    "f",  "p",   "n",  &mu,  "m",  "", "k", "M", "G",  "T",  "P", "E",  "Z", "Y"};
	 char mu = ((char)229);

	 //first measure
	 int initial_multiplier_index_1[SIZE_DRAWRLC_P] = {4, 4, 7, 7, 8, 8, 8, 8, 8, 7};  // Capacitance = 4; Inductance = 7; Resistance = 8; Conductance = 8
     int multiplier_index = 0;
     double multiplier = 1;
    // int digitToPrint = 0;
     char *_type[2] = {"firstOut", "secondOut"};

     //second measure
     int initial_multiplier_index_2[SIZE_DRAWRLC_S] = {8, 8, 8, 8, 8, 8, 8, 8, 8, 7};  // Capacitance = 4; Inductance = 7; Resistance = 8; Conductance = 8
    // int multiplier_index_2 = 0;
    // double multiplier_2 = 1;



	const char **SelectableParams[SIZE_DRAWRLC_SEL]={
			primaryParLabels,
			secondaryParLabels,
			//tParLabels,
			//rhParLabels,
			fParLabels,
			vParLabels,
			bParLabels,
			igParLabels,
			ctParLabels,
			smParLabels,
			//arParLabels
						}; //len ->10 // 8

	const char **ConstParams[SIZE_DRAWRLC_SEL]={
			tParLabels,
			rhParLabels
	};

	const char **UnitsParams[2]={
			primaryUnits,
			secondaryUnits
	};


/*
	resultMeasure result[4]={
			{"pF", 0},
			{"", 0},
			{"C", 0},
			{"%",0}
	};
	*/

	resultMeasure result[4];

	const uint8_t menuSize[4]={SIZE_DRAWMENU, SIZE_PASSUSB, SIZE_PASSWIFI, SIZE_DRAWRLC_SEL};
	const uint8_t selectableSize[SIZE_DRAWRLC_SEL]={
			SIZE_DRAWRLC_P,
			SIZE_DRAWRLC_S,
			SIZE_DRAWRLC_F,
			SIZE_DRAWRLC_V,
			SIZE_DRAWRLC_B,
			SIZE_DRAWRLC_G,
			SIZE_DRAWRLC_CT,
			SIZE_DRAWRLC_SM
	};

	//Time counters
	unsigned long long int lastMeasure;
	unsigned long long int lastUpTime;
	unsigned long long int lastBtnCheck;
	unsigned long long int lastBtnPressed;

	unsigned long long int buttonTimer;
	bool buttonActive;
	bool longPressActive;
	long longPressTime;


	bool check1,check2;
	int param2, unit2; // secondo parametro da printare a schermo dinamicamente (aggiornato nella setParameter);
	//uint8_t sizeMenu=4;
	//uint8_t sizeMenu=10;

	//Internal state
	int8_t menuFocus;
	bool needUpdate;
	bool needFullRefresh;
	uint8_t lastMenu;
	uint8_t currentMenu;
	//uint8_t selectedPar;

	uint8_t currentConf[SIZE_DRAWRLC_SEL]={0};

	//int8_t menuI;

	// speaker class
	SPEAKER sp;

	//wifi
	WiFiClass *WiFi;
	WiFiServer *server;
	char ssid[15]="SensiplusNet";
	bool _wifi;

	//configuration
	Preferences *persistentConf;

	//private methods
	void _drawHeader();
	void _getMeasure(resultMeasure result[]);
	void _updateInternalState();
	void _changeInternalState();
	void _deepSleepOn();
	void _storeConfiguration();
	void _initVars();




public:
	Menu();
	Menu( WiFiClass *WiFi, WiFiServer *server, Preferences *persistentConf);

	void drawMenu();
	void drawPassTh(uint32_t count);
	void poll();
	void drawRLC();
	void forceUpdate();
	void drawWiFi();
	void initBeep();
	bool onWiFi();
	void updateDisplay();
	void loadConfiguration();
	struct outValues customOutput(struct outValues);
	void setParameter(); //pensare ad una struttura (guiParam) di default nella classe e poi inizializzarla (_initVars)
	struct resultMeasure adaptMeasure(double raw, char *type, int indexToReduce);
};


#endif /* IOTMAT_MENU_HPP_ */
