/*
 * SPDataRepresentationManager.h
 *
 *  Created on: 01 giu 2020
 *      Author: giova
 */

#ifndef IOTMAT_SPDATAREPRESENTATIONMANAGER_HPP_
#define IOTMAT_SPDATAREPRESENTATIONMANAGER_HPP_

#include "../API/initialization/configurations.h"
#include "../API/level2/Parameters/SPMeasurementParameterEIS.h"
#include "../API/level2/Items/SPParamItemMeasures.h"


//import com.sensichips.sensiplus.level2.parameters.items.SPParamItemPOTMeasures;
/*
extern "C"{
#include "guiParameters.h"
}

#define MEASURE_UNIT_SIZE 	11
#define MULTIPLIER_SIZE 	17
#define POSITIVE_INFINITY	1.0/0.0
#define NEGATIVE_INFINITY	-1.0/0.0


typedef struct str_spdatarepresentation{

 //    static DecimalFormat formatter;
 //   static final long serialVersionUID = 42L;

    char* measureUnit = ""; //Volt, Ampere, ecc.
    char* prefix;
    double range_min;
    double range_max;
    double alarm_threshold;
    double values[][];
    double meanValue;
    int indexToReduce = 0;

}SPDataRepresentation;



class SPDataRepresentationManager {

  private:
     char *measureUnit = ""; //Volt, Ampere, ecc.

    // int    multiplier_index = 0;
     double rangeMin;
     double rangeMax;
     double alarmThreshold;
     int    initialMultiplierIndex = 0;
     double initialRangeMin;
     double initialRangeMax;
     double initialAlarmThreshold;

    //private double th = 1;
     int indexToReduce = 0;
     int multiplier_index = 0;
     double multiplier = 1;

  public:

     int getIndexToReduce() {
        return indexToReduce;
    }

     void setIndexToReduce(int indexToReduce) {
        this->indexToReduce = indexToReduce;
    }

     char mu = ((char)229);
     const char *MULTIPLIER_PREFIX[MULTIPLIER_SIZE] = {  "y",  "z",   "a",    "f",  "p",   "n",  &mu,  "m", "", "k", "M", "G", "T", "P", "E", "Z", "Y"};
     const double *MULTIPLIER[MULTIPLIER_SIZE] 		= {-24.0, -21.0, -18.0, -15.0, -12.0, -9.0, -6.0, -3.0, 0.0, 3.0, 6.0, 9.0, 12.0, 15.0, 18.0, 21.0, 24.0};

     char ohm = ((char)233);
     char deg = ((char)9);
     char* celsius = strcat(&deg, "C");
     const char *MEASURE_UNIT_LABEL[MEASURE_UNIT_SIZE] 	= {"Ohm", "Farad", "Henry",   "C",    "%", "V", "A", "L", "time", "", "rad"};
     const char *MEASURE_UNIT[MEASURE_UNIT_SIZE] 		= {&ohm, 	"F",     "H",   &celsius, "%", "V", "A", "L",   "t",  "", "rad"};



     SPDataRepresentationManager(int indexToReduce){
        this->indexToReduce = indexToReduce;
    }




     void initForSensorMeasurements(SPSensingElement spSensingElement){

        if(strcmp(spSensingElement.measureTecnique, "EIS" ) == 0){ //&& !spSensingElement.getName().contains("POLYMIDE")){
                initForEISRawMeasurements();
        }
    }

     void initForEISRawMeasurements(){
        if (this->indexToReduce == CAPACITANCE){
            this->setMeasureUnit("F");
            this->setRangeMin(0.0);
            this->setRangeMax(1.0);
            this->setInitial_multiplier(-12);
            this->setDefault_alarm_threshold(0.5);
        }else if (this->indexToReduce == RESISTANCE){
            this->setMeasureUnit(&ohm);
            this->setRangeMin(0.0);
            this->setRangeMax(1000.0);
            this->setInitial_multiplier(0);
            this->setDefault_alarm_threshold(1000);
        }else if (this->indexToReduce == INDUCTANCE){
            this->setMeasureUnit("H");
            this->setRangeMin(0.0);
            this->setRangeMax(1.0);
            this->setInitial_multiplier(-3); //0?
            this->setDefault_alarm_threshold(1.0);
        }else if (this->indexToReduce == IN_PHASE){
            this->setMeasureUnit("N");
            this->setRangeMin(0.0);
            this->setRangeMax(33000);
            this->setInitial_multiplier(0);
            this->setDefault_alarm_threshold(15000);
        }else if (this->indexToReduce == QUADRATURE){
            this->setMeasureUnit("N");
            this->setRangeMin(0.0);
            this->setRangeMax(33000);
            this->setInitial_multiplier(0);
            this->setDefault_alarm_threshold(15000);
        }else if (this->indexToReduce == MODULE){
            this->setMeasureUnit("O");
            this->setRangeMin(0.0);
            this->setRangeMax(1000.0);
            this->setInitial_multiplier(0);
            this->setDefault_alarm_threshold(500);
        }else if (this->indexToReduce == PHASE){
            this->setMeasureUnit("rad");
            this->setRangeMin(0.0);
            this->setRangeMax(0.0 + (pigreco/2));
            this->setInitial_multiplier(0);
            this->setDefault_alarm_threshold(pigreco/4 + 0);
        }else if (this->indexToReduce == CONDUCTANCE){
            this->setMeasureUnit("S");
            this->setRangeMin(1E-8);
            this->setRangeMax(1E-7);
            this->setInitial_multiplier(0);
            this->setDefault_alarm_threshold(0.6E-7);
        }else if (this->indexToReduce == SUSCEPTANCE){
            this->setMeasureUnit("S");
            this->setRangeMin(0.0);
            this->setRangeMax(1000.0);
            this->setInitial_multiplier(0);
            this->setDefault_alarm_threshold(500);
        }else if (this->indexToReduce == VOLTAGE){
        	this->setMeasureUnit("V");
        	this->setRangeMin(0.0);
        	this->setRangeMax(1000.0);
        	this->setInitial_multiplier(-3);
        	this->setDefault_alarm_threshold(500);
        }else if (this->indexToReduce == CURRENT){
        	this->setMeasureUnit("A");
        	this->setRangeMin(0.0);
        	this->setRangeMax(1000.0);
        	this->setInitial_multiplier(-3);
        	this->setDefault_alarm_threshold(500);
        }else if (this->indexToReduce == DELTA_V_APPLIED){
        	this->setMeasureUnit("V");
        	this->setRangeMin(0.0);
        	this->setRangeMax(1000.0);
        	this->setInitial_multiplier(-3);
        	this->setDefault_alarm_threshold(500);
        }else if (this->indexToReduce == CURRENT_APPLIED){
        	this->setMeasureUnit("A");
        	this->setRangeMin(0.0);
        	this->setRangeMax(1000.0);
        	this->setInitial_multiplier(-3);
        	this->setDefault_alarm_threshold(500);
        }
    }


     int getInitialMultiplierIndex() {
        return initialMultiplierIndex;
    }

     char* getMeasureUnit() {
        return measureUnit;
    }

     void setMeasureUnit(char* measureUnit) {
        this->measureUnit = measureUnit;
    }

     double getRangeMin() {
        return rangeMin;
    }

     void setRangeMin(double rangeMin){
    	 this->initialRangeMin = rangeMin;
         this->rangeMin = initialRangeMin;
    }

     double getRangeMax() {
        return initialRangeMax;
    }

     void setRangeMax(double rangeMax){
    	 this->initialRangeMax =  rangeMax;
    	 this->rangeMax = initialRangeMax;
    }

     void setAlarmThreshold(double alarmThreshold) {
        this->alarmThreshold = alarmThreshold;
    }

     double getAlarmThreshold() {
        return alarmThreshold;
    }


     void setDefault_alarm_threshold(double default_alarm_threshold){

            this->initialAlarmThreshold = default_alarm_threshold;
            this->alarmThreshold = initialAlarmThreshold;
    }

     char* getMultiplierPrefix(){
        return MULTIPLIER_PREFIX[initialMultiplierIndex];
    }


     void setInitial_multiplier(double multiplier){

    	 bool found = false;
    	 int i = 0;
    	 while(i < MULTIPLIER_SIZE && !found){
    		 if(MULTIPLIER[i] == multiplier)
    			 found = true;
            i++;
        }
        initialMultiplierIndex = i;
        multiplier_index = i;
    }


      char* getMeasureUnitCoded(char* measureUnit){

        bool found = true;
        int i = 0;
        while(i < MEASURE_UNIT_SIZE && found){
            found = strcasecmp(MEASURE_UNIT_LABEL[i], measureUnit);
            i++;
        }
        i--;
        return MEASURE_UNIT[i];
    }

     double getMultiplier() {
        return multiplier;
    }

     bool adapted = false;

     SPDataRepresentation adapt(double values, bool changeMultiplier)  {

        double k = 1000;
        double coeff = 1;
        double value = 0;

        bool flagInfinityValue= false;
        for (int i = 0; i < 1; i++) { //NUM_OF_CHIPS
            if(values[i][indexToReduce] == POSITIVE_INFINITY || values[i][indexToReduce] == NEGATIVE_INFINITY  ){
                flagInfinityValue = true;
            }
        }

        if(changeMultiplier && !flagInfinityValue) {
            for (int i = 0; i < 1; i++) { //NUM_OF_CHIPS
                value += values[i][indexToReduce];
            }
            value = value / 1; //NUM_OF_CHIPS
            value = value * multiplier;
            value = abs(value);
            if (value < 1 && value!=0) {
                do {
                    multiplier_index--;
                    value = value * k;
                    coeff = coeff * k;
                } while (value < 1);


            } else if (value >= 1000 && value!=0) {
                k = 1 / k;
                do {
                    multiplier_index++;
                    value = value * k;
                    coeff = coeff * k;
                } while (value >= 1000);

            }
            multiplier = multiplier * coeff;
            rangeMax *= coeff;
            rangeMin *= coeff;
            alarmThreshold *= coeff;
        }

        for (int i = 0; i < 1; i++) { //NUM_OF_CHIPS
            values[i][indexToReduce] *= multiplier;
        }
        adapted = true;

        SPDataRepresentation myMeasure = {measureUnit, MULTIPLIER_PREFIX[multiplier_index], rangeMin,
        									rangeMax, alarmThreshold, values, value, indexToReduce};

        	return myMeasure;
    }

    SPDataRepresentation getSPDataRepresentation(double values[][]){
        return SPDataRepresentation(measureUnit, MULTIPLIER_PREFIX[multiplier_index], rangeMin, rangeMax, alarmThreshold, values, indexToReduce, 0,false);
    }


     double transformValue(double value){
        return value*multiplier;
    }
};

*/
#endif /* IOTMAT_SPDATAREPRESENTATIONMANAGER_HPP_ */
