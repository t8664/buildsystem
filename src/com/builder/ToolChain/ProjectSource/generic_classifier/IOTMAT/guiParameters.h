/*
 * guiParameters.h
 *
 *  Created on: 17 mag 2020
 *      Author: giova
 */

#ifndef IOTMAT_GUIPARAMETERS_H_
#define IOTMAT_GUIPARAMETERS_H_

extern "C"{
#include "../variables.h"
#include "../API/level2/Items/SPParamItemMeasures.h"
}

/***** Frequencies *****/
#define	 	F_10Hz 		10.0f
#define 	F_100Hz 	100.0f
#define 	F_1KHz 		1000.0f
#define		F_100KHz 	100000.0f
#define 	F_1MHz 		1000000.0f
#define		F_DEFAULT	78125.0f

/***** First Label *****/
#define		Cp			0
#define		Cs			1
#define		Lp			2
#define		Ls			3
#define		R_p			4
#define		R_s			5
#define		Z			6
#define		G			7
#define		Y			8
#define		Vdc			9

/***** Second Label *****/
#define		D			0
#define		Q			1
#define		G_2			2
#define		Rs			3
#define		Rp			4
#define		X			5
#define		B			6
#define		THETA_DEG	7
#define		THETA_RAD	8
#define		Idc			9

/***** AutoRange Label *****/
#define	AUTORANGE_OFF	false
#define	AUTORANGE_ON	true


extern struct guiParam{
	const float fParLabelsValues[6] = {F_10Hz,  F_100Hz, F_1KHz, F_100KHz, F_1MHz, F_DEFAULT };
	char  *ctParValues[2] = {"TWO", "FOUR"};
	char  *rParValues[4]  = {"50", "500", "5000", "50000"};
	char  *igParValues[4] = {"1", "12", "20", "40"};
	char  *ogParValues[8] = {"0", "1", "2", "3", "4", "5", "6", "7"}; //V -> OutGain
	const int bParValues[1] = {0}; //BiasP -> B da mappare in Volt

	/*
	const int bParValues[24] = {0, -2048, -1024, -512, -256, -128, -64, -32, -16, -8, -4, -2, -1,
									2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2047}; //BiasP -> B da mappare in Volt
	*/
//	char outgainValuesRUN5[8][4] = {"000", "001", "010", "011", "100", "101", "110", "111"};

	const int outEIS[13] = {IN_PHASE, QUADRATURE, CONDUCTANCE, SUSCEPTANCE, MODULE,
			PHASE, RESISTANCE, CAPACITANCE, INDUCTANCE, VOLTAGE, CURRENT, DELTA_V_APPLIED, CURRENT_APPLIED};

//	const int customOUT[17] = {Cp, Cs, Lp, Ls, R, Z, G, Y, Vdc, D, Q, Rs, Rp, X, B, THETA, Idc};
	const int firstCustomOUT[10] = {Cp, Cs, Lp, Ls, R_p, R_s, Z, G, Y, Vdc};
	const int secondCustomOUT[10] = {D, Q, G_2, Rs, Rp, X, B, THETA_DEG, THETA_RAD, Idc};

	int firstOut = Cp;	//default
	int secondOut = D;	//default
//	bool check_autorange = AUTORANGE_OFF; //default
//	bool show_autorange = AUTORANGE_OFF; //default

//	bool _Cp = false;
//	bool _Cs = false;
//	bool _Lp = false;
//	bool _Ls = false;
//	bool _y = false;

}guiParam;


#endif /* IOTMAT_GUIPARAMETERS_H_ */
