/*
 * JsonStreamingParser_c.h
 *
 *  Created on: Jul 22, 2020
 *      Author: zen
 */

#if !defined(JSON_PARSER_C_JSONSTREAMINGPARSER_C_H_)
#define JSON_PARSER_C_JSONSTREAMINGPARSER_C_H_

#include <stdbool.h>

#include "../MCU_UTILITY/JsonListener_c.h"

#define STATE_START_DOCUMENT     0
#define STATE_DONE               -1
#define STATE_IN_ARRAY           1
#define STATE_IN_OBJECT          2
#define STATE_END_KEY            3
#define STATE_AFTER_KEY          4
#define STATE_IN_STRING          5
#define STATE_START_ESCAPE       6
#define STATE_UNICODE            7
#define STATE_IN_NUMBER          8
#define STATE_IN_TRUE            9
#define STATE_IN_FALSE           10
#define STATE_IN_NULL            11
#define STATE_AFTER_VALUE        12
#define STATE_UNICODE_SURROGATE  13

#define STACK_OBJECT             0
#define STACK_ARRAY              1
#define STACK_KEY                2
#define STACK_STRING             3

#define BUFFER_MAX_LENGTH  512

typedef struct JsonStreamingParser_c JsonStreamingParser_c;

struct JsonStreamingParser_c{
	/* Don't call directly -> private  */
	/* Structure variables */
    int _state;
    int _stack[20];
    int _stackPos;
   JsonListener_c* myListener;
    //testSteamingParser_c* myListener;
    bool _doEmitWhitespace;
    /* Fixed length buffer array to prepare for c code */
    char _buffer[BUFFER_MAX_LENGTH];
    int _bufferPos;
    char _unicodeEscapeBuffer[10];
    int _unicodeEscapeBufferPos;
    char _unicodeBuffer[10];
    int _unicodeBufferPos;
    int _characterCounter;
    int _unicodeHighSurrogate;

    /* Function pointers */
    void (*_increaseBufferPointer)(JsonStreamingParser_c* this_c);
    void (*_endString)(JsonStreamingParser_c* this_c);
    void (*_endArray)(JsonStreamingParser_c* this_c);
    void (*_startValue)(JsonStreamingParser_c* this_c, char c);
    void (*_startKey)(JsonStreamingParser_c* this_c);
    void (*_processEscapeCharacters)(JsonStreamingParser_c* this_c, char c);
    bool (*_isDigit)(JsonStreamingParser_c* this_c, char c);
    bool (*_isHexCharacter)(JsonStreamingParser_c* this_c, char c);
    char (*_convertCodepointToCharacter)(JsonStreamingParser_c* this_c, int num);
    void (*_endUnicodeCharacter)(JsonStreamingParser_c* this_c, int codepoint);
    void (*_startNumber)(JsonStreamingParser_c* this_c, char c);
    void (*_startString)(JsonStreamingParser_c* this_c);
    void (*_startObject)(JsonStreamingParser_c* this_c);
    void (*_startArray)(JsonStreamingParser_c* this_c);
    void (*_endNull)(JsonStreamingParser_c* this_c);
    void (*_endFalse)(JsonStreamingParser_c* this_c);
    void (*_endTrue)(JsonStreamingParser_c* this_c);
    void (*_endDocument)(JsonStreamingParser_c* this_c);
    int (*_convertDecimalBufferToInt)(JsonStreamingParser_c* this_c, char myArray[], int length);
    void (*_endNumber)(JsonStreamingParser_c* this_c);
    void (*_endUnicodeSurrogateInterstitial)(JsonStreamingParser_c* this_c);
    bool (*_doesCharArrayContain)(JsonStreamingParser_c* this_c, char myArray[], int length, char c);
    int (*_getHexArrayAsDecimal)(JsonStreamingParser_c* this_c, char hexArray[], int length);
    void (*_processUnicodeCharacter)(JsonStreamingParser_c* this_c, char c);
    void (*_endObject)(JsonStreamingParser_c* this_c);

    /* Access functions -> public  */
    void (*parse)(JsonStreamingParser_c* this_c, char c);
    void (*setListener)(JsonStreamingParser_c* this_c, JsonListener_c* listener);
    void (*reset)(JsonStreamingParser_c*);
};

void initJsonStreamingParser(JsonStreamingParser_c* this_c);



#endif /* JSON_PARSER_C_JSONSTREAMINGPARSER_C_H_ */
