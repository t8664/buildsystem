
/*
 * fileHandlerMCU.cpp
 *
 *  Created on: 16 mar 2019
 *      Author: Luca
 */


extern "C"{
#include "../EXT_OPERATION/fileHandlerMCU.h"
#include "JsonListener_c.h"
#include "JsonStreamingParser_c.h"
}

#if defined (ARDUINO)

//#include "init.h"
#include "SPIFFS.h"
//#include "testStreamingParser.h"
//#include "JsonStreamingParser.h"

//typedef struct test_struct jsonTest;

int writeFile(unsigned char value){
	int out = -1;
	SPIFFS.begin(true);
	File file = SPIFFS.open("/data.txt", "a");
	if(!file){
		//Serial.println("ERRORE CREAZIONE FILE");
		return out;
	}
	//Serial.printf("Scrivo: %c \n", value);
	out = file.print((char)value);
	file.close();
	//SPIFFS.end();

	return out;
}

void readFile(){

	SPIFFS.begin(true);
	File file = SPIFFS.open("/data.txt", "r");
	if(!file){
		//Serial.println("ERRORE APERTURA FILE");
		return;
	}
//	JsonStreamingParser parser;
//	TestJsonListener listener;
//	parser.setListener(&listener);
//
//	while(file.available()){
//		parser.parse(file.read());
//	}

	JsonStreamingParser_c parser;
	initJsonStreamingParser(&parser);
    JsonListener_c listener;
    initJsonListener(&listener);
    parser.setListener(&parser, &listener);

    while(file.available()){
    	parser.parse(&parser, file.read());
    	//Serial.print("a");
    }
//	file.seek(0, SeekSet);
	//Serial.println("File Content:");
	//while(file.available()){
	//	Serial.write(file.read());
	//}
	//file.seek(0, SeekSet); //riporto il puntatore all'inizio del file
//	Serial.print("File size:");
//	Serial.println(file.size());
	file.close();

}

void deleteFile(){
	SPIFFS.begin(true);
	if(SPIFFS.exists("/data.txt")){
		SPIFFS.remove("/data.txt");
	} else {
		Serial.println("FILE INESISTENTE!!");
	}
}

void formatFile(){
	SPIFFS.begin(true);
	SPIFFS.format();
}

#endif

