/*
 * initSPParameters.c
 *
 *  Created on: 31 mar 2019
 *      Author: Luca
 */


#include "initSPParameters.h"

void parseSPConfiguration(const char* key, const char* value){
	if(strcmp(key, "description") == 0){
		strcpy(spConfiguration.description, value);
	} else if (strcmp(key, "hostcontroller") == 0){
		strcpy(spConfiguration.host_controller, value);
	} else if(strcmp(key, "apiowner") == 0){
		strcpy(spConfiguration.api_owner, value);
	} else if(strcmp(key, "mcu") == 0){
		strcpy(spConfiguration.mcu, value);
		if(strcmp(value, "ESP8266") == 0){
			//initSPProtocolESP8266();
		}
	} else if(strcmp(key, "protocol") == 0){
//		if(strcmp(value, "SENSIBUS") == 0){
//			spConfiguration.protocol = &spProtocol;
//		}
	} else if(strcmp(key, "addressingtype") == 0){
		strcpy(spConfiguration.addressingType, value);
	} else if(strcmp(key, "clusterid") == 0){
		spConfiguration.cluster = &spCluster;
	} else if(strcmp(key, "vcc") == 0){
		strcpy(spConfiguration.vccStringValue, value);
	}
}

void parseSPCluster(const char* key, const char* value){
	if(strcmp(key, "clusterid") == 0){
		strcpy(spCluster.clusterID, value);
	} else if (strcmp(key, "multicastcluster") == 0){
		strcpy(spCluster.multicastAddress, value);
	}
}

void parseSPFamily(const char* key, const char* value){
	if(strcmp(key, "familyname") == 0){
		strcpy(spFamily.name, value);
		spFamily.measureTypeDim = 0;
	} else if(strcmp(key, "familyid") == 0){
		strcpy(spFamily.id, value);
	} else if (strcmp(key, "hwversion") == 0){
		strcpy(spFamily.hwVersion, value);
	} else if (strcmp(key, "sysclock") == 0){
		spFamily.sysClock = atoi(value);
	} else if (strcmp(key, "osctrim") == 0){
		strcpy(spFamily.oscTrim, value);
	} else if(strcmp(key, "measuretype") == 0){
		strcpy(spFamily.measureTypesList[spFamily.measureTypeDim++], value);
	} else if(strcmp(key, "multicastdefault") == 0){
		strcpy(spFamily.multicastDefaultAddress, value);
	} else if(strcmp(key, "broadcast") == 0){
		strcpy(spFamily.broadcastAddress, value);
		strcpy(spCluster.broadcastAddress, value);
	} else if(strcmp(key, "broadcastslow") == 0){
		strcpy(spFamily.broadcastSlowAddress, value);
		strcpy(spCluster.broadcastSlowAddress, value);
	}
}

void parseSPChip(const char* key, const char* value, int chipCount){
	if(strcmp(key, "familyid") == 0){
		if(strcmp(spFamily.id, value) == 0)
			spCluster.chipList[chipCount].family = &spFamily;
	} else if(strcmp(key, "serialnumber") == 0){
		//strcpy(spCluster.chipList[chipCount].serialNumber, value);
		setSerialNumber(&spCluster.chipList[chipCount], value);
	} else if (strcmp(key, "i2CADDRESS") == 0){
		strcpy(spCluster.chipList[chipCount].i2cAddress, value);
	} else if (strcmp(key, "osctrim") == 0){
		strcpy(spCluster.chipList[chipCount].oscTrimOverride, spCluster.chipList[chipCount].family->oscTrim);
	}
}

void parseSPSensingElement(const char* key, const char* value, int count){
	if(strcmp(key, "rsense") == 0 && strcmp(value, "null") != 0){
		strcpy(spSensingElement[count].rsense, value);
	} else if(strcmp(key, "name") == 0){
		strcpy(spSensingElement[count].name, value);
	} else if(strcmp(key, "inGain") == 0 && strcmp(value, "null") != 0){
		strcpy(spSensingElement[count].ingain, value);
	} else if(strcmp(key, "outGain") == 0 && strcmp(value, "null") != 0){
		strcpy(spSensingElement[count].outgain, value);
	} else if(strcmp(key, "contacts") == 0 && strcmp(value, "null") != 0){
		strcpy(spSensingElement[count].contacts, value);
	} else if(strcmp(key, "frequency") == 0 && strcmp(value, "null") != 0){
		spSensingElement[count].frequency = atof(value);
	} else if(strcmp(key, "modeVI") == 0 && strcmp(value, "null") != 0){
		strcpy(spSensingElement[count].modeVI, value);
	} else if(strcmp(key, "measureTechnique") == 0){
		strcpy(spSensingElement[count].measureTecnique, value);
	} else if(strcmp(key, "measureType") == 0 && strcmp(value, "null") != 0){
		strcpy(spSensingElement[count].measureType, value);
	} else if(strcmp(key, "filter") == 0){
		strcpy(spSensingElement[count].filter, value);
	} else if(strcmp(key, "phaseShiftMode") == 0 && strcmp(value, "null") != 0){
		strcpy(spSensingElement[count].phaseShiftMode, value);
	} else if(strcmp(key, "phaseShift") == 0 && strcmp(value, "null") != 0){
		spSensingElement[count].phaseShift = atoi(value);
	} else if(strcmp(key, "iq") == 0 && strcmp(value, "null") != 0){
		strcpy(spSensingElement[count].iq, value);
	} else if(strcmp(key, "conversionRate") == 0){
		spSensingElement[count].conversionRate = atoi(value);
	} else if(strcmp(key, "inportADC") == 0){
		strcpy(spSensingElement[count].inportADC, value);
	} else if(strcmp(key, "measureUnit") == 0){
		strcpy(spSensingElement[count].measureUnit, value);
	} else if(strcmp(key, "rangeMin") == 0){
		spSensingElement[count].rangeMin = atof(value);
	} else if(strcmp(key, "rangeMax") == 0){
		spSensingElement[count].rangeMax = atof(value);
	} else if(strcmp(key, "alarmThreshold") == 0){
		spSensingElement[count].defaultAlarmThreshold = atof(value);
	} else if(strcmp(key, "multiplier") == 0){
		spSensingElement[count].multipier = atof(value);
	} else if(strcmp(key, "ndata") == 0){
		spSensingElement[count].NData = atoi(value);
	} else if(strcmp(key, "harmonic") == 0 && strcmp(value, "null") != 0){
		strcpy(spSensingElement[count].harmonic, value);
	} else if(strcmp(key, "dcbiasP") == 0 && strcmp(value, "null") != 0){
		spSensingElement[count].dcBiasP = atoi(value);
	} else if(strcmp(key, "dcbiasN") == 0 && strcmp(value, "null") != 0){
		spSensingElement[count].dcBiasN = atoi(value);
	}
}

int findPort(const char* value){
	if(strcmp(value, "PORT0") == 0){
		return PORT0;
	} else if(strcmp(value, "PORT1") == 0){
		return PORT1;
	} else if(strcmp(value, "PORT2") == 0){
		return PORT2;
	} else if(strcmp(value, "PORT3") == 0){
		return PORT3;
	} else if(strcmp(value, "PORT4") == 0){
		return PORT4;
	} else if(strcmp(value, "PORT5") == 0){
		return PORT5;
	} else if(strcmp(value, "PORT6") == 0){
		return PORT6;
	} else if(strcmp(value, "PORT7") == 0){
		return PORT7;
	} else if(strcmp(value, "PORT8") == 0){
		return PORT8;
	} else if(strcmp(value, "PORT9") == 0){
		return PORT9;
	} else if(strcmp(value, "PORT10") == 0){
		return PORT10;
	} else if(strcmp(value, "PORT11") == 0){
		return PORT11;
	} else if(strcmp(value, "PORT_HP") == 0){
		return PORT_HP;
	} else if(strcmp(value, "PORT_EXT1") == 0){
		return PORT_EXT1;
	} else if(strcmp(value, "PORT_EXT2") == 0){
		return PORT_EXT2;
	} else if(strcmp(value, "PORT_EXT3") == 0){
		return PORT_EXT3;
	} else if(strcmp(value, "PORT_TEMPERATURE") == 0){
		return PORT_TEMPERATURE;
	} else if(strcmp(value, "PORT_VOLTAGE") == 0){
		return PORT_VOLTAGE;
	} else if(strcmp(value, "PORT_LIGHT") == 0){
		return PORT_LIGHT;
	} else if(strcmp(value, "PORT_DARK") == 0){
		return PORT_DARK;
	} else if(strcmp(value, "PORT_NA") == 0){
		return PORT_NA;
	}
	return -1;
}

void parseSPSensingElementOnFamily(const char* key, const char* value, int count){
	if(strcmp(key, "sensingelementid") == 0){
		strcpy(spSeOnFamily[count].id, value);
	} else if(strcmp(key, "sensingelementname") == 0){
		spSeOnFamily[count].spSensingElement = &spSensingElement[count];
	} else if(strcmp(key, "sensingelementport") == 0){
		spSeOnFamily[count].port = &spPortList[findPort(value)];
	}
}

int findSeOnFamily(const char* value){
	for(int i = 0; i < NUM_SE_ON_FAMILY; i++){  //30
		if(strcmp(spSeOnFamily[i].id, value) == 0){
			return i;
		}
	}
	return -1;
}

void parseSPSensingElementOnChip(const char* key, const char* value, int chipCount, int seOnChipCount){
	if(strcmp(key, "sensingelementid") == 0){
		spSeOnChip[chipCount][seOnChipCount].spSensingElementOnFamily = &spSeOnFamily[findSeOnFamily(value)];
	}
}

void parseSPCalibrationParameters(const char* key, const char* value,  int chipCount, int seOnChipCount){
	if(strcmp(key, "m") == 0){
		spSeOnChip[chipCount][seOnChipCount].spCalibrationParameters.m = atof(value);
	} else if(strcmp(key, "n") == 0){
		spSeOnChip[chipCount][seOnChipCount].spCalibrationParameters.n = atof(value);
	} else if(strcmp(key, "threshold") == 0 && strcmp(value, "null") != 0){
		spSeOnChip[chipCount][seOnChipCount].spCalibrationParameters.threshold = atof(value);
	}
}
