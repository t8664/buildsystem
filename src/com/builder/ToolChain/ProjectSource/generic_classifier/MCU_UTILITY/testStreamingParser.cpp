/*
 * testStreamingParser.cpp
 *
 *  Created on: 25 mar 2019
 *      Author: Luca
 */


#include "testStreamingParser.h"

int seCounter = -1;
int seOnChipCounter = -1;
int chipCounter = -1;
int calParamCounter = -1;

TestJsonListener::TestJsonListener(){
	this->lastkey = "";
	this->currentState = START_OBJECT;
	this->arrayState = NO_ARRAY;
	this->allSeSet = false;
	this->allSeOnChipSet = false;
	this->allSeOnFamilySet = false;
}

void TestJsonListener::whitespace(char c) {
}

void TestJsonListener::key(String key) {
	this->lastkey = key;
}

void TestJsonListener::value(String value) {
	switch(this->currentState){
	case IN_CONFIGURATION:
		parseSPConfiguration(this->lastkey.c_str(), value.c_str());
		break;
	case IN_CLUSTER:
		parseSPCluster(this->lastkey.c_str(), value.c_str());
		break;
	case IN_CHIP:
		parseSPChip(this->lastkey.c_str(), value.c_str(), chipCounter);
		break;
	case IN_FAMILY:
		parseSPFamily(this->lastkey.c_str(), value.c_str());
		break;
	case IN_SENSING_ELEMENT:
		if(!this->allSeOnFamilySet){
			parseSPSensingElement(this->lastkey.c_str(), value.c_str(), seCounter);
		}
		break;
	case IN_SE_ON_CHIP:
		parseSPSensingElementOnChip(this->lastkey.c_str(), value.c_str(), chipCounter, seOnChipCounter);
		break;
	case IN_CALIBRATION_PARAMETER:
		parseSPCalibrationParameters(this->lastkey.c_str(), value.c_str(), chipCounter, seOnChipCounter);


		break;
	case IN_SE_ON_FAMILY:
		if(!this->allSeOnFamilySet){
			parseSPSensingElementOnFamily(this->lastkey.c_str(), value.c_str(), seCounter);
		}
		break;
	default:
		break;
	}
}

void TestJsonListener::startDocument() {

}

void TestJsonListener::endDocument() {

	spCluster.dimChipList = chipCounter+1;

//	Serial.printf("Family: %s  ID: %s\n", spCluster.familyOfChips->name, spCluster.familyOfChips->id);
//	Serial.printf("Family measureType: %s, %s, %s\n",spCluster.familyOfChips->measureTypesList[0],
//			spCluster.familyOfChips->measureTypesList[1], spCluster.familyOfChips->measureTypesList[2]);
//	Serial.printf("sensingelementonfamily: \n");
//	for(int i = 0; i < 25; i++){
//		Serial.printf("SensingElementOnFamily: %s \n", spCluster.familyOfChips->spSensingElementOnFamilyList[i].id);
//		Serial.printf("SensingElementOnFamily port LABEL: %s \n", spCluster.familyOfChips->spSensingElementOnFamilyList[i].port->portLabel);
//		Serial.printf("SensingElementOnFamily port VALUE: %s \n", spCluster.familyOfChips->spSensingElementOnFamilyList[i].port->portValue);
//		Serial.printf("SensingElementOnFamily sensing Element Name: %s \n", spFamily.spSensingElementOnFamilyList[i].spSensingElement->name);
//		Serial.printf("SensingElementOnFamily sensing Element Rsense: %s \n", spFamily.spSensingElementOnFamilyList[i].spSensingElement->rsense);
//	}
//	Serial.printf("\n\nSensingElementOnChip (chip 1): \n");
//	for(int i = 0; i < 16; i++){
//		Serial.printf("SensingElementOnChip: %s \n", spCluster.chipList[0].spSensingElementOnChipList[i].spSensingElementOnFamily->id);
//		Serial.printf("SensingElementOnChip m: %f \n", spCluster.chipList[0].spSensingElementOnChipList[i].spCalibrationParameters.m);
//		Serial.printf("SensingElementOnChip n: %f \n", spCluster.chipList[0].spSensingElementOnChipList[i].spCalibrationParameters.n);
//	}
//	Serial.printf("\n\nSensingElementOnChip (chip 2): \n");
//	for(int i = 0; i < 16; i++){
//		Serial.printf("SensingElementOnChip: %s \n", spCluster.chipList[1].spSensingElementOnChipList[i].spSensingElementOnFamily->id);
//		Serial.printf("SensingElementOnChip m: %f \n", spCluster.chipList[1].spSensingElementOnChipList[i].spCalibrationParameters.m);
//		Serial.printf("SensingElementOnChip n: %f \n", spCluster.chipList[1].spSensingElementOnChipList[i].spCalibrationParameters.n);
//	}

	chipCounter = -1;
}

void TestJsonListener::startArray() {
	switch(this->currentState){
	case IN_CLUSTER:
		if(this->lastkey.equals("chip")){
			this->currentState = IN_CHIPLIST;
			this->arrayState = IN_CHIPLIST;
		}
		break;
	case IN_FAMILY:
		if (this->lastkey.equals("sensingelementonfamily")){
			this->currentState = IN_SE_ON_FAMILY_LIST;
			this->arrayState = IN_SE_ON_FAMILY_LIST;
		} else if(this->lastkey.equals("analyte")){
			this->currentState = IN_ANALYTE_LIST;
			this->arrayState = IN_ANALYTE_LIST;
		}
		break;
	case IN_CHIP:
		if (this->lastkey.equals("sensingelementonchip")){
			this->currentState = IN_SE_ON_CHIP_LIST;
			this->arrayState = IN_SE_ON_CHIP_LIST;
		}
		break;

	default:
		break;
	}

}

void TestJsonListener::endArray() {
	switch(this->currentState){
	case IN_SE_ON_FAMILY_LIST:
		this->currentState = IN_FAMILY;
		this->arrayState = IN_CHIPLIST;
		this->allSeOnFamilySet = true;
		//Serial.printf("%d se on family!!!\n", seCounter+1);
		break;
	case IN_ANALYTE_LIST:
		this->currentState = IN_FAMILY;
		this->arrayState = IN_CHIPLIST;
		break;
	case IN_SE_ON_CHIP_LIST:
		this->currentState = IN_CHIP;
		this->arrayState = IN_CHIPLIST;
		//this->allSeOnChipSet = true;
		//Serial.printf("%d se on chip!!!\n", seOnChipCounter);
		//Serial.printf("%d calibration parameters!!!\n", calParamCounter+1);
		break;
	case IN_CHIPLIST:
		this->currentState = IN_CLUSTER;
		this->arrayState = NO_ARRAY;
		//Serial.printf("\n\nCHIP TOTALI %d\n\n", chipCounter+1);
		//Serial.printf("\n\n\n\n****************************************FINE CHIP LIST*************************************\n\n\n\n");
		//chipCounter = 0;
		break;
	default:
		break;
	}
}


void TestJsonListener::startObject() {
	switch(this->currentState){
	case START_OBJECT:
		this->currentState = IN_CONFIGURATION;
		break;
	case IN_CONFIGURATION:
		if(this->lastkey.equals("cluster")){
			this->currentState = IN_CLUSTER;
		}
		break;
	case IN_CHIPLIST:
		this->currentState = IN_CHIP;
		chipCounter++;
		//Serial.printf("******************ENTRO NEL CHIP %d******************\n",chipCounter+1);
		seCounter = seOnChipCounter = calParamCounter = -1;
		break;
	case IN_CHIP:
		if(this->lastkey.equals("spFamily")){
			this->currentState = IN_FAMILY;
		}
		break;
	case IN_SE_ON_FAMILY_LIST:
		this->currentState = IN_SE_ON_FAMILY;
		seCounter++;
		break;
	case IN_SE_ON_FAMILY:
		if(this->lastkey.equals("sensingElement")){
			this->currentState = IN_SENSING_ELEMENT;
		}
		break;
	case IN_ANALYTE_LIST:
		this->currentState = IN_ANALYTE;
		break;
	case IN_SE_ON_CHIP_LIST:
		this->currentState = IN_SE_ON_CHIP;
		seOnChipCounter++;
		break;
	case IN_SE_ON_CHIP:
		if(this->lastkey.equals("sensingElementOnFamily")){
			this->currentState = IN_SE_ON_FAMILY;
		} else if(this->lastkey.equals("calibrationparameter")){
			this->currentState = IN_CALIBRATION_PARAMETER;
			calParamCounter++;
		}
		break;
	case IN_CLUSTER:
		this->currentState = IN_CHIP;
		break;
	default:
		break;
	}
}

void TestJsonListener::endObject() {
	switch(this->currentState){
	case IN_SENSING_ELEMENT:
		this->currentState = IN_SE_ON_FAMILY;
		break;
	case IN_SE_ON_FAMILY:
		if(this->arrayState == IN_SE_ON_FAMILY_LIST){
			this->currentState = IN_SE_ON_FAMILY_LIST;
		} else if(this->arrayState == IN_SE_ON_CHIP_LIST){
			this->currentState = IN_SE_ON_CHIP;
		}
		break;
	case IN_ANALYTE:
		this->currentState = IN_ANALYTE_LIST;
		break;
	case IN_FAMILY:
		this->currentState = IN_CHIP;
		this->allSeSet = true;
		break;
	case IN_CALIBRATION_PARAMETER:
		this->currentState = IN_SE_ON_CHIP;
		break;
	case IN_SE_ON_CHIP:
		this->currentState = IN_SE_ON_CHIP_LIST;
		break;
	case IN_CHIP:
		this->currentState = IN_CHIPLIST;
		break;
	case IN_CLUSTER:
		this->currentState = IN_CONFIGURATION;
		break;
	default:
		break;
	}
}
