/*
 * testStreamingParser.h
 *
 *  Created on: 25 mar 2019
 *      Author: Luca
 */

#ifndef TESTSTREAMINGPARSER_H_
#define TESTSTREAMINGPARSER_H_

#include "JsonListener.h"

extern "C"{
#include "initSPParameters.h"
}

#define NO_ARRAY					0

#define START_OBJECT			 	0
#define IN_CONFIGURATION		 	1
#define IN_CLUSTER			 		2
#define IN_CHIPLIST			 		3
#define IN_CHIP				 		4
#define IN_FAMILY			 		5
#define IN_SE_ON_FAMILY_LIST		6
#define IN_SE_ON_FAMILY				7
#define IN_SENSING_ELEMENT	  	    8
#define IN_ANALYTE_LIST		   	    9
#define IN_ANALYTE				   10
#define IN_SE_ON_CHIP_LIST		   11
#define IN_SE_ON_CHIP			   12
#define IN_CALIBRATION_PARAMETER   13
#define IN_PORT					   14

class TestJsonListener: public JsonListener {

private:

	String lastkey;
	int currentState;
	int arrayState;
	bool allSeSet;
	bool allSeOnChipSet;
	bool allSeOnFamilySet;

public:

	TestJsonListener();

	virtual void whitespace(char c);

	virtual void startDocument();

	virtual void key(String key);

	virtual void value(String value);

	virtual void endArray();

	virtual void endObject();

	virtual void endDocument();

	virtual void startArray();

	virtual void startObject();
};




#endif /* TESTSTREAMINGPARSER_H_ */
