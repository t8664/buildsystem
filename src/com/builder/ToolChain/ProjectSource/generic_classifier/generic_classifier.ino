#ifdef ESP8266                  // ESP8266
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WiFiClient.h>
// Hardcode WiFi parameters as this isn't going to be moving around.
ESP8266WiFiMulti wifiMulti;     // Create an instance of the ESP8266WiFiMulti class, called 'wifiMulti'


#else                           // ESP32
#include <WiFi.h>
#include <WiFiMulti.h>
WiFiMulti wifiMulti;
#endif

#include "DRIVER/ESP8266/StatusSENSIBUS.h"
#include "DRIVER/ESP8266/MsgSENSIBUS.h"

#include "DRIVER/ESP8266/CommSENSIBUS.h"
#include "DRIVER/ESP8266/CommUSB.h"

#include "IOTMAT/Menu.hpp"
#include "IOTMAT/IOTMatConfig.h"
#include <Preferences.h>  /* store/load configuration */

/*API SECTION*/
#include "SPIFFS.h"
//#include "ardPrint.h"
#include "DRIVER/ESP8266/CommSENSIBUS.h"
#include "DRIVER/ESP8266/CommUSB.h"
#include "DRIVER/ESP8266/MsgSENSIBUS.h"
#include "DRIVER/ESP8266/StatusSENSIBUS.h"
//#include "MCU_UTILITY/init.h"
#include "task.h"

extern "C"{
#include "EXT_OPERATION/set_SPMeasuramentParameterEIS.h"
#include "EXT_OPERATION/fileHandlerMCU.h"
#include "API/level1/decoder/SPDecoderSensibus.h"
#include "API/level1/protocols/esp8266/SPProtocolESP8266_SENSIBUS.h"
#include "DRIVER/ESP8266/SendData.h"
#include "variables.h"
}

//extern "C" {
//#include "classifier/nn.h"
//}
////inizializzazione rete neurale
//neural_network my_nn; 										// Rete neurale allocata
//neural_network * nn = &(my_nn);								// Puntatore alla rete neurale
//nn_data input_vect[8];

_DECLARATION_CLASSIFIER_

int instanceID;
CommSENSIBUS commSENSIBUS(DATI);
StatusSENSIBUS statusSENSIBUS;
MsgSENSIBUS msgSENSIBUS;
char chace[NUM_OF_INSTRUCTIONS][17] = {'\0'};

char sensorName[SENSOR_TYPE_CHARNUMBER];
/*API SECTION   END*/



void setup() {

	/*
	 *  Configurazione della comunicazione seriale su USB
	 * dove verranno stampati i valori delle misure
	 */
	Serial.begin(115200);

	/*
	 * Configurazione pin GPIO
	 */
	pinMode(SENSIBUS_VCC, OUTPUT_OPEN_DRAIN);
	digitalWrite(SENSIBUS_VCC, HIGH);
	pinMode(LED_BUILTIN, OUTPUT); // led

	/*************INIZIALIZZAZZIONE API*************/

	/*
	 * Inizializzazione strutture dati: spPort, spFamily, spCluster e spDecoder
	 */
	initSPVariables();
	/*
	 * Operazione che prevede il caricamento della porzione del LayoutConfiguration
	 * contenente le informazioni relative al chip presente.
	 * Queste informazioni sono definite all'interno di un file json che risiede nella flash
	 * del dispositivo( in questo caso contenuto all'intero di una partizione "SPIFFS")
	 */
	readFile();

	/*
	 * Imposto la tipologia di indirizzamento ( per la comunicazione con i chip SENSIPLUS)
	 * a partire dalle informazioni ottenute precedentemente
	 */
	if (strcmp(spConfiguration.addressingType, "ShortAddress") == 0){
		ADDRESSING_MODE = SHORT_ADDRESS;
	} else if (strcmp(spConfiguration.addressingType, "NoAddress") == 0){
		ADDRESSING_MODE = NO_ADDRESS;
	} else {
		ADDRESSING_MODE = FULL_ADDRESS;
	}

	/*
	 * Completamento della fase di inizializzazione
	 * dove vengono associate e riempite le restanti strutture dati
	 */
	fillChip();
	fillConfiguration();
	initSPProtocolESP8266();
	initAllSPMeasurements();
	/* Spengo il led per comunicare che è finita la fase di inizializzazione */
	WRITE_HIGH(LED_BUILTIN);

	/* Inizializzazione dei chip ( prevede la comunicazione con quest'ultimi) */
	open_connection();
	delay(5);

	/*************CONFIGURAZIONE PARAMETRI SENSORE (spParamSENSOR) *************/

	//instanceID = 0;
	//strcpy(sensorName, "ONCHIP_TEMPERATURE");
	//strcpy(sensorName, "OFFCHIP_ORP");
	//strcpy(sensorName, "OFFCHIP_SPECIFIC_CONDUCTIVITY");
	/* Imposto la label associata al sensore da voler utilizzare */
	//strcpy(spParamSENSOR.sensorName, sensorName);
	/*  A partire dal nome del sensore ottengo i parametri del Sensing Element voluto (ONCHIP_TEMPERATURE) */
	//Serial.println("PRIMA SENSORE #######");
	//SPSensingElementOnChip* seOnChip = spConfiguration.searchSPSensingElementOnChip(&spConfiguration, sensorName);
	//Serial.println("DOPO SENSORE #######");
	//SPSensingElement* sensingElement = (seOnChip->spSensingElementOnFamily->spSensingElement);
	/* Associo la porta corretta per il sensore voluto */
	//strcpy(spParamSENSOR.port, seOnChip->spSensingElementOnFamily->port->portValue);
	/**/
	//spMeasurementRUNX[instanceID].spMeasurement->measureIndexToSave = -1;
	//strcpy(spParamSENSOR.filter, sensingElement->filter);
	//strcpy(spParamSENSOR.filter, "16");  /*Come il batch */
	/* Dato che è un sensore "DIRECT" non è necessario utilizzare EIS */
  //spParamSENSOR.paramInternalEIS = NULL;
	/**/
	//qistatus[instanceID].status.dimBuffer = atoi(spParamSENSOR.filter);
	//qistatus[instanceID].status.dimBuffer = 16;

	/*
	* Una volta configurato SpParamSENSOR si procede
	 * con l'effettivo utilizzo dello stesso, quindi configurando i chip
	 * per effettuare la misura voluta
	 */
	//spMeasurementRUNX[instanceID].setSENSOR(&spMeasurementRUNX[instanceID], &spParamSENSOR, NULL);

	_INIT_SENSOR_

	/* Prima misura e reset cache- */
	for(int i = 0; i < NUM_OF_MEASUREMENT; i++){
		qistatus[instanceID].status.firstMeasure = true;
	}

	for(int i = 0; i < NUM_OF_INSTRUCTIONS; i++){
		strcpy(chace[i], "\0");
	}


//	init_neural_network(nn);		// Inizializza la rete neurale

	_INIT_CLASSIFIER_
}


void loop() {

	/* Effettuo la misura di temperatura e ne stampo il valore */

//	 spMeasurementRUNX[instanceID].getSENSOR(&spMeasurementRUNX[instanceID], outValues.outTEMP, &spParamSENSOR);
//	 Serial.print("OFFCHIP_ORP: ");
//	 Serial.print(outValues.outTEMP[0][0],10);
//	 Serial.println("mV ");


//	 for(int i =0; i<8; i++){
//		 spMeasurementRUNX[instanceID].getSENSOR(&spMeasurementRUNX[instanceID], outValues.outTEMP, &spParamSENSOR);
//		 input_vect[i] = outValues.outTEMP[0][0];
//	 }

  _MEASURE_


	// set_input(nn, &input_vect[0] );
	//predict(nn);	// Predice la classe sui valori di input impostati
	 //int found_class = compute_class_found(nn); 		// Mi dice la classe trovata
	 _INFERENCE_


	//Serial.println(found_class);
	 _RESULT_


	 delay(2000);


}

void sendData(uint8 header, uint8 command, uint8* addressByte, short dimAddress,
		uint8* data, short dimData, char* out){

	//Serial.println("...INIZIO DRIVER....");

	/*****INIZIALIZZAZIONE VARIABILI*****/

	//	Serial.print("CURRENT SPEED: 0x");
	//	Serial.println(speed, HEX);
	msgSENSIBUS.reset();
	msgSENSIBUS.command = (unsigned)header;

	if(msgSENSIBUS.command & EXT_INSTR){
		// TODO: manage ext instruction
		msgSENSIBUS.instructionType = msgSENSIBUS.command;
	} else {
		// Manage normal instruction
		//*** Receive address
		statusSENSIBUS.addressingMode = msgSENSIBUS.command & ADDRESS_MODE_MASK;

		if (statusSENSIBUS.addressingMode == LastAddressValue){
			statusSENSIBUS.addressingMode = statusSENSIBUS.lastAddressingMode;
		} else {
			statusSENSIBUS.lastAddressingMode = statusSENSIBUS.addressingMode;
			if (statusSENSIBUS.addressingMode == NoAddressValue) {
				// No addressing byte to receive
				msgSENSIBUS.chipAddressLength = 0;

			} else if (statusSENSIBUS.addressingMode == ShortAddressValue) {
				msgSENSIBUS.chipAddressLength = 1;
			} else if (statusSENSIBUS.addressingMode == FullAddressValue) {
				msgSENSIBUS.chipAddressLength = 6;
			}
		}
		msgSENSIBUS.dataRcvLength = msgSENSIBUS.command & DATA_LENGTH_MASK;
		msgSENSIBUS.instructionType = NORMAL_INSTR;
	}
	//TODO: definire caso NO_INSTR

	if(msgSENSIBUS.instructionType & EXT_INSTR){
		//TODO: caso istruzione estesa da implementare.

	} else { //NORMAL INSTRUCTION
		//Receive address
		msgSENSIBUS.chipAddressLength = dimAddress;
		for(int i = 0; i < dimAddress; i++){
			msgSENSIBUS.chipAddress[i] = (unsigned)addressByte[i];
		}
		msgSENSIBUS.registerAddress[0] = (unsigned)command;
		// Establish if is a read or write operation (1 read, 0 false)
		statusSENSIBUS.isRead = IS_WRITE_MASK & msgSENSIBUS.registerAddress[0];
		//**** Receive data
		if (statusSENSIBUS.isRead){ //READ
			for(int i = 0; i < msgSENSIBUS.dataRcvLength; i++){
				msgSENSIBUS.dataRcv[i] = 0xFF;	// FF in order to wait for data
			}
		} else { //WRITE
			for(int i = 0; i < msgSENSIBUS.dataRcvLength; i++){
				msgSENSIBUS.dataRcv[i] = (unsigned)data[i];
			}
		}
	}

	/******INIZIO STAMPA*****/

	//	Serial.print("comando: 0x");
	//	Serial.println(SBMsg.command, HEX);
	//	Serial.print("intruction type: 0x");
	//	Serial.println(SBMsg.instructionType, HEX);
	//	Serial.print("Addressing Mode: 0x");
	//	Serial.println(status.addressingMode, HEX);
	//	Serial.print("Register Address: 0x");
	//	Serial.println(SBMsg.registerAddress[0], HEX);

	//long start = millis();
	commSENSIBUS.transaction(msgSENSIBUS, statusSENSIBUS);
	//long elapsed = millis() - start;
	//totalDriver += elapsed;

	char appo[5];
	if(out != NULL){
		out[0] = '\0';
		strcpy(out, "[");
	}

	for(int i = 0; i < msgSENSIBUS.dataToHostLength; i++){
		if(out != NULL){
			sprintf(appo, "0x%02X", msgSENSIBUS.dataToHost[i]);
			strcat(out, appo);
			if(i+1 < msgSENSIBUS.dataToHostLength){
				strcat(out, " ");
			}
		}
	}
	if(out != NULL){
		strcat(out,"]");
	}
}

