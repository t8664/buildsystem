#include "SPAutoRangeAlgorithm.h"
#include <math.h>

#include "config.h"

// valutare se passare SPMeasurementParameterEIS* paramEIS
void autoRange(int _vcc){  // SPMeasurementRunX* this_c, SPMeasurementParameterEIS* paramEIS,
	//createMap()
	float _Vmax[]={0.835f, 2.0f, 2.0f, 2.0f, 2.0f, 2.0f};

	AutoRangeConfig _autoRangeConfig[6]={
			{"1_50",      RSenseLabels[3], ingainLabelsRUN5[0], _Vmax[0], _Vmax[0]/10  },
			{"20_50",     RSenseLabels[3], ingainLabelsRUN5[2], _Vmax[1], _Vmax[1]/2   },
			{"40_50",     RSenseLabels[3], ingainLabelsRUN5[3], _Vmax[2], _Vmax[2]/10  },
			{"40_500",    RSenseLabels[2], ingainLabelsRUN5[3], _Vmax[3], _Vmax[3]/10  },
			{"40_5000",   RSenseLabels[1], ingainLabelsRUN5[3], _Vmax[4], _Vmax[4]/10  },
			{"40_50000",  RSenseLabels[0], ingainLabelsRUN5[3], _Vmax[5], 1.4e-45f     }
	};
//	AutoRangeConfig _autoRangeConfig[6]={
//			{"1_50",      RSenseLabels[3], RSenseValues[3], ingainLabelsRUN5[0], ingainValuesRUN5[0], _Vmax[0], _Vmax[0]/10  },  //ingainValuesRUN5 per qualche motivo viene letto
//			{"20_50",     RSenseLabels[3], RSenseValues[3], ingainLabelsRUN5[2], ingainValuesRUN5[2], _Vmax[1], _Vmax[1]/2   },  // al contrario
//			{"40_50",     RSenseLabels[3], RSenseValues[3], ingainLabelsRUN5[3], ingainValuesRUN5[3], _Vmax[2], _Vmax[2]/10  },
//			{"40_500",    RSenseLabels[2], RSenseValues[2], ingainLabelsRUN5[3], ingainValuesRUN5[3], _Vmax[3], _Vmax[3]/10  },
//			{"40_5000",   RSenseLabels[1], RSenseValues[1], ingainLabelsRUN5[3], ingainValuesRUN5[3], _Vmax[4], _Vmax[4]/10  },
//			{"40_50000",  RSenseLabels[0], RSenseValues[0], ingainLabelsRUN5[3], ingainValuesRUN5[3], _Vmax[5], 1.4e-45f     }
//	};
	//end createMap()

	int tempIndexOfMeasure = spParamEIS_MyNewBatch.measure; //spParamEIS_MyNewBatch spParamEIS
	//int indexOfMeasure = 7;
	// paramEIS.setMeasure(SPParamItemMeasures.measureLabels[indexOfMeasure]);
	 setMeasure(&spParamEIS_MyNewBatch, measureLabelsRUN5[7]); //spParamEIS_MyNewBatch spParamEIS

	 double VM;
	 SPMeasurementParameterADC paramADC;   // rivedere inizializzazione
	 int i = 0;
	 do{
//		 strcpy(spParamEIS.QIparam->rsense, _autoRangeConfig[i].RsenseValue); //devo passare il value
//		 strcpy(spParamEIS.QIparam->ingain, _autoRangeConfig[i].InGainValue); //devo passare il value
		 setRSense(spParamEIS_MyNewBatch.QIparam, _autoRangeConfig[i].RsenseLabel);//devo passare la label //spParamEIS_MyNewBatch spParamEIS
		 setInGain(spParamEIS_MyNewBatch.QIparam, _autoRangeConfig[i].InGainLabel);//devo passare la label //spParamEIS_MyNewBatch spParamEIS
/*
	  	 print_f("\nInGainvalue[%d]: %s", i, spParamEIS_MyNewBatch.QIparam->ingain); //spParamEIS_MyNewBatch spParamEIS
		 print_f("\nRsensevalue[%d]: %s", i, spParamEIS_MyNewBatch.QIparam->rsense); //spParamEIS_MyNewBatch spParamEIS
		 print_f("\nFrequency(autoRange)[%d]: %f", i, spParamEIS_MyNewBatch.QIparam->frequency); //spParamEIS_MyNewBatch spParamEIS
*/
		 SPMeasuramentOutput message;
		// setEIS(spMeasurementRUN5, paramEIS, &message);
		 spMeasurementRUNX[instanceID].setEIS(&spMeasurementRUNX[instanceID], &spParamEIS_MyNewBatch, &message); //spParamEIS_MyNewBatch spParamEIS
//		 setEIS_7(this_c, spParamEIS, &message);//paramEIS
		 //ADC parameters

		 //da rivedere, utilizzare  paramEIS->QIparam->spMeasurementParameterADC ?
//		 SPMeasurementParameterADC paramADC;   // rivedere inizializzazione
		 initSPParamItemRsense(&paramADC.spParamItemRSense); //valutare se toglierlo
		 initSPParamItemInGain(&paramADC.spParamItemInGain); //valutare se toglierlo
		 initSPParamItemMux(&paramADC.spParamItemMux); //valutare se toglierlo
		 paramADC.CommandX20 = true;
		 sp_double conversionRate = (sp_double)50;
//		 setConversionRate(&paramADC, conversionRate);
		 paramADC.conversionRate = conversionRate;

		 paramADC.spMeasurementParameterPort = spParamADC.spMeasurementParameterPort;
		 paramADC.spMeasurementParameterPort->spMeasurementParameter->spParamItemFIFO_DEEP.numericalValue = 1;
		 paramADC.spMeasurementParameterPort->spMeasurementParameter->spParamItemFIFO_READ.numericalValue = 1;
//		 setFIFO_DEEP(&paramADC,1);  //PROBABILMENTE � SEMPRE 1//  //8??
//		 setFIFO_READ(&paramADC,1);  //PROBABILMENTE � SEMPRE 1// il valore cambia a seconda di i
		 setInPortADC(&paramADC, INPORT_IA);
		 paramADC.spParamItemRSense.setRsense(&paramADC.spParamItemRSense, _autoRangeConfig[i].RsenseLabel); //devo passare la label
		 paramADC.spParamItemInGain.setInGain(&paramADC.spParamItemInGain, _autoRangeConfig[i].InGainLabel); //devo passare la label
//		 print_f("\nRsense[%d]: %s", i, paramADC.spParamItemRSense.item.label);
//		 print_f("\nInGain[%d]: %s", i, paramADC.spParamItemInGain.item.label);
		// setCommandX20(&paramADC, true);  // togliere?
		 spMeasurementRUNX[instanceID].setADC(&spMeasurementRUNX[instanceID], &paramADC, &message);
//		 setADC_7(this_c, &paramADC, &message);
		 sp_double _output[NUM_OF_CHIPS][EIS_NUM_OUTPUT];
		 spMeasurementRUNX[instanceID].getSingleEIS(&spMeasurementRUNX[instanceID], _output, &spParamEIS_MyNewBatch); //spParamEIS_MyNewBatch spParamEIS
//		 getSingleEIS(this_c, _output, spParamEIS);//paramEIS

/*		 //prova valori autorange
		  print_f("\n**********NEW-AUTORANGE[%d]**********\n", i);
			for(int i = 0; i < 1; i++){ //NUM_OF_CHIPS
				for (int j = 0; j < EIS_NUM_OUTPUT; j++){  //EIS_NUM_OUTPUT or SENSORS_TYPE_NUMBER
					print_f("outEIS[%d][%d]: ", i, j);
					print_f("%e\n",_output[i][j]);
				}
			}
			print_f("**********************************\n\n");
*/

		 _output[0][QUADRATURE] = (_output[0][QUADRATURE] * _vcc) / (pow(2, 15)* 1000); //32768->Math.pow(2,15)
         _output[0][IN_PHASE] = (_output[0][IN_PHASE] * _vcc) / (pow(2, 15) * 1000); //32768->Math.pow(2,15)
         VM = sqrt( pow(_output[0][QUADRATURE], 2) + pow(_output[0][IN_PHASE], 2));
//         print_f("VM: %f\n", VM);

         if (i == 0 && VM > _Vmax[i]){
             print_f("Overflow condition for auto ranging!\n");
        	 return;
         }
         if (VM >= _autoRangeConfig[i].Vmin){
            break;
          }

		i++;
	 }while( i < 6);

//	 if (i == 6){  //valutare se metterlo
//		 i--;
//	 }

	 if (VM >= _autoRangeConfig[i].Vmin){
		 spParamADC_MyNewBatch = paramADC;  // assegno a spParamADC (che verr� utilizzata nella successiva misura EIS) gli ultimi parametri di AR
// setto gli rsense e ingain per la GUI
		  r_sense = paramADC.spParamItemRSense.item.label.string;
		  in_gain = paramADC.spParamItemInGain.item.label.string;
		  ingain_rsense_index = i;
		  print_f("Autorange ok");  // non lo faccio con spParamEIS perch� uso sempre la struttura Globale

	 } else
		  print_f("Auto Range ko!!");

	  setMeasure(&spParamEIS_MyNewBatch, measureLabelsRUN5[tempIndexOfMeasure]); //spParamEIS_MyNewBatch spParamEIS    7


}
