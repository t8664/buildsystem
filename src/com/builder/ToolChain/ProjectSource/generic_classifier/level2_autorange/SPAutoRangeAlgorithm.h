
#ifndef LEVEL2_SPAUTORANGEALGORITHM_H
#define LEVEL2_SPAUTORANGEALGORITHM_H

#include "../API/level2/Parameters/SPMeasurementParameterEIS.h"
#include "../API/level2/SPMeasurementRun5.h" // togliere?
#include "../API/level2/SPMeasurementRun7.h" // togliere?
#include "../API/level2/SPMeasurementRunX.h"
#include "../API/level2/Items/SPParamItemRSense.h"
#include "../API/level2/Items/SPParamItemInGain.h"
#include "../API/level2/Items/SPParamItemMeasures.h"
#include "../API/level2/Items/SPParamItemPortADC.h"

#include "../variables.h"


typedef struct AutoRangeConfig{
	char key[10];
	char *RsenseLabel;
//	char *RsenseValue;
	char *InGainLabel;
//	char *InGainValue;
	float Vmax;
	float Vmin;
}AutoRangeConfig;

void autoRange(int _vcc); //SPMeasurementRunX* this_c, SPMeasurementParameterEIS* paramEIS,

#endif
