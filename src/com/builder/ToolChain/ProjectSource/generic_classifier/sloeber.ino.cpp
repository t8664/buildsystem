#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2020-11-21 16:20:59

#include "Arduino.h"
#include <WiFi.h>
#include <WiFiMulti.h>
extern WiFiMulti wifiMulti;
#include "DRIVER/ESP8266/StatusSENSIBUS.h"
#include "DRIVER/ESP8266/MsgSENSIBUS.h"
#include "DRIVER/ESP8266/CommSENSIBUS.h"
#include "DRIVER/ESP8266/CommUSB.h"
#include "IOTMAT/Menu.hpp"
#include "IOTMAT/IOTMatConfig.h"
#include <Preferences.h>
#include "SPIFFS.h"
#include "DRIVER/ESP8266/CommSENSIBUS.h"
#include "DRIVER/ESP8266/CommUSB.h"
#include "DRIVER/ESP8266/MsgSENSIBUS.h"
#include "DRIVER/ESP8266/StatusSENSIBUS.h"
#include "task.h"
extern "C" {
#include "EXT_OPERATION/set_SPMeasuramentParameterEIS.h"
}
extern "C" {
#include "EXT_OPERATION/fileHandlerMCU.h"
}
extern "C" {
#include "API/level1/decoder/SPDecoderSensibus.h"
}
extern "C" {
#include "API/level1/protocols/esp8266/SPProtocolESP8266_SENSIBUS.h"
}
extern "C" {
#include "DRIVER/ESP8266/SendData.h"
}
extern "C" {
#include "variables.h"
}

void setup() ;
void loop() ;
void sendData(uint8 header, uint8 command, uint8* addressByte, short dimAddress, 		uint8* data, short dimData, char* out);

#include "generic_classifier.ino"


#endif
