/*
 * task.cpp
 *
 *  Created on: 31 lug 2018
 *      Author: Luca
 */
#include "task.h"
#include "Arduino.h"

extern "C"{
#include "variables.h"
#include "IOTMAT/guiParameters.h"
#include "EXT_OPERATION/softResetMCU.h"
#include "API/level1/protocols/esp8266/SPProtocolESP8266_SENSIBUS.h"
#include "DRIVER/ESP8266/MsgSENSIBUS.h"
#include "EXT_OPERATION/set_Sensor.h"
#include "EXT_OPERATION/set_SPMeasuramentParameterEIS.h"
#include "EXT_OPERATION/set_SPMeasuramentParameterADC.h"
#include "level2_autorange/SPAutoRangeAlgorithm.h"
}

unsigned long long int MEASURE_TIME_STAMP = 0;

void set_speed(char* sensibus_speed){
	char write_speed_instr[30] = "WRITE SENSIBUS_SET+1 S ";
	char read_speed_instr[] = "READ SENSIBUS_SET M 2";

	strcat(write_speed_instr, sensibus_speed);

	sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, write_speed_instr, spMeasurement[instanceID].spProtocol->spCluster);
	sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, read_speed_instr, spMeasurement[instanceID].spProtocol->spCluster);
}

void init_chip(){
	char write_Ana_Config[] = "WRITE ANA_CONFIG M 0x0000";//ok
	char write_Ana_Config_1[] = "WRITE ANA_CONFIG+1 S 0x04";//ok
	char write_Ana_Config_S[] = "WRITE ANA_CONFIG S 0x1B";//ok
	char write_Cha_Select_S[] = "WRITE CHA_SELECT S 0XCC";//ok
	char write_Dss_Select_M[] = "WRITE DSS_SELECT M 0x0020";
	//char write_Dss_Select_M[] = "WRITE DSS_SELECT M 0X0020";//ok
	char write_Cha_Condit[] = "WRITE CHA_CONDIT M 0x031F";//ok
	char write_Cha_Filter[] = "WRITE CHA_FILTER M 0x9200";//ok
	char write_Ana_Config_1B[] = "WRITE ANA_CONFIG S 0x1B"; //ok
	char write_Cha_Divider[] = "WRITE CHA_DIVIDER M 0X8036";//ok
	char write_Cha_Select[] = "WRITE CHA_SELECT+1 S 0XC0";//ok
	char write_Cha_Config[] = "WRITE CHA_CONFIG S 0X21";//ok
	char write_Ana_Config_1S[] = "WRITE ANA_CONFIG+1 S 0x00";//ok
	char write_Ana_Config_S4[] = "WRITE ANA_CONFIG+1 S 0x04";//ok
	char write_Command[] = "WRITE COMMAND S 0x20";
	char read_Cha_FifoL[] = "READ CHA_FIFOL M 2";

	int output[NUM_OF_CHIPS][SETTINGS_ADC_NDATA_INT];


	//Serial.println("**********INIZIO INIT**********");
	sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, write_Ana_Config, &spCluster);
	sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, write_Ana_Config_1, &spCluster);
	sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, write_Ana_Config_S, &spCluster);
	sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, write_Cha_Select_S, &spCluster);
	sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, write_Dss_Select_M, &spCluster);
	sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, write_Cha_Condit, &spCluster);
	sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, write_Cha_Filter, &spCluster);
	sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, write_Ana_Config_1B, &spCluster);
	sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, write_Cha_Divider, &spCluster);
	sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, write_Cha_Select, &spCluster);
	sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, write_Cha_Config, &spCluster);
	sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, write_Ana_Config_1S, &spCluster);
	sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, write_Ana_Config_S4, &spCluster);

	spMeasurementRUNX[instanceID].getData(&spMeasurementRUNX[instanceID], output, spParamEIS.QIparam->spMeasurementParameterADC, true);
//	double k = 3 * 2800.0/32768.0;
//	double volt1 = (double) output[0][0] * k;
//	double volt2 = (double) output[1][0] * k;

	//Serial.printf("output[0][0]: %d\n", output[0][0]);
	//Serial.printf("output[1][0]: %d\n", output[1][0]);

	//	sendInstruction(spMeasurement.spProtocol, &appoProtocol, write_Command, &spCluster);
	//	delay(100);
	//	sendInstruction(spMeasurement.spProtocol, &appoProtocol, read_Cha_FifoL, &spCluster);

	//Serial.printf("Bandgap voltage1: %f \n", volt1);
	//Serial.printf("Bandgap voltage2: %f \n", volt2);

	//Serial.print("**********FINE INIT**********\n\n");

}

void run_misura(){
	Serial.println("****************INIZIO MISURA****************");
	spMeasurementRUNX[instanceID].setEIS(&spMeasurementRUNX[instanceID], &spParamEIS, NULL);
	spMeasurementRUNX[instanceID].setADC(&spMeasurementRUNX[instanceID], &spParamADC, NULL);

	//	sendInstruction(spMeasurement.spProtocol, &appoProtocol, write_command, &spCluster);
	//	sendInstruction(spMeasurement.spProtocol, &appoProtocol, read_command, &spCluster);
	//	delay(100);
	//	sendInstruction(spMeasurement.spProtocol, &appoProtocol, read_CHA_FIFOL, &spCluster);
	//
	//	sendInstruction(spMeasurement.spProtocol, &appoProtocol, write_command2, &spCluster);
	//	sendInstruction(spMeasurement.spProtocol, &appoProtocol, read_command, &spCluster);
	//	delay(100);
	//	sendInstruction(spMeasurement.spProtocol, &appoProtocol, read_CHA_FIFOL, &spCluster);

	//	delay(3000);
	spMeasurementRUNX[instanceID].getSingleEIS(&spMeasurementRUNX[instanceID], output1, &spParamEIS);

	Serial.printf("****************FINE MISURA****************\n\n");

	Serial.println("**********INIZIO STAMPA OUTEIS**********");
	for(int i = 0; i < NUM_OF_CHIPS; i++){
		for (int j = 0; j < EIS_NUM_OUTPUT; j++){
			Serial.printf("Out[%d][%d]: ", i, j);
			Serial.printf("%e\n",output1[i][j]);
		}
	}
	Serial.printf("**********FINE STAMPA OUTEIS**********\n\n");

}

void set_sensingElement(uint8 rSense){

}

void open_connection(){
//	Serial.printf("Open Loop 0\n");
		char instruction[15][30];
		char appo[30];
		int g1 = 0;
		MsgSENSIBUS msg;

		strcpy(instruction[0], "WRITE SENSIBUS_SET S 0xCC");
		strcpy(instruction[1], "WRITE COMMAND S 0x60");
		strcpy(instruction[2], "WRITE COMMAND S 0x40");
		strcpy(instruction[3], "WRITE SENSIBUS_SET S 0xDC"); //Addressing mode NoAddress
		strcpy(instruction[4], "WRITE DIG_CONFIG+1 S 0X0F");
		strcpy(instruction[5], "WRITE INT_CONFIG S 0x01");
		strcpy(instruction[6], "READ DEVICE_ID0 M 6");
  //loop di g1
		strcpy(instruction[7], "WRITE ANA_CONFIG M 0x0000");	// Set bandgap
		strcpy(instruction[8], "WRITE ANA_CONFIG+1 S 0x00");	//896	getSPBandGapFromVCC  --> getSPBandGapRATIOMETRIC
	// (dentro la funzione restartBandGap)
		strcpy(instruction[9], "WRITE ANA_CONFIG+1 S 0x00");		//getSPBandGapRATIOMETRIC
		strcpy(instruction[10], "WRITE ANA_CONFIG+1 S 0x00");		//getSPBandGapFromVCC-->getSPBandGapRATIOMETRIC
		strcpy(instruction[11], "WRITE COMMAND S 0x24");		//inizio getSENSOR
		strcpy(instruction[12], "READ CHA_FIFOL M 2");			//fine getSENSOR
	// (fine funzione restartBandGap)

  //fine loop

		strcpy(instruction[13], "WRITE INT_CONFIG+1 S 0x02");
		strcpy(instruction[14], "WRITE INT_CONFIG+1 S 0x00");

		softResetMCU();  //first reset [0x84] ?????
//		Serial.printf("Open Loop 1\n");
		//setAddressingType(appo, spMeasurement[instanceID].spProtocol->addressingMode, FULL_ADDRESS, spMeasurement[instanceID].spProtocol, &appoProtocol);
		sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, instruction[0], spMeasurement[instanceID].spProtocol->spCluster);
		sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, instruction[1], spMeasurement[instanceID].spProtocol->spCluster);
		sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, instruction[2], spMeasurement[instanceID].spProtocol->spCluster);
		//sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, instruction[3], spMeasurement[instanceID].spProtocol->spCluster);
		setAddressingType(appo, spMeasurement[instanceID].spProtocol->addressingMode, NO_ADDRESS, spMeasurement[instanceID].spProtocol, &appoProtocol);
		sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, instruction[4], spMeasurement[instanceID].spProtocol->spCluster);
		activeMulticast();
		sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, instruction[5], spMeasurement[instanceID].spProtocol->spCluster);
		//sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, instruction[6], spMeasurement[instanceID].spProtocol->spCluster);
//		Serial.printf("Open Loop 2\n");
  // LOOOP
	//	Serial.printf("Open Loop\n");

  while(g1 < 2){
		sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, instruction[7], spMeasurement[instanceID].spProtocol->spCluster);
		sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, instruction[8], spMeasurement[instanceID].spProtocol->spCluster);

		//inizio setSensor per restartBandgap 93020301

		msg.dataRcv[0] = 0x3;   //controllo byte significativi 03
		msg.dataRcv[1] = 0x1;	//01

			 set_Sensor(msg.dataRcv);
			 spMeasurementRUNX[instanceID].setSENSOR(&spMeasurementRUNX[instanceID], &spParamSENSOR, NULL);
				//msgSENSIBUS.instructionType = EXT_INSTR_SET_SENSOR;

		 //fine setSensor per restartBandgap 93020301

		  sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, instruction[9], spMeasurement[instanceID].spProtocol->spCluster);
		  sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, instruction[10], spMeasurement[instanceID].spProtocol->spCluster);

		  delay(10);//30

	// Get Sensor per restartBandgap (0x94)
			spMeasurementRUNX[instanceID].getSENSOR(&spMeasurementRUNX[instanceID], output1, &spParamSENSOR); // TO DO: testing
		//	sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, instruction[11], spMeasurement[instanceID].spProtocol->spCluster); //&spCluster
		//	delay(100);
		//	sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, instruction[12], spMeasurement[instanceID].spProtocol->spCluster); //&spCluster

/*
				spMeasurementRUNX[instanceID].getSENSOR(&spMeasurementRUNX[instanceID], output1, &spParamSENSOR);
					//msgSENSIBUS.instructionType = EXT_INSTR_GET_SENSOR;
					Serial.printf("PRE-TEST getVoltageBandgap\n");

					Serial.printf("getSensor1[0][0]: %f\n", output1[0][0]);
					Serial.printf("getSensor1[1][0]: %f\n", output1[1][0]);

					Serial.printf("TEST getVoltageBandgap\n");
*/

		delay(5);
		g1++;
  }
  //FINE LOOP

		sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, instruction[13], spMeasurement[instanceID].spProtocol->spCluster);
		sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, instruction[14], spMeasurement[instanceID].spProtocol->spCluster);

  // Fine Test stand-alone OPEN (MCU Owner) (OPEN):  giova (25/04/2020)

}

double temp_measure(){

// Test stand-alone temperature measure (TEMP_SET_SENSOR):  giova (21/04/2020)

	// Set Sensor
	// byte rcv[] = {0x02,0x01};
	MsgSENSIBUS msgSENSIBUS;

	  	  msgSENSIBUS.dataRcv[0] = 0x2;
	  	  msgSENSIBUS.dataRcv[1] = 0x1;

	  	  	set_Sensor(msgSENSIBUS.dataRcv);
	  		spMeasurementRUNX[instanceID].setSENSOR(&spMeasurementRUNX[instanceID], &spParamSENSOR, NULL);

	  		//Serial.print("Misura di temp effettuata!");
	  	    delay(1);

	 // Get Sensor (0x94)
	  	  	spMeasurementRUNX[instanceID].getSENSOR(&spMeasurementRUNX[instanceID], outValues.outTEMP, &spParamSENSOR);

/*
	  	  Serial.println("**********INIZIO STAMPA**********");
	  	  	for(int i = 0; i < 1; i++){ //NUM_OF_CHIPS
	  	  		for (int j = 0; j < 1; j++){  //EIS_NUM_OUTPUT or SENSORS_TYPE_NUMBER
	  	  			Serial.printf("outTEMP[%d][%d]: ", i, j);
	  	  			Serial.printf("%e\n",outTEMP[i][j]);
	  	  		}
	  	  	}
	  	  	Serial.printf("**********FINE STAMPA**********\n\n");

	  	    Serial.flush ();
*/
	  	    delay(1);

	  	    return outValues.outTEMP[0][0];
}


struct outValues batch_eis_measure(){

// Test stand-alone batch eis measure:  giova (09/05/2020) //90 0d 71 eb 0b 20 b8 00 08 00 47 98 96 80 00
// Test stand-alone batch EIS_SIMPLE_Semplified 			 90 0d c1 ec 0c 20 b8 00 08 00 47 98 96 80 00
	MsgSENSIBUS msgADC, msgSENSIBUS;

// setEIS

	if(frst == 0){
		byte payload = 0x0D;
		byte data[DATA_RCVD_MAXLEN] = {0xc1, 0xec, 0x0c, 0x20, 0xb8, 0x00, 0x08, 0x00, 0x47, 0x98, 0x96, 0x80, 0x00}; //spostarlo
		MsgSENSIBUS msgEIS = MsgSENSIBUS(payload, data);
		//	if(msgEIS.dataRcvLength >= 0x0D){
			set_SPMeasuramentParamenterEIS(msgEIS.dataRcv); // da fare solo la prima volta
		//	spParamEIS.QIparam->frequency = guiParam.fparLabelsValues[0];//setta default (10Hz) // RISOLVERE IL PROBLEMA DEL RECUPERO
			//}																					// CONFIGURAZIONE SALVATA DI FREQ
		frst++;
	}

//	msgEIS.dataRcvLength = 0x0D; //payload
//
//	msgEIS.dataRcv[0] = 0x71;
//	msgEIS.dataRcv[1] = 0xeb;
//	msgEIS.dataRcv[2] = 0x0b;
//	msgEIS.dataRcv[3] = 0x20;
//	msgEIS.dataRcv[4] = 0xb8;
//	msgEIS.dataRcv[5] = 0x00;
//	msgEIS.dataRcv[6] = 0x08;
//	msgEIS.dataRcv[7] = 0x00;
//	msgEIS.dataRcv[8] = 0x47;
//	msgEIS.dataRcv[9] = 0x98;
//	msgEIS.dataRcv[10] = 0x96;
//	msgEIS.dataRcv[11] = 0x80;
//	msgEIS.dataRcv[12] = 0x00;

//	Serial.printf("\nfrequency(batch_eis_measure): %f\n", spParamEIS.QIparam->frequency);
//	spParamEIS.QIparam->frequency = guiParam.fparLabelsValues[currentConf[2]]; // cambio campo per campo in base ai valori cambiati in setParameter
//	spParamEIS.dcBiasN
//	spParamEIS.voltaggio

	spMeasurementRUNX[instanceID].setEIS(&spMeasurementRUNX[instanceID], &spParamEIS, NULL);

	delay(1);

//setADC - 91020131
	// Test stand-alone batch EIS_SIMPLE_Semplified 91020101

//	msgADC.dataRcv[2] = {0x01, 0x31};
	msgADC.dataRcv[0] = 0x01;
	msgADC.dataRcv[1] = 0x01;

	set_SPMeasuramentParameterADC(msgADC.dataRcv);
	spMeasurementRUNX[instanceID].setADC(&spMeasurementRUNX[instanceID], &spParamADC, NULL);

	delay(10); //50


	//spParamEIS.QIparam
	//for(int i = 0; i < 10; i++){    // for more eis measures
		spMeasurementRUNX[instanceID].getSingleEIS(&spMeasurementRUNX[instanceID], outValues.outEIS, &spParamEIS);
		//delay(1); //10
	//}
/*
	  Serial.println("**********INIZIO STAMPA**********");
	  	for(int i = 0; i < 1; i++){ //NUM_OF_CHIPS
	  		for (int j = 0; j < EIS_NUM_OUTPUT; j++){  //EIS_NUM_OUTPUT or SENSORS_TYPE_NUMBER
	  			Serial.printf("outEIS[%d][%d]: ", i, j);
	  			Serial.printf("%e\n",outValues.outEIS[i][j]);
	  		}
	  	}
	  	Serial.printf("**********FINE STAMPA**********\n\n");

	    Serial.flush ();
*/
	    delay(1);
/*

	    // TEMP MEASURE
	    	  	  msgSENSIBUS.dataRcv[0] = 0x02;

	  //  	  	  if(cnt == 0){
	    	  		  msgSENSIBUS.dataRcv[1] = 0x01;
	    //	  		  cnt++;
	    //	  	  }else
	   // 	  		msgSENSIBUS.dataRcv[1] = 0x0;

	    	set_Sensor(msgSENSIBUS.dataRcv);
	    	spMeasurementRUNX[instanceID].setSENSOR(&spMeasurementRUNX[instanceID], &spParamSENSOR, NULL);

	    	  		//Serial.print("Misura di temp effettuata!");
	    	delay(1);

	   // Get Sensor (0x94)
	    	spMeasurementRUNX[instanceID].getSENSOR(&spMeasurementRUNX[instanceID], outValues.outTEMP, &spParamSENSOR);


	    		  	  Serial.println("**********INIZIO STAMPA**********");
	    		  	  	for(int i = 0; i < 1; i++){ //NUM_OF_CHIPS
	    		  	  		for (int j = 0; j < 1; j++){  //EIS_NUM_OUTPUT or SENSORS_TYPE_NUMBER
	    		  	  			Serial.printf("outTEMP[%d][%d]: ", i, j);
	    		  	  			Serial.printf("%e\n",outValues.outTEMP[i][j]);
	    		  	  		}
	    		  	  	}
	    		  	  	Serial.printf("**********FINE STAMPA**********\n\n");

	    		  	    Serial.flush ();
*/
	    return outValues; //outValues.outEIS[0][0]
}

struct outValues new_batch_eis_measure(bool check_autorange){

//	MEASURE_TIME_STAMP = millis();
//	Serial.printf("\nINIZIO_MEASURE_TIME_STAMP: %llu\n", MEASURE_TIME_STAMP);

	//open_connection();

//set EIS
	if(frst == 0){

		//Inizializzazione spParamADC_MyNewBatch
				 initSPParamItemRsense(&spParamADC_MyNewBatch.spParamItemRSense); //valutare se toglierlo
				 initSPParamItemInGain(&spParamADC_MyNewBatch.spParamItemInGain); //valutare se toglierlo
				 initSPParamItemMux(&spParamADC_MyNewBatch.spParamItemMux); //valutare se toglierlo
				 spParamADC_MyNewBatch.CommandX20 = true;
				 sp_double conversionRate = (sp_double)50;
				 spParamADC_MyNewBatch.conversionRate = conversionRate;
				 spParamADC_MyNewBatch.spMeasurementParameterPort = spParamADC.spMeasurementParameterPort;
				 spParamADC_MyNewBatch.spMeasurementParameterPort->spMeasurementParameter->spParamItemFIFO_DEEP.numericalValue = 1;
				 spParamADC_MyNewBatch.spMeasurementParameterPort->spMeasurementParameter->spParamItemFIFO_READ.numericalValue = 1;
				 setInPortADC(&spParamADC_MyNewBatch, INPORT_IA);
				 spParamADC_MyNewBatch.spParamItemRSense.setRsense(&spParamADC_MyNewBatch.spParamItemRSense, r_sense);
				 spParamADC_MyNewBatch.spParamItemInGain.setInGain(&spParamADC_MyNewBatch.spParamItemInGain, in_gain);

	/*
	byte payload = 0x0D;
	byte data[6][DATA_RCVD_MAXLEN] = {  {0xc1, 0xec, 0x0c, 0x20, 0xb8, 0x00, 0x08, 0x00, 0x47, 0x98, 0x96, 0x80, 0x00},
										{0xe1, 0xec, 0x0c, 0x20, 0xb8, 0x00, 0x08, 0x00, 0x47, 0x98, 0x96, 0x80, 0x00},
										{0xf1, 0xec, 0x0c, 0x20, 0xb8, 0x00, 0x08, 0x00, 0x47, 0x98, 0x96, 0x80, 0x00},
										{0xb1, 0xec, 0x0c, 0x20, 0xb8, 0x00, 0x08, 0x00, 0x47, 0x98, 0x96, 0x80, 0x00},
										{0x71, 0xec, 0x0c, 0x20, 0xb8, 0x00, 0x08, 0x00, 0x47, 0x98, 0x96, 0x80, 0x00},
										{0x31, 0xec, 0x0c, 0x20, 0xb8, 0x00, 0x08, 0x00, 0x47, 0x98, 0x96, 0x80, 0x00}  };

*/

//	MsgSENSIBUS msgEIS_ar = MsgSENSIBUS(payload, data[0]);
//	set_SPMeasuramentParamenterEIS(msgEIS_ar.dataRcv); //messo per inizializzare spParamEIS (altrimenti crasha)
//	spParamEIS_MyNewBatch = spParamEIS; // messo per inizializzare la prima volta spParamEIS_MyNewBatch

	frst++;
}

// AUTO RANGE
//	Serial.println("\n**********NEW AUTO RANGE RUNNING**********");

//	MsgSENSIBUS msgEIS_ar = MsgSENSIBUS(payload, data[0]);
//	set_SPMeasuramentParamenterEIS(msgEIS_ar.dataRcv); //messo per inizializzare spParamEIS (altrimenti crasha)
//	spParamEIS_MyNewBatch = spParamEIS; // messo per inizializzare la prima volta spParamEIS_MyNewBatch

/*
	// test risettaggio solo delle porte e della frequenza (a causa della set_Sensor chiamata da HUMIDITY)
	setSPMeasurementParameterEIS(&spParamEIS_MyNewBatch, "TWO", "1", "FIRST_HARMONIC", "VOUT_IIN", frq, 0, 0, "7",  //78125.00f  frq
			"PORT_HP", "PORT_HP", "CAPACITANCE", "50", "1", "Quadrants", 0, "IN_PHASE");
*/

//	setInPort(spParamEIS_MyNewBatch.QIparam,"PORT_HP");
//	setOutPort(spParamEIS_MyNewBatch.QIparam,"PORT_HP");
//	setFrequency(spParamEIS_MyNewBatch.QIparam, frq);

//	Serial.printf("\n****spParamEIS_MyNewBatch PRE SET EIS****\n");
//	Serial.printf("\nContacts: %s\n", spParamEIS_MyNewBatch.Contacts.item.label.string);
//	Serial.printf("\ndcBiasN: %d\n", spParamEIS_MyNewBatch.dcBiasN);
//	Serial.printf("\ndcBiasP: %d\n", spParamEIS_MyNewBatch.dcBiasP);
//	Serial.printf("\nmeasure: %d\n", spParamEIS_MyNewBatch.measure);
//	Serial.printf("\nfilter: %s\n", spParamEIS_MyNewBatch.QIparam->filter);
//	Serial.printf("\nfrequency (TASK.CPP): %f\n", spParamEIS_MyNewBatch.QIparam->frequency);
//	Serial.printf("\nharmonic: %s\n", spParamEIS_MyNewBatch.QIparam->harmonic);
//	Serial.printf("\ningain: %s\n", spParamEIS_MyNewBatch.QIparam->ingain);
//	Serial.printf("\ninport: %s\n", spParamEIS_MyNewBatch.QIparam->inport->port->portValue);
//	Serial.printf("\noutport: %s\n", spParamEIS_MyNewBatch.QIparam->inport->port->portValue);
//	Serial.printf("\nmodeVI: %s\n", spParamEIS_MyNewBatch.QIparam->modeVI);
//	Serial.printf("\noutgain: %s\n", spParamEIS_MyNewBatch.QIparam->outgain);
//	Serial.printf("\nphaseShift: %d\n", spParamEIS_MyNewBatch.QIparam->phaseShift);
//	Serial.printf("\nphaseShiftMode: %d\n", spParamEIS_MyNewBatch.QIparam->phaseShiftMode);
//	Serial.printf("\nq_i: %s\n", spParamEIS_MyNewBatch.QIparam->q_i);
//	Serial.printf("\nrsense: %s\n", spParamEIS_MyNewBatch.QIparam->rsense);

	//Serial.printf("\nContacts: %s\n", contacts);
	//Serial.printf("\ninGain: %s\n", in_gain);
	//check_autorange
	if(check_autorange == true){
		// test risettaggio solo delle porte e della frequenza (a causa della set_Sensor chiamata da HUMIDITY)
		setSPMeasurementParameterEIS(&spParamEIS_MyNewBatch, contacts, in_gain, "FIRST_HARMONIC", "VOUT_IIN", frq, 0, 0, "7",  //"TWO"
				"PORT_HP", "PORT_HP", "CAPACITANCE", "50", "1", "Quadrants", 0, "IN_PHASE"); //out_gain al posto di "7", BiasP ->0
		int _vcc = spConfiguration.VREF;
		autoRange(_vcc);
		Serial.print("\nAUTORANGE DONE!\n");

		spMeasurementRUNX[instanceID].setEIS(&spMeasurementRUNX[instanceID], &spParamEIS_MyNewBatch, NULL); //spParamEIS_MyNewBatch  spParamEIS
		delay(1);

	}else{

//		Serial.printf("\nfrq(TASK.CPP): %f\n", frq);
		// ho modificato setRSense, setInGain; ho aggiunto setQI (tutto rispetto alla set_SPMeasurementParameterEIS
		setSPMeasurementParameterEIS(&spParamEIS_MyNewBatch, contacts, in_gain, "FIRST_HARMONIC", "VOUT_IIN", frq, BiasP, 0, out_gain,
				"PORT_HP", "PORT_HP", "CAPACITANCE", r_sense, "1", "Quadrants", 0, "IN_PHASE"); //"50000"

//		Serial.printf("\nMEASURE_TIME_STAMP(dopo la set_EIS): %llu\n", (millis() - MEASURE_TIME_STAMP));

//		Serial.printf("\n****spParamEIS_MyNewBatch POST SET EIS****\n");
//		Serial.printf("\nContacts: %s\n", spParamEIS_MyNewBatch.Contacts.item.label.string);
//		Serial.printf("\ndcBiasN: %d\n", spParamEIS_MyNewBatch.dcBiasN);
//		Serial.printf("\ndcBiasP: %d\n", spParamEIS_MyNewBatch.dcBiasP);
//		Serial.printf("\nmeasure: %d\n", spParamEIS_MyNewBatch.measure);
//		Serial.printf("\nfilter: %s\n", spParamEIS_MyNewBatch.QIparam->filter);
//		Serial.printf("\nfrequency (TASK.CPP): %f\n", spParamEIS_MyNewBatch.QIparam->frequency);
//		Serial.printf("\nharmonic: %s\n", spParamEIS_MyNewBatch.QIparam->harmonic);
//		Serial.printf("\ningain: %s\n", spParamEIS_MyNewBatch.QIparam->ingain);
//		Serial.printf("\ninport: %s\n", spParamEIS_MyNewBatch.QIparam->inport->port->portValue);
//		Serial.printf("\noutport: %s\n", spParamEIS_MyNewBatch.QIparam->inport->port->portValue);
//		Serial.printf("\nmodeVI: %s\n", spParamEIS_MyNewBatch.QIparam->modeVI);
//		Serial.printf("\noutgain: %s\n", spParamEIS_MyNewBatch.QIparam->outgain);
//		Serial.printf("\nphaseShift: %d\n", spParamEIS_MyNewBatch.QIparam->phaseShift);
//		Serial.printf("\nphaseShiftMode: %d\n", spParamEIS_MyNewBatch.QIparam->phaseShiftMode);
//		Serial.printf("\nq_i: %s\n", spParamEIS_MyNewBatch.QIparam->q_i);
//		Serial.printf("\nrsense: %s\n", spParamEIS_MyNewBatch.QIparam->rsense);

//		Serial.printf("\nspParamEIS_MyNewBatch.QIparam->frequency(TASK.CPP): %f\n", spParamEIS_MyNewBatch.QIparam->frequency);
	//	spParamADC.spParamItemRSense.setRsense(&spParamADC.spParamItemRSense, r_sense);
		 spParamADC_MyNewBatch.spParamItemRSense.setRsense(&spParamADC_MyNewBatch.spParamItemRSense, r_sense);

		spMeasurementRUNX[instanceID].setEIS(&spMeasurementRUNX[instanceID], &spParamEIS_MyNewBatch, NULL); //spParamEIS_MyNewBatch  spParamEIS
		delay(5);

/*
		//set ADC
		MsgSENSIBUS msgADC;

			msgADC.dataRcv[0] = 0x01;
			msgADC.dataRcv[1] = 0x01; //0x31 -> Capacitance, 0x01 -> PER EIS_SIMPLE_Semplified.xml -> Inductance (FIFO_READ)
			set_SPMeasuramentParameterADC(msgADC.dataRcv); // testare attivarla sopo la setEIS

			spParamADC.spParamItemInGain.setInGain(&spParamADC.spParamItemInGain, in_gain);
*/

//Inizializzazione spParamADC_MyNewBatch
//		 initSPParamItemRsense(&spParamADC_MyNewBatch.spParamItemRSense); //valutare se toglierlo
//		 initSPParamItemInGain(&spParamADC_MyNewBatch.spParamItemInGain); //valutare se toglierlo
//		 initSPParamItemMux(&spParamADC_MyNewBatch.spParamItemMux); //valutare se toglierlo
//		 spParamADC_MyNewBatch.CommandX20 = true;
//		 sp_double conversionRate = (sp_double)50;
//		 spParamADC_MyNewBatch.conversionRate = conversionRate;
//		 spParamADC_MyNewBatch.spMeasurementParameterPort = spParamADC.spMeasurementParameterPort;
//		 spParamADC_MyNewBatch.spMeasurementParameterPort->spMeasurementParameter->spParamItemFIFO_DEEP.numericalValue = 1;
//		 spParamADC_MyNewBatch.spMeasurementParameterPort->spMeasurementParameter->spParamItemFIFO_READ.numericalValue = 1;
//		 setInPortADC(&spParamADC_MyNewBatch, INPORT_IA);
		// spParamADC_MyNewBatch.spParamItemRSense.setRsense(&spParamADC_MyNewBatch.spParamItemRSense, r_sense);
		 spParamADC_MyNewBatch.spParamItemInGain.setInGain(&spParamADC_MyNewBatch.spParamItemInGain, in_gain);
	//	 delay(5);

//		 Serial.printf("\ninstanceID (pre setEIS): %d\n", instanceID);
//		 spMeasurementRUNX[instanceID].setEIS(&spMeasurementRUNX[instanceID], &spParamEIS_MyNewBatch, NULL); //spParamEIS_MyNewBatch  spParamEIS

//		 Serial.printf("\nMEASURE_TIME_STAMP(dopo la setEIS): %llu\n", (millis() - MEASURE_TIME_STAMP));
	}

//	Serial.println("\n**********NEW AUTORANGE COMPLETED**********\n");

/*
//set ADC
	MsgSENSIBUS msgADC[3];

	msgADC[0].dataRcv[0] = 0x01;
	msgADC[0].dataRcv[1] = 0x01;
	msgADC[1].dataRcv[0] = 0x01;
	msgADC[1].dataRcv[1] = 0x21; // Cambiano nella getPacketforSetADC (modalit� MCU) quindi dove nella modalit� PC? setADC?
	//viene fatto il bitwise or tra fifo read: 1 e ingain
	msgADC[2].dataRcv[0] = 0x01;
	msgADC[2].dataRcv[1] = 0x31;
*/

/*
/// AUTO RANGE
	Serial.println("**********AUTO RANGE RUNNING**********");

	for(int i = 0; i < 6; i++){

		MsgSENSIBUS msgEIS_ar = MsgSENSIBUS(payload, data[i]);

		set_SPMeasuramentParamenterEIS(msgEIS_ar.dataRcv);  // IN FUTURO FARE DIRETTAMENTE I Set PARAMETER???
		spMeasurementRUNX[instanceID].setEIS(&spMeasurementRUNX[instanceID], &spParamEIS, NULL);
		delay(1);

		if(i == 0){
			set_SPMeasuramentParameterADC(msgADC[0].dataRcv);
		}else if(i == 1){
			set_SPMeasuramentParameterADC(msgADC[1].dataRcv);
		}else
			set_SPMeasuramentParameterADC(msgADC[2].dataRcv);

		spMeasurementRUNX[instanceID].setADC(&spMeasurementRUNX[instanceID], &spParamADC, NULL);

		delay(10); //50

		//for(int i = 0; i < 10; i++){    // for more eis measures
			spMeasurementRUNX[instanceID].getSingleEIS(&spMeasurementRUNX[instanceID], outValues.outEIS, &spParamEIS);
			//delay(1); //10
		//}

		  Serial.printf("**********AUTORANGE[%d]**********", i);
			for(int i = 0; i < 1; i++){ //NUM_OF_CHIPS
				for (int j = 0; j < EIS_NUM_OUTPUT; j++){  //EIS_NUM_OUTPUT or SENSORS_TYPE_NUMBER
					Serial.printf("outEIS[%d][%d]: ", i, j);
					Serial.printf("%e\n",outValues.outEIS[i][j]);
				}
			}
			Serial.printf("**********************************\n\n");

			Serial.flush ();

			delay(1);

	}

	Serial.println("**********AUTORANGE OK**********");
*/

//	MsgSENSIBUS msgEIS = MsgSENSIBUS(payload, data[5]);
	//set_SPMeasuramentParamenterEIS(msgEIS.dataRcv);

	//spMeasurementRUNX[instanceID].setEIS(&spMeasurementRUNX[instanceID], &spParamEIS_MyNewBatch, NULL); //spParamEIS_MyNewBatch  spParamEIS
//	delay(5);

	//Serial.println("\nPre setADC*\n");
	//set_SPMeasuramentParameterADC(msgADC[2].dataRcv);  // l'ho commentato perch� in autoRange faccio (spParamADC = paramADC) nell'ultima iterazione di autorange
//	Serial.printf("\ninstanceID (pre setADC): %d\n", instanceID);
	spMeasurementRUNX[instanceID].setADC(&spMeasurementRUNX[instanceID], &spParamADC_MyNewBatch, NULL);
	delay(5);//10
//	Serial.printf("\nMEASURE_TIME_STAMP(dopo la setADC): %llu\n", (millis() - MEASURE_TIME_STAMP));
//	Serial.printf("\ninstanceID (pre getSingleEIS): %d\n", instanceID);
	spMeasurementRUNX[instanceID].getSingleEIS(&spMeasurementRUNX[instanceID], outValues.outEIS, &spParamEIS_MyNewBatch); // spParamEIS_MyNewBatch  spParamEIS


		Serial.println("\n**********INIZIO STAMPA**********");
		  	for(int i = 0; i < 1; i++){ //NUM_OF_CHIPS
		  		for (int j = 0; j < EIS_NUM_OUTPUT; j++){  //EIS_NUM_OUTPUT or SENSORS_TYPE_NUMBER
		  			Serial.printf("outEIS[%d][%d]: ", i, j);
		  			Serial.printf("%e\n",outValues.outEIS[i][j]);
		  		}
		  	}
		 Serial.printf("**********FINE STAMPA**********\n\n");

		 Serial.flush ();

		 delay(1);

//	Serial.printf("\nMEASURE_TIME_STAMP(misuraEIS): %llu\n", (millis() - MEASURE_TIME_STAMP));



// TEMP MEASURE

	MsgSENSIBUS	msgTEMP;
	msgTEMP.dataRcv[0] = 0x02;
//  if(cnt == 0){
	msgTEMP.dataRcv[1] = 0x01;
//	  	cnt++;
//	}else
// 	  	msgSENSIBUS.dataRcv[1] = 0x0;

	set_Sensor(msgTEMP.dataRcv);
//	Serial.printf("\ninstanceID (pre setSENSOR temp): %d\n", instanceID);
	spMeasurementRUNX[instanceID].setSENSOR(&spMeasurementRUNX[instanceID], &spParamSENSOR, NULL);
	delay(5);

// Get Sensor (0x94)
//	Serial.printf("\ninstanceID (pre getSENSOR temp): %d\n", instanceID);
	spMeasurementRUNX[instanceID].getSENSOR(&spMeasurementRUNX[instanceID], outValues.outTEMP, &spParamSENSOR);


			  Serial.println("\n**********TEMPERATURA**********");
				for(int i = 0; i < 1; i++){ //NUM_OF_CHIPS
					for (int j = 0; j < 1; j++){  //EIS_NUM_OUTPUT or SENSORS_TYPE_NUMBER
						Serial.printf("outTEMP[%d][%d]: ", i, j);
						Serial.printf("%e\n",outValues.outTEMP[i][j]);
					}
				}
				Serial.printf("*******************************\n\n");

	Serial.flush ();


// Istruzione mandata lato winux dopo una misura di Temperatura!
	char instruction[1][30];
	strcpy(instruction[0], "WRITE ANA_CONFIG+1 S 0x00"); //getSPBandGapFromVCC  --> getSPBandGapRATIOMETRIC
	sendInstruction(spMeasurement[instanceID].spProtocol, &appoProtocol, instruction[0], spMeasurement[instanceID].spProtocol->spCluster);

//	Serial.printf("\nMEASURE_TIME_STAMP(misuraTEMP): %llu\n", (millis() - MEASURE_TIME_STAMP));

	// HUMIDITY MEASURE 9302d301
	//il problema � in humidity! CAMBIA LA PORTA DA PORT_HP A PORT_EXT3 E CAMBIA LA FREQ, e poi non si resetta al giro successivo

		MsgSENSIBUS	msgHUMIDITY;
		msgHUMIDITY.dataRcv[0] = 0xd3;
	//  if(cnt == 0){
		msgHUMIDITY.dataRcv[1] = 0x01;
	//	  	cnt++;
	//	}else
	// 	  	msgSENSIBUS.dataRcv[1] = 0x0;

		set_Sensor(msgHUMIDITY.dataRcv);  // questo provoca il reset di spParamEIS.QIparam->frequency e delle porte in/out
		//ora spParamSENSOR.paramInternalEIS punta a &spParamEIS???

		//provare una set_Sensor manuale?

//		Serial.printf("\ninstanceID (pre setSENSOR Hum): %d\n", instanceID);
		spMeasurementRUNX[instanceID].setSENSOR(&spMeasurementRUNX[instanceID], &spParamSENSOR, NULL);
		delay(5);
	// Get Sensor (0x94)
//		Serial.printf("\ninstanceID (pre getSENSOR Hum): %d\n", instanceID);
		spMeasurementRUNX[instanceID].getSENSOR(&spMeasurementRUNX[instanceID], outValues.outHUMIDITY, &spParamSENSOR);


				  Serial.println("\n**********UMIDITA'**********");
					for(int i = 0; i < 1; i++){ //NUM_OF_CHIPS
						//for (int j = 0; j < EIS_NUM_OUTPUT; j++){  //EIS_NUM_OUTPUT or SENSORS_TYPE_NUMBER
							Serial.printf("outHUMIDITY[%d][%d]: ", i, 7);
							Serial.printf("%e\n",outValues.outHUMIDITY[i][7]);
					//	}
					}
					Serial.printf("*******************************\n\n");

		Serial.flush ();
		delay(1);


//		Serial.printf("\nMEASURE_TIME_STAMP(TOTALE): %llu\n", (millis() - MEASURE_TIME_STAMP));

	//	Serial.printf("\nspParamEIS FREQUENCY AFTER HUMIDITY: %f\n", spParamEIS.QIparam->frequency);
	//	Serial.printf("\nspParamEIS_MyNewBatch FREQUENCY AFTER HUMIDITY: %f\n", spParamEIS_MyNewBatch.QIparam->frequency);


	return outValues;
}


