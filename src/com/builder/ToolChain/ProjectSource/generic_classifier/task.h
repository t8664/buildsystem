/*
 * task.h
 *
 *  Created on: 30 lug 2018
 *      Author: Luca
 */

#ifndef TASK_H_
#define TASK_H_

#include "IOTMAT/Menu.hpp"
#include "IOTMAT/IOTMatConfig.h"

extern void set_speed(char* sensibus_speed);
extern void init_chip();
extern void run_misura();
extern void open_connection();
extern double temp_measure();
extern struct outValues batch_eis_measure();
extern struct outValues new_batch_eis_measure(bool check_autorange);


/*
struct defaultSetEIS{
	byte payload = 0x0D;
	byte data[32] = {0x71, 0xeb, 0x0b, 0x20, 0xb8, 0x00, 0x08, 0x00, 0x47, 0x98, 0x96, 0x80, 0x00};      //DATA_RCVD_MAXLEN
}defaultSetEIS;
*/

#endif /* TASK_H_ */
