/*
 * variables.cpp
 *
 *  Created on: 25 nov 2018
 *      Author: Luca
 */

#include "pgmspace.h"
extern "C"{
#include "variables.h"
}

//char port_Label[PORT_NUMBER][PORTLABEL_MAXLEN] = {"PORT0", "PORT1", "PORT2", "PORT3", "PORT4", "PORT5", "PORT6", "PORT7", "PORT8", "PORT9", "PORT10", "PORT11", "PORT_HP", "PORT_EXT1", "PORT_EXT2", "PORT_EXT3",  "PORT_TEMPERATURE", "PORT_VOLTAGE", "PORT_LIGHT", "PORT_DARK", "PORT_NA"};
//char port_Value[PORT_NUMBER][PORTVALUE_MAXLEN] = {"00000", "00001", "00010", "00011", "00100", "00101", "00110", "00111", "01000", "01001", "01010", "01011", "11100", "11101", "11110", "11111", "01101", "01100", "01110", "01111", "11010"};
//char noPort[] = "NOPORT";

SPMeasurementParameterSENSOR spParamSENSOR;

SPFamily spFamilyMulticast;

SPCluster spCluster;
SPConfiguration spConfiguration;
SPProtocol spProtocol;
SPMeasurementRunX spMeasurementRUNX[NUM_OF_MEASUREMENT];
SPMeasurement spMeasurement[NUM_OF_MEASUREMENT];
SPDecoder spDecoder;
SPDecoderGenericInstruction spInstruction;
SPChip spChip;
SPPort in, out, portADC;
SPMeasurementStatusQI qistatus[NUM_OF_MEASUREMENT];
SPMeasurementStatusPOT potstatus[NUM_OF_MEASUREMENT];

SPMeasurementParameterEIS spParamEIS;
SPMeasurementParameterEIS spParamEIS_MyNewBatch;
SPMeasurementParameterPOT spParamPOT;
SPMeasurementParameterQI  spParamQI;
SPMeasurementParameterADC spParamADC;
SPMeasurementParameterADC spParamADC_MyNewBatch;
SPMeasurementParameterPort spParamPort;
SPMeasurementParameter spParam;

SPMeasurementParameterPort in_Port;
SPMeasurementParameterPort out_Port;

SPDriverESP8266 testDriver;
SPDriver spDriver;

SPSensingElement spSensingElement[NUM_OF_SE];
SPSensingElementOnChip spSeOnChip[NUM_OF_CHIPS][NUM_SE_ON_CHIP];
SPSensingElementOnFamily spSeOnFamily[NUM_SE_ON_FAMILY];
SPPort spPortList[PORT_NUMBER];
SPFamily spFamily;
sp_double output1[NUM_OF_CHIPS][EIS_NUM_OUTPUT];
//sp_double outTEMP[NUM_OF_CHIPS][EIS_NUM_OUTPUT];
//sp_double outEIS[NUM_OF_CHIPS][EIS_NUM_OUTPUT];

struct outValues outValues;
long avail_frq; // mi serve per calcolare D e Q nei parametri da mostrare a display
int cnt, frst;
sp_double frq; // , last_frq; // per l'update della freq da gui
char *contacts;
char *in_gain;
char *out_gain;
char *r_sense;
int BiasP;
int ingain_rsense_index;

//SPDataRepresentation spAdaptMeasure; //per adattare le misure

SPChip spChiplist[NUM_OF_CHIPS];
SPprotocolOut appoProtocol;

//SPChip spChipList[NUM_OF_CHIPS];
SPChip multicastChip, broadcastSlowChip, broadcastChip;

int ADDRESSING_MODE = SHORT_ADDRESS;

static void initConfiguration(SPConfiguration* configuration, SPCluster* cluster, SPProtocol* protocol, char* description);

static void initChip(SPChip* c, SPFamily* f, char* i2cAddress, char* serialNumber, char* oscTrimOverride,
		SPSensingElementOnChip* seOnChipList, int chipType);

static void initPort(SPPort* port);
static void initStatusQI();
static void initMeasurement();
static void initStatusPOT();
static void initRUN5();
static void initRUN7();
static void initParameter();
static void initParameterPort();
static void initParameterADC();
static void initParameterQI();
static void initParameterEIS();
static void initParameterPOT();
static void initParameterSENSOR();
static void initMyCounters();


void initSPVariables(){
	initPort(spPortList);
	initConfiguration(&spConfiguration, &spCluster, &spProtocol, "");
	spFamily.spSensingElementOnFamilyList = spSeOnFamily;
	spCluster.familyOfChips = &spFamily;
	spCluster.chipList = spChiplist;
	spCluster.dimChipList = 0;
	for(int i = 0; i < NUM_OF_CHIPS; i++){
		spCluster.chipList[i].family = &spFamily;
		spCluster.chipList[i].spSensingElementOnChipList = spSeOnChip[i];
	}
	spDecoder.instuction2String = &instruction2String;
	initMyCounters();
}

void initSPProtocolESP8266(){
	spProtocol.spDriver = &spDriver;
	spDriver.spDriverESP8266 = &testDriver;
	spDriver.spDriverESP8266->mcuDriver = &sendData;
	spProtocol.spDecoder = &spDecoder;
	spProtocol.spCluster = &spCluster;
	spProtocol.sp_send = &sp_send;
	spProtocol.getADCDelay = &getADCDelayESP8266;

//	if(strcmp(spConfiguration.addressingType, "fullAddress") == 0){
//		spProtocol.addressingMode = FULL_ADDRESS;
//	} else if(strcmp(spConfiguration.addressingType, "shortAddress") == 0){
//		spProtocol.addressingMode = SHORT_ADDRESS;
//	} else{
//		spProtocol.addressingMode = NO_ADDRESS;
//	}
	spProtocol.addressingMode = FULL_ADDRESS;
}

void fillChip(){
	char BROADCAST_I2C_ADDRESS[] = "0x00";

	strcpy(spFamilyMulticast.hwVersion, spFamily.hwVersion);
	strcpy(spFamilyMulticast.id, "0X00");
	strcpy(spFamilyMulticast.name, spFamily.name);
	strcat(spFamilyMulticast.name, "_MULTICAST");
	strcpy(spFamilyMulticast.oscTrim, spFamily.oscTrim);

	spCluster.broadcastChip = &broadcastChip;
	spCluster.broadcastSlowChip = &broadcastSlowChip;
	spCluster.multicastChip = &multicastChip;

	initChip(spCluster.broadcastChip, &spFamilyMulticast, BROADCAST_I2C_ADDRESS, spFamily.broadcastSlowAddress, spFamily.oscTrim, NULL, CHIP_TYPE_BROADCAST);
	initChip(spCluster.broadcastSlowChip, &spFamilyMulticast, BROADCAST_I2C_ADDRESS, spFamily.broadcastSlowAddress, spFamily.oscTrim, NULL, CHIP_TYPE_BROADCAST_SLOW);
	//initChip(spCluster.broadcastSlowChip, &spFamilyMulticast, BROADCAST_I2C_ADDRESS, "0x0000000000", spFamily.oscTrim, NULL, CHIP_TYPE_BROADCAST_SLOW);
	initChip(spCluster.multicastChip, &spFamilyMulticast, BROADCAST_I2C_ADDRESS, spCluster.multicastAddress, spFamily.oscTrim, NULL, CHIP_TYPE_MULTICAST);

}

void fillConfiguration(){
	spConfiguration.setVCC(&spConfiguration, spConfiguration.vccStringValue);
}

void initAllSPMeasurements(){
	initMeasurement();
	initStatusQI();
	initStatusPOT();
	if(strstr(spConfiguration.description, "RUN5") != NULL){
		initRUN5();
		initRegistersListRUN5();
	} else if (strstr(spConfiguration.description, "RUN7") != NULL){
		initRUN7();
		initRegistersListRUN7();
	}else{ //test riavvio softMCU
		initRUN7();
		initRegistersListRUN7();
	}
	initParameter();
	initParameterPort();
	initParameterADC();
	initParameterQI();
	initParameterEIS();
	initParameterPOT();
	initParameterSENSOR();
}

void initChip(SPChip* c, SPFamily* f, char* i2cAddress, char* serialNumber, char* oscTrimOverride,
		SPSensingElementOnChip* seOnChipList, int chipType){

	c->family = f;
	strcpy(c->i2cAddress, i2cAddress);
	//strcpy(c->serialNumber, serialNumber);
	setSerialNumber(c, serialNumber);
	strcpy(c->oscTrimOverride, oscTrimOverride);
	c->spSensingElementOnChipList = seOnChipList;
	c->chip_type = chipType;

	return;
}

void initPort(SPPort* port){
	for(int i = 0; i < PORT_NUMBER; i++){
		port[i].isInternal = false;
		port[i].noPort = false;
		if(strcmp(portLabels[i], NOPORT) == 0){
			port[i].noPort = true;
		} else {
			strcpy(port[i].portValue, portValues[i]);
			strcpy(port[i].portLabel, portLabels[i]);
			port[i].isInternal = i < MAX_INTERNAL_INDEX;
		}
	}
}

void initConfiguration(SPConfiguration* configuration, SPCluster* cluster, SPProtocol* protocol, char* description){

	configuration->cluster = cluster;
	configuration->protocol = protocol;
	initSPConfiguration(configuration);
	strcpy(configuration->description, description);

	return;
}

void initStatusQI(){
	for(int i = 0; i < NUM_OF_MEASUREMENT; i++){
		qistatus[i].status.firstMeasure = true;
		qistatus[i].status.counter = 0;
	}
}

void initStatusPOT(){
	for(int i = 0; i < NUM_OF_MEASUREMENT; i++){
		potstatus[i].status.firstMeasure = true;
		potstatus[i].status.counter = 0;
	}
}

void initRUN5(){
	for(int i = 0; i < NUM_OF_MEASUREMENT; i++){
		spMeasurementRUNX[i].spMeasurement = &spMeasurement[i];
		spMeasurementRUNX[i].spCluster = &spCluster;
		spMeasurementRUNX[i].qiStatus = &qistatus[i];
		spMeasurementRUNX[i].potStatus = &potstatus[i];
		initSPMeasurementRUN5(&spMeasurementRUNX[i]);
	}
}

void initRUN7(){
	for(int i = 0; i < NUM_OF_MEASUREMENT; i++){
		spMeasurementRUNX[i].spMeasurement = &spMeasurement[i];
		spMeasurementRUNX[i].spCluster = &spCluster;
		spMeasurementRUNX[i].qiStatus = &qistatus[i];
		spMeasurementRUNX[i].potStatus = &potstatus[i];
		initSPMeasurementRUN7(&spMeasurementRUNX[i]);
	}
}

void initMeasurement(){
	for(int i = 0; i < NUM_OF_MEASUREMENT; i++){
		spMeasurement[i].measureIndexToSave = -1;
		spMeasurement[i].spProtocol = &spProtocol;
		spMeasurement[i].spProtocol->addressingMode = FULL_ADDRESS;
	}
}


void initParameter(){
	spParam.temporaryCluster = &spCluster;
	spParam.spConfiguration = NULL;
	spParam.spParamItemFIFO_DEEP.numericalValue = 1;
	spParam.spParamItemFIFO_READ.numericalValue = 1;
	spParam.sequentialMode = false;
	spParam.burstMode = false;
}

void initParameterPort(){
	spParamPort.spMeasurementParameter = &spParam;
	spParamPort.port = &portADC;
}

void initParameterQI(){
	in_Port.port = &in;
	out_Port.port = &out;
	spParamQI.inport = &in_Port;
	spParamQI.outport = &out_Port;
	spParamQI.spMeasurementParameterADC = &spParamADC;
}

void initParameterADC(){
	spParamADC.spMeasurementParameterPort = &spParamPort;
	spParamADC.CommandX20 = true;
	spParamADC.conversionRate = -1;
	spParamADC.spMeasurementParameterPort = &spParamPort;
	spParamADC.spMeasurementParameterPort->spMeasurementParameter = &spParam;
	initSPParamItemInGain(&spParamADC.spParamItemInGain);
	initSPParamItemRsense(&spParamADC.spParamItemRSense);
	initSPParamItemMux(&spParamADC.spParamItemMux);
}

void initParameterEIS(){
	spParamEIS.QIparam = &spParamQI;
	spParamEIS.toString = &printSPMeasurementParameterEIS;
	initSPParamItemContacts(&spParamEIS.Contacts);
	//debug
	spParamEIS_MyNewBatch.QIparam = &spParamQI;
	spParamEIS_MyNewBatch.toString = &printSPMeasurementParameterEIS;
	initSPParamItemContacts(&spParamEIS_MyNewBatch.Contacts);
}

void initParameterPOT(){
	spParamPOT.ADCparam = &spParamADC;
	init_SPMeasurementParameterPOT(&spParamPOT, &spConfiguration);
}

void initParameterSENSOR(){
	spParamSENSOR.spMeasurementParameterADC = &spParamADC;
}

void initMyCounters(){
	cnt = 0;
	frst = 0;
	frq = 0.0f; //poi rinizializzarlo all'ultimo valore salvato
	BiasP = 0;

}
