/*
 * variables.h
 *
 *  Created on: 31 lug 2018
 *      Author: Luca
 */

#ifndef VARIABLES_H_
#define VARIABLES_H_

#include "API/level1/protocols/esp8266/SPProtocolESP8266_SENSIBUS.h"
#include "API/level1/decoder/SPDecoderSensibus.h"
#include "API/initialization/configurations.h"
#include "API/level2/SPMeasurementRunX.h"
#include "API/util/printMCU.h"
#include "xml.h"
#include "IOTMAT/SPDataRepresentationManager.hpp"


/***** Port Index *****/

#define PORT0						0
#define PORT1						1
#define PORT2						2
#define PORT3						3
#define PORT4						4
#define PORT5						5
#define PORT6						6
#define PORT7						7
#define PORT8						8
#define PORT9						9
#define PORT10			   		   10
#define PORT11			   	 	   11
#define PORT_HP			   	   	   12
#define PORT_EXT1		   		   13
#define PORT_EXT2		   		   14
#define PORT_EXT3		      	   15
#define PORT_EXT1_1				   16
#define PORT_EXT2_1				   17
#define PORT_EXT3_1				   18
#define PORT_TEMPERATURE   		   19
#define PORT_VOLTAGE	   		   20
#define PORT_LIGHT		   		   21
#define PORT_DARK		   		   22
#define PORT_NA			   		   23
#define PORT_SHORT				   24
#define PORT_OPEN				   25

/***** SensingElement On Chip Index *****/

#define ONCHIP_DARK					0
#define ONCHIP_LIGHT				1
#define ONCHIP_TEMPERATURE			2
#define ONCHIP_VOLTAGE				3
#define ONCHIP_VOC_PORT0			4
#define ONCHIP_VOC_PORT1			5
#define ONCHIP_VOC_PORT2			6
#define ONCHIP_VOC_PORT3			7
#define ONCHIP_VOC_PORT4			8
#define ONCHIP_ALUMINUM_OXIDE	    9
#define ONCHIP_UREA				   10
#define ONCHIP_VOC_PORT7		   11
#define ONCHIP_VOC_PORT8		   12
#define ONCHIP_VOC_PORT9		   13
#define ONCHIP_VOC_PORT10		   14
#define ONCHIP_POLYMIDE			   15
#define OFFCHIP_HP_DC			   16
#define OFFCHIP_HP_AC			   17
#define OFFCHIP_AU_GRAPHENE_EXT3   18

/***** Chace Index *****/

#define CHA_SELECT		0
#define DSS_SELECT		1
#define DSSDIVIDER		2
#define DAS_CONFIG		3
#define CHA_FILTER		4
#define CHA_CONDIT		5
#define ANA_CONFIG		6
#define CHA_DIVIDER		7
#define CHA_SELECT1		8
#define CHA_CONFIG		9
#define DIG_CONFIG	   10


extern SPFamily spFamily;
extern SPFamily spFamilyMulticast;
extern SPCluster spCluster;
extern SPConfiguration spConfiguration;
extern SPProtocol spProtocol;
extern SPMeasurementRunX spMeasurementRUNX[NUM_OF_MEASUREMENT];
extern SPMeasurement spMeasurement[NUM_OF_MEASUREMENT];
extern SPDecoder spDecoder;
extern SPDecoderGenericInstruction spInstruction;
//extern SPChip spChip;
extern SPPort in, out, portADC;
extern SPPort spPortList[PORT_NUMBER];
extern SPMeasurementStatusQI qistatus[NUM_OF_MEASUREMENT];
extern SPMeasurementStatusPOT potstatus[NUM_OF_MEASUREMENT];
extern int instanceID;

extern SPMeasurementParameterEIS spParamEIS;
extern SPMeasurementParameterEIS spParamEIS_MyNewBatch;
extern SPMeasurementParameterPOT spParamPOT;
extern SPMeasurementParameterQI  spParamQI;
extern SPMeasurementParameterADC spParamADC;
extern SPMeasurementParameterADC spParamADC_MyNewBatch;
extern SPMeasurementParameterPort spParamPort;
extern SPMeasurementParameter spParam;
extern SPMeasurementParameterSENSOR spParamSENSOR;

extern SPMeasurementParameterPort in_Port;
extern SPMeasurementParameterPort out_Port;

extern SPSensingElement spSensingElement[NUM_OF_SE]; //Da definire bene dimensione massima 35
extern SPSensingElementOnFamily spSeOnFamily[NUM_SE_ON_FAMILY]; //Da definire bene dimensione massima 55
extern SPSensingElementOnChip spSeOnChip[NUM_OF_CHIPS][NUM_SE_ON_CHIP]; //Da definire bene dimensione massima 55

extern sp_double output1[NUM_OF_CHIPS][EIS_NUM_OUTPUT];
//extern sp_double outTEMP[NUM_OF_CHIPS][EIS_NUM_OUTPUT];
//extern sp_double outEIS[NUM_OF_CHIPS][EIS_NUM_OUTPUT];

extern struct outValues{
	 sp_double outTEMP[NUM_OF_CHIPS][EIS_NUM_OUTPUT];
	 sp_double outHUMIDITY[NUM_OF_CHIPS][EIS_NUM_OUTPUT]; //poi togliere e accorpare a outEIS
	 sp_double outEIS[NUM_OF_CHIPS][EIS_NUM_OUTPUT];
	 sp_double firstOut[NUM_OF_CHIPS][17];
	 sp_double secondOut[NUM_OF_CHIPS][17];
}outValues;

//extern SPDataRepresentation spAdaptMeasure;

//PENSARE AD UNA STRUTTURA?
extern long avail_frq; // mi serve per calcolare D e Q nei parametri da mostrare a display
extern int cnt, frst;
extern sp_double frq;  // per l'update della freq da gui
extern char *contacts; // contatti settabili via GUI
extern char *in_gain;  // ingain settabile via GUI
extern char *out_gain;  // outgain settabile via GUI
extern char *r_sense;  // rsense settabile via GUI
extern int BiasP; //dcBiasP settabile via GUI
extern int ingain_rsense_index;



extern SPprotocolOut appoProtocol;
extern SPChip spChiplist[NUM_OF_CHIPS];
extern char chace[NUM_OF_INSTRUCTIONS][17];

extern SPDriverESP8266 testDriver;
extern SPDriver spDriver;

extern int ADDRESSING_MODE;

void initVariables();
void initSPVariables();
void initSPProtocolESP8266();
void initAllSPMeasurements();
void fillChip();
void fillConfiguration();
void findChip(SPChip* spChip, char* serialNumber);

#endif /* VARIABLES_H_ */
