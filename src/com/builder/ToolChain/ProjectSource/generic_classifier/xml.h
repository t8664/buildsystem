/*
 * xml.h
 *
 *  Created on: 12 dic 2019
 *      Author: Luca Gerevini
 */

#ifndef XML_H_
#define XML_H_

/***** Configuration XML*****/
#define NUM_OF_CHIPS		5//10
#define NUM_OF_MEASUREMENT	10
#define NUM_SE_ON_FAMILY	60//55
#define NUM_SE_ON_CHIP		60//55
#define NUM_OF_SE			50//35



#endif /* XML_H_ */
