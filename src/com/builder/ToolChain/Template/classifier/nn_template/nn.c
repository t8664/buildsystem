#include "nn.h"

// Funzione ReLU
nn_data relu(nn_data v){
	if (v > 0) return v;
	return 0;
}

// Funzione sigmoid
nn_data sigmoid(nn_data x)
{
     nn_data exp_value;
     nn_data return_value;

     // Calcolo dell'esponenziale
     exp_value = exp(-x);

     // Calcolo sigmoide
     return_value = 1 / (1 + exp_value);

     return return_value;
}

// Setta i nodi di input con i valori dell'array measure
void set_input(neural_network * nn, nn_data * measure){
	for(int i=0; i < INPUT_SIZE; i++) nn->input_nodes[i] = measure[i];
}

// Funzione di attivazione
nn_data activ_func(nn_data v){
	return relu(v);
}

// Propaga il nodo bias di input
void prop_input_bias(neural_network * nn){

	for(int j=0; j<HIDDEN_SIZE; j++){
		(nn->hidden_nodes)[j]  += (nn->input_bias_node)*((nn->input_bias_weights)[j]);
	}

}

// Propaga il nodo bias di output
void prop_output_bias(neural_network * nn){

	for(int j=0; j<OUTPUT_SIZE;j++){
		(nn->output_nodes)[j] += (nn->output_bias_node)*((nn->output_bias_weights)[j]);

	}

}

// Propagazione input-hidden (nodo input i verso strato hidden)
void prop_input_hidden(neural_network * nn, int i){

	for(int j=0; j<HIDDEN_SIZE; j++){
		(nn->hidden_nodes)[j] += ((nn->input_nodes)[i])*((nn->input_weights)[i][j]);
	}
}

// Propagazione hidden-output (nodo hidden i verso strato output)
void prop_hidden_output(neural_network * nn, int i){

	for(int j=0; j<OUTPUT_SIZE; j++){
		(nn->output_nodes)[j] += ((nn->hidden_nodes)[i])*((nn->output_weights)[i][j]);
	}
}

// Applica la funzione di attivazione allo strato hidden
void activ_hidden_nodes(neural_network * nn){

	for(int j=0; j<HIDDEN_SIZE; j++){
		(nn->hidden_nodes)[j] = activ_func((nn->hidden_nodes)[j]);
	}
}

// Applica la funzione di attivazione allo strato output
void activ_output_nodes(neural_network * nn){

	for(int j=0; j<OUTPUT_SIZE; j++){
		(nn->output_nodes)[j] = activ_func((nn->output_nodes)[j]);
	}
}

// Azzera i valori sugli strati input-hidden-output (mantiene i pesi)
void reset_data_neural_network(neural_network * nn){


	for(int i=0; i<INPUT_SIZE; i++){
		(nn->input_nodes)[i] = 0;
	}

	for(int i=0; i<HIDDEN_SIZE; i++){
		(nn->hidden_nodes)[i] = 0;
	}

	for(int i=0; i<OUTPUT_SIZE; i++){
		(nn->output_nodes)[i] = 0;
	}
}

// Genera un numero in virgola mobile random tra -max_value e +max_value
nn_data get_random_value(nn_data max_value){

	nn_data sign;

	if (((nn_data)rand()/(nn_data)(RAND_MAX)) > 0.5) sign = -1;
	else sign = 1;

	nn_data random_value = sign*(((nn_data)rand()/(nn_data)(RAND_MAX)) * max_value);
	return random_value;

}

// Inizializza la rete neurale con dei pesi random
void init_random_neural_network(neural_network * nn){

	reset_data_neural_network(nn); // Pone gli strati input-hidden-output a zero

	srand((unsigned int)time(NULL)); // Init seme random

	//importazione dei pesi
		for(int i=0; i<INPUT_SIZE; i++){
			for(int j=0; j<HIDDEN_SIZE; j++){
				(nn->input_weights)[i][j] = get_random_value(MAX_WEIGHT_VALUE);
			}
		}

		for(int j=0; j<HIDDEN_SIZE; j++){
			for(int t=0; t<OUTPUT_SIZE; t++){
				(nn->output_weights)[j][t] = get_random_value(MAX_WEIGHT_VALUE);
			}
		}

		for(int j=0; j<HIDDEN_SIZE; j++){
			(nn->input_bias_weights)[j] = get_random_value(MAX_WEIGHT_VALUE);
		}

		for(int j=0;j<OUTPUT_SIZE;j++){
			(nn->output_bias_weights)[j] = get_random_value(MAX_WEIGHT_VALUE);
		}

		nn->input_bias_node = 1;
		nn->output_bias_node = 1;
}

// Inizializza la rete neurale con i pesi presenti nell'header "weights.h"
void init_neural_network(neural_network * nn){

	reset_data_neural_network(nn); // Pone gli strati input-hidden-output a zero

	//importazione dei pesi
	for(int i=0; i<INPUT_SIZE; i++){
		for(int j=0; j<HIDDEN_SIZE; j++){
			(nn->input_weights)[i][j] = weights1[i][j];

		}
	}

	for(int j=0; j<HIDDEN_SIZE; j++){
		for(int t=0; t<OUTPUT_SIZE; t++){
			(nn->output_weights)[j][t] = weights2[j][t];
		}
	}

	for(int j=0; j<HIDDEN_SIZE; j++){
		(nn->input_bias_weights)[j] = biases1[j];
	}

	for(int j=0;j<OUTPUT_SIZE;j++){
		(nn->output_bias_weights)[j] = biases2[j];
	}

	nn->input_bias_node = 1;
	nn->output_bias_node = 1;
}


// Stampa i valori dello strato di input
void print_input_nodes(neural_network * nn){

	for(int i=0; i<INPUT_SIZE; i++){
		printf("nodo input [%d] = %f \n", i, (nn->input_nodes)[i]);
	}
}

// Stampa i valori dello strato di hidden
void print_hidden_nodes(neural_network * nn){

	for(int i=0; i<HIDDEN_SIZE; i++){
		printf("nodo hidden [%d] = %f \n", i, (nn->hidden_nodes)[i]);
	}
}

// Stampa i valori dello strato di output
void print_output_nodes(neural_network * nn){

	for(int i=0; i<OUTPUT_SIZE; i++){
		printf("nodo output [%d] = %f \n", i, (nn->output_nodes)[i]);
	}
}

// Stampa il valore del nodo bias di input
void print_input_bias_node(neural_network * nn){

	printf("nodo bias input = %f \n", (nn->input_bias_node));

}

// Stampa il valore del nodo bias di output
void print_output_bias_node(neural_network * nn){

	printf("nodo bias output = %f \n", (nn->output_bias_node));
}

// Stampa i pesi tra il nodo bias di input e lo starto hidden
void print_input_bias_weights(neural_network * nn){

	for(int j=0; j<HIDDEN_SIZE; j++){
		printf("peso bias input [%d] = %f \n", j, (nn->input_bias_weights)[j]);
	}
}

// Stampa i pesi tra il nodo bias di output e lo strato di output
void print_output_bias_weights(neural_network * nn){

	for(int j=0; j<OUTPUT_SIZE; j++){
		printf("peso bias output [%d] = %f \n", j,  (nn->output_bias_weights)[j]);
	}
}

// Stampa i pesi tra lo strato di input e quello hidden
void print_input_weights(neural_network * nn){

	for(int i=0; i<INPUT_SIZE; i++){
		for(int j=0; j<HIDDEN_SIZE; j++){
			printf("peso input [%d][%d] = %lf \n", i, j, (nn->input_weights)[i][j]);
		}
	}
}

// Stampa i pesi tra lo starto hidden e quello di output
void print_output_weights(neural_network * nn){

	for(int j=0; j<HIDDEN_SIZE; j++){
		for(int t=0; t<OUTPUT_SIZE; t++){
			printf("peso output [%d][%d] = %f \n", j, t, (nn->output_weights)[j][t]);
		}
	}

}

// Stampa tutti i pesi (di input e di output)
void print_weights(neural_network * nn){

	print_input_weights(nn);
	print_output_weights(nn);
}

// Stampa tutti i valori dei nodi (input-hidden-output)
void print_nodes(neural_network * nn){

	print_input_nodes(nn);
	print_hidden_nodes(nn);
	print_output_nodes(nn);
}

// Stampa tutte le informazioni sullo stato della rete neurale
void print_stats(neural_network * nn){

	printf("NODI DI INPUT: \n");
	print_input_nodes(nn);
	printf("NODI HIDDEN: \n");
	print_hidden_nodes(nn);
	printf("NODI DI OUTPUT: \n");
	print_output_nodes(nn);
	printf("NODI BIAS: \n");
	print_input_bias_node(nn);
	print_output_bias_node(nn);

	printf("PESI DI INPUT: \n");
	print_input_weights(nn);
	printf("PESI DI OUTPUT: \n");
	print_output_weights(nn);
	printf("PESI INPUT BIAS: \n");
	print_input_bias_weights(nn);
	printf("PESI OUTPUT BIAS: \n");
	print_output_bias_weights(nn);
}

// Estrae il valore massimo da un array di valori
nn_data extract_max(nn_data * input, int input_len){

	nn_data m = input[0];
    	for (int i = 1; i < input_len; i++) {
        	if (input[i] > m) {
            		m = input[i];
		}
	}
	return m;
}

// Estrae l'indice che punta al valore massimo di un array di valori
int extract_max_index(nn_data * input, int input_len){

	int j = 0;
    	for (int i = 1; i < input_len; i++) {
        	if (input[i] > input[j]) {
            		j = i;
		}
	}
	return j;
}

// Normalizza il vettore di nn_data/double in numeri di probabilit� con somma totale pari a 1
void softmax(nn_data * input, int input_len){

	nn_data m = extract_max(input, input_len); //max value of vector
	nn_data sum = 0;

    	for (int i = 0; i < input_len; i++) {
        	sum += exp((double) (input[i]-m));
	}

	for (int i = 0; i < input_len; i++) {
        	input[i] = exp((double)(input[i] - m - log((double)sum)));
	}

}

// Restituisce la classe predetta per quell'ingresso di input
int compute_class_found(neural_network * nn){

	int i = extract_max_index(nn->output_nodes, OUTPUT_SIZE);
	return i;
}

// Stampa la matrice di confusione
void print_confusion_matrix(int confusion_matrix[OUTPUT_SIZE][OUTPUT_SIZE]){
	printf("Matrice di Confusione: \n");
	for(int i=0;i < OUTPUT_SIZE; i++){
		for(int j=0;j < OUTPUT_SIZE; j++){
			printf("%d ", confusion_matrix[i][j]);
		}
		printf("\n");
	}
}

// Propaga i valori di input nell'output
void predict(neural_network * nn){
	for(int i=0; i < INPUT_SIZE; i++) prop_input_hidden(nn, i); 	// Propaga la misura nello strato hidden
	prop_input_bias(nn);						// Propaga il nodo bias di input nello strato hidden
	activ_hidden_nodes(nn);						// Applica la funzione di attivazione allo strato hidden
	for(int j=0; j< HIDDEN_SIZE; j++) prop_hidden_output(nn, j); 	// Propaga lo strato hidden in output
	prop_output_bias(nn);						// Propaga il nodo bias di hidden nello strato output
	if (SOFTMAX) softmax(nn->output_nodes, OUTPUT_SIZE);
}
