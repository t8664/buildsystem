package com.builder.core;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class BoardPartitionsScheme {


    private String separator;

    public class PartitionInfo{
        public String name;
        public String type;
        public String subType;
        public String offset;
        public String size;
        public String flags;
    }

    public List<PartitionInfo> partitionsList;

    public class DataPartition {
        public int fsPageSizeBytes;
        public int fsBlockSizeBytes;
        public long fsPartitionSizeBytes;
        public String fsHexOffset;
        public String fsType;   // spiffs / ffat
    }

    public DataPartition dataPartition;

    public BoardPartitionsScheme() {
        dataPartition = new DataPartition();
        partitionsList = new ArrayList<PartitionInfo>();
        separator = ",";

        //SETTING default data partition params from default.csv with spiffs 4MB(IoTMat) --TESTING purpose
        dataPartition.fsPageSizeBytes = 256;
        dataPartition.fsBlockSizeBytes = 4096;
        dataPartition.fsType = "spiffs";
        //dataPartition.fsHexOffset = "0x290000";
        //dataPartition.fsPartitionSizeBytes = Long.decode("0x170000");
        dataPartition.fsHexOffset = "0x310000";
        dataPartition.fsPartitionSizeBytes = Long.decode("0xF0000"); /* Huge APP*/

    }

    public BoardPartitionsScheme(String csvFullPath) {
        dataPartition = new DataPartition();
        partitionsList = new ArrayList<PartitionInfo>();
        separator = ",";
        parseCSV(csvFullPath);
        setDataPartitionType();

    }

    public BoardPartitionsScheme(String csvFullPath, String separator) {
        dataPartition = new DataPartition();
        partitionsList = new ArrayList<PartitionInfo>();
        this.separator = separator;
        parseCSV(csvFullPath);
        setDataPartitionType();
    }

    public void printPartitionsListOnConsole(){

        for(int i = 0; i < partitionsList.size(); i++)
        {
            System.out.println(
                    partitionsList.get(i).name + "\t" +
                    partitionsList.get(i).type + "\t" +
                    partitionsList.get(i).subType + "\t" +
                    partitionsList.get(i).offset + "\t" +
                    partitionsList.get(i).size + "\t" +
                    partitionsList.get(i).flags
            );
        }

    }

    private void setDataPartitionType()
    {
        //
        for(int i = 0; i < partitionsList.size(); i++)
        {
            if(partitionsList.get(i).name.equals("spiffs")){
                //setting default parameters
                dataPartition.fsPageSizeBytes = 256;
                dataPartition.fsBlockSizeBytes = 4096;
                dataPartition.fsType = partitionsList.get(i).name;
                dataPartition.fsHexOffset = partitionsList.get(i).offset;
                dataPartition.fsPartitionSizeBytes = Long.decode(partitionsList.get(i).size);

            }else if(partitionsList.get(i).name.equals("ffat")){

                //unused require manual params (fsblock ecc)
                dataPartition.fsType = partitionsList.get(i).name;
                dataPartition.fsHexOffset = partitionsList.get(i).offset;
                dataPartition.fsPartitionSizeBytes = Long.decode(partitionsList.get(i).size);
            }


        }
    }


    private void parseCSV(String csvFullPath)
    {
        String line = "";
        PartitionInfo linePartition;
        try (BufferedReader br = new BufferedReader(new FileReader(csvFullPath))) {

            while ((line = br.readLine()) != null) {

                if(line.startsWith("#")) continue;
                String[] element = line.split(this.separator);

                partitionsList.add(
                    new PartitionInfo() {{
                        this.name = element[0].trim();
                        this.type = element[1].trim();
                        this.subType = element[2].trim();
                        this.offset = element[3].trim();
                        this.size = element[4].trim();
                        if(element.length == 6) this.flags=element[5].trim();
                    }}
                );

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
