package com.builder.core;

public class ESP32BoardConfig {
    public String chip;
    public String port;
    public String baud;
    public String flashMode;
    public String flashFreq;
    public String flashSize;


    public ESP32BoardConfig(){
        chip="";
        port="";
        baud="";
        flashMode="";
        flashFreq="";
        flashSize="";
    }

    public ESP32BoardConfig(String chip, String port, String baud, String flashMode, String flashFreq, String flashSize ){
        this.chip=chip;
        this.port=port;
        this.baud=baud;
        this.flashMode=flashMode;
        this.flashFreq=flashFreq;
        this.flashSize=flashSize;
    }


    //region SETTER+GETTER AUTOGEN
    public String getChip() {
        return chip;
    }

    public void setChip(String chip) {
        this.chip = chip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getBaud() {
        return baud;
    }

    public void setBaud(String baud) {
        this.baud = baud;
    }

    public String getFlashMode() {
        return flashMode;
    }

    public void setFlashMode(String flashMode) {
        this.flashMode = flashMode;
    }

    public String getFlashFreq() {
        return flashFreq;
    }

    public void setFlashFreq(String flashFreq) {
        this.flashFreq = flashFreq;
    }

    public String getFlashSize() {
        return flashSize;
    }

    public void setFlashSize(String flashSize) {
        this.flashSize = flashSize;
    }
    //endregion
}
