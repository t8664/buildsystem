package com.builder.core;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.*;
import static java.nio.file.FileVisitResult.*;
import java.util.*;

    public  class Finder extends SimpleFileVisitor<Path> {

        List<String> makeFiles = new ArrayList<String>();
        List<String> ignoredFiles;
        public Finder() {
            //
           //List<String> tmpList=new ArrayList<String>();
           //tmpList.add("lock.lock");
            this(Arrays.asList("lock.lock", "partitions.csv"));
        }

        public Finder(List<String> ignoredFiles) {
            this.ignoredFiles= new ArrayList<String>(ignoredFiles);
        }
        void findAndRemove(Path file) {
            Path name = file.getFileName();
            if (name != null) {
                if(name.toString().equalsIgnoreCase("makefile") || getExtension(name.toString()).equals("mk")) {
                    makeFiles.add(file.toString());
                    //System.out.println(file);
                }else
                {
                    try {
                        Files.delete(file);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        public List<String>  result() {
            return makeFiles;
        }
        // Invoke the pattern matching
        // method on each file.
        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {

            String test=file.getFileName().toString();
            if(ignoredFiles.contains(file.getFileName().toString()))
                return CONTINUE;

            findAndRemove(file);
            return CONTINUE;
        }

        // Invoke the pattern matching
        // method on each directory.
        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
            //find(dir);
            return CONTINUE;
        }

        @Override
        public FileVisitResult visitFileFailed(Path file,
                                               IOException exc) {
            System.err.println(exc);
            return CONTINUE;
        }

        public String getExtension(String fileName) {

            int i = fileName.lastIndexOf('.');
            if (i > 0) {
                return fileName.substring(i+1);
            }

            return "";
        }
    }


