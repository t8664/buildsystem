package com.builder.core;

import com.util.CopyDir;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;


public class MakeConfigGenerator {

    private String os;
    protected String target;
    private final String lockFile="lock.lock";
    private final String hardwareTag="_HARDWARE_";
    private final String extLibTag="_EXT_LIB_";
    private final String outDirTag="_OUT_DIR_";
    private final String prjSourceTag="_PRJ_SOURCE_";


    //compiler names
    protected LinkedHashMap<String,String> executable = new  LinkedHashMap<String,String>() {{
        put("gcc", null);
        put("gpp", null);
        put("elfSize", null);
        put("elfAr", null);
    }};
    //python script??

    //compiler full path
    protected LinkedHashMap<String,String> fullPaths = new  LinkedHashMap<String,String>() {{
        put("gcc", null);
        put("gpp", null);
        put("elfSize", null);
        put("elfAr", null);
        put("outputDir", null);
        put("projectSourceDir", null);
        put("projectSkelDir", null);
        put("hardwareDir", null);
        put("extLibDir", null);
        put("targetOutDir", null);
    }};

    public MakeConfigGenerator(String target) throws IOException {

        this.target=target;
        //check location of ToolChain folder (for future production purpose)
        os=System.getProperty("os.name");

        String sysPath=getExecutionPath();

        System.out.println(sysPath);
        if(Files.notExists(Paths.get(sysPath+"/ToolChain"))){
            sysPath=sysPath+"/"+MakeConfigGenerator.class.getPackage().getName().replaceAll("\\.", "/");
            if(Files.notExists(Paths.get(sysPath+"/ToolChain")))
            {
             sysPath = sysPath.replaceAll("/core$","");
                if(Files.notExists(Paths.get(sysPath+"/ToolChain"))) {
                    System.out.println("No ToolChain found!");
                    //raise error ToolChain not found
                    //throw new IOException("ToolChain not found!");
                }
            }
        }



        if(os.startsWith("Linux"))
        {

            executable.replace("gcc", "ToolChain/Platform/linux/arduino-esp32/tools/xtensa-esp32-elf/bin/xtensa-esp32-elf-gcc");
            executable.replace("gpp", "ToolChain/Platform/linux/arduino-esp32/tools/xtensa-esp32-elf/bin/xtensa-esp32-elf-g++");
            executable.replace("elfSize", "ToolChain/Platform/linux/arduino-esp32/tools/xtensa-esp32-elf/bin/xtensa-esp32-elf-size");
            executable.replace("elfAr", "ToolChain/Platform/linux/arduino-esp32/tools/xtensa-esp32-elf/bin/xtensa-esp32-elf-ar");

            fullPaths.replace("hardwareDir", sysPath+"/ToolChain/Platform/linux");

        }else if(os.startsWith("Windows")) //da verificare
        {
            //TODO: check exe names
            executable.replace("gcc", "ToolChain/Platform/windows/arduino-esp32/tools/xtensa-esp32-elf/bin/xtensa-esp32-elf-gcc.exe");
            executable.replace("gpp", "ToolChain/Platform/windows/arduino-esp32/tools/xtensa-esp32-elf/bin/xtensa-esp32-elf-g++.exe");
            executable.replace("elfSize", "ToolChain/Platform/windows/arduino-esp32/tools/xtensa-esp32-elf/bin/xtensa-esp32-elf-size.exe");
            executable.replace("elfAr", "ToolChain/Platform/windows/arduino-esp32/tools/xtensa-esp32-elf/bin/xtensa-esp32-elf-ar.exe");

            fullPaths.replace("hardwareDir", sysPath+"/ToolChain/Platform/windows");
        }else{
            //OS not supported -> raise error

        }



        //generation system and location dependent full paths --probabilmente da levare, problema semplificato

        for(String key : executable.keySet() )
        {
            fullPaths.replace(key,sysPath+"/"+executable.get(key));
        }

        fullPaths.replace("outputDir", sysPath+"/Release"); // SI POTREBBE ANCHE FAR SETTARE LATO UTILIZZATORE
        fullPaths.replace("projectSourceDir", sysPath+"/ToolChain/ProjectSource");
        fullPaths.replace("projectSkelDir", sysPath+"/ToolChain/ProjectSkel");
        fullPaths.replace("extLibDir", sysPath+"/ToolChain/ExtLibraries");
        fullPaths.replace("targetOutDir", fullPaths.get("outputDir") + "/" + target);
    }

    public MakeConfigGenerator() throws IOException {
        this("Test");
    }

    public void makeConfig(){
        try {

            if(Files.notExists(Paths.get(this.fullPaths.get("outputDir"))))
                Files.createDirectory(Paths.get(this.fullPaths.get("outputDir")));

            if(cpSkelDir(this.target, this.fullPaths.get("outputDir")))
            {
                // la configurazione non era aggiornata
                // è stata ricopiata quella di default,
                // bisogna quindi risostituire i path giusti

                Finder finder = new Finder(Arrays.asList(this.lockFile, "partitions.csv")); //OBS: rimuove tutti i file relativi a compilazioni precedenti,
                                                                                            // affinché ad ogni aggiornamento di configurazione si lavori in maniera "pulita"
                Files.walkFileTree(Paths.get(this.getFullPaths().get("outputDir")+"/"+this.target), finder);
                List<String> makeFiles = finder.result();
                String content;


                for(String i : makeFiles){
                    content = new String(Files.readAllBytes(Paths.get(i)), StandardCharsets.UTF_8);
                  content = content.replaceAll(hardwareTag, this.fullPaths.get("hardwareDir"));
                  //content = content.replaceAll(hardwareTag, "/home/zen/Documents/sloeber-workspace/Arduino/hardware");
                  //content = content.replaceAll(hardwareTag, "/home/zen/Desktop/hardware");
                  content = content.replaceAll(extLibTag, this.fullPaths.get("extLibDir"));
                  content = content.replaceAll(outDirTag, this.fullPaths.get("outputDir") + "/" + this.target);
                  content = content.replaceAll(prjSourceTag, this.fullPaths.get("projectSourceDir")+ "/" + this.target);
                    //content =content.replaceAll(prjSourceTag, "..");
                   //if(os.startsWith("Windows"))
                   //{
                   //    content = content.replace("/", "\\");
                   //}
                    Files.write(Paths.get(i), content.getBytes(StandardCharsets.UTF_8));
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void cleanConfig(){
        //equivalente di un " make clean" ma senza l'ausilio di quest'ultimo
        String lockFilePath=this.fullPaths.get("outputDir") +"/"+ target + "/" + this.lockFile;
        if(Files.exists(Paths.get(lockFilePath)))
        {
            try {
                Files.delete(Paths.get(lockFilePath));
                this.makeConfig();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private boolean cpSkelDir(String target, String dst) throws IOException {

        if(target == null ) target=this.target;

        if(dst == null )
            dst=this.fullPaths.get("outputDir") +"/"+ target;
        else
            dst= dst +"/"+ target;

        String src;
        if(os.startsWith("Windows")) {
            src = this.fullPaths.get("projectSkelDir") + "/" + target + "_windows";
        }else if(os.startsWith("Linux"))
            src = this.fullPaths.get("projectSkelDir") + "/" + target + "_linux";
        else
            src = this.fullPaths.get("projectSkelDir") + "/" + target + "_linux"; 

        boolean needCopy=false;
        String lockFilePath=dst + "/" + this.lockFile;

        try {

            if(Files.exists(Paths.get(dst)))
                throw new FileAlreadyExistsException("Directory exists");
            else
                needCopy=true;

        } catch (FileAlreadyExistsException e) {
            //directory presente controllo se corretta( per evitare errori dovuti a copie e cambiamenti di percorso)
            if ( Files.notExists(Paths.get(lockFilePath)) || !Files.readAllLines(Paths.get(lockFilePath)).get(0).equals(lockFilePath) )
                needCopy = true;
        }
        finally {
            if(needCopy){
                Files.walkFileTree(Paths.get(src), new CopyDir(Paths.get(src), Paths.get(dst), true));
                //aggiungo il file di controllo
                Files.write(Paths.get(lockFilePath), lockFilePath.getBytes());
                return true;
            }
        }

        return false;
    }

    public void printEnvOnConsole(){

        System.out.println("### ENV MakeConfigGenerator VARS ###");
        System.out.println(" OS = "+os);
        System.out.println(" Package dir = "+MakeConfigGenerator.class.getPackage().getName());
        System.out.println(" UserDirectory = "+System.getProperty("user.dir"));
        System.out.println(" Execution Path = "+this.getExecutionPath());
        fullPaths.entrySet().forEach(entry->{
            System.out.println(" "+entry.getKey() + ": " + entry.getValue());
        });
        System.out.println("### END MakeConfigGenerator VARS ###");


    }

    private String getExecutionPath() {
        String absolutePath = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
        absolutePath = absolutePath.substring(0, absolutePath.lastIndexOf("/"));
        absolutePath = absolutePath.replaceAll("%20", " ");
        //windows test
        //absolutePath = absolutePath.replaceAll(":", "");
        if(System.getProperty("os.name").startsWith("Windows"))
        {
            absolutePath = absolutePath.substring(1);
        }
        return absolutePath;
    }

    public String getOs() {
        return os;
    }

    public  LinkedHashMap<String, String> getExecutable() {
        return executable;
    }

    public  LinkedHashMap<String, String> getFullPaths() {
        return fullPaths;
    }

    public void setFullPaths(LinkedHashMap<String, String> fullPaths) {
        this.fullPaths = fullPaths;
    }

    public void setProjSrcDir(String newDir){
        fullPaths.replace("projectSourceDir", newDir);
    }
}
