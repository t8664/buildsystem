package com.builder.core;

import java.io.File;
import java.io.IOException;

public abstract class Target extends MakeConfigGenerator {

    private String genEsp32Path;
    private String esptoolPath;
    private String makePath;
    private String makeSPIFFS;
    public ESP32BoardConfig boardConfig;
    public BoardPartitionsScheme partitionsScheme;

    public Target(String target) throws IOException {
        super(target);

        boardConfig= new ESP32BoardConfig(  //IoTMat default parameters
                "esp32",
                "OSDependent", //use runtime value
                //"115200",
                "921600",
                "dio",
                "40m",
                "4MB"  //4MB
        );
        String os=System.getProperty("os.name");


        if(os.startsWith("Linux"))
        {
            boardConfig.setPort("/dev/ttyUSB0"); //use runtime value
            //testing with OS exe
           makePath="make";
           esptoolPath= this.fullPaths.get("hardwareDir") + "/arduino-esp32/tools/esptool/esptool.py";
           genEsp32Path =this.fullPaths.get("hardwareDir") + "/arduino-esp32/tools/gen_esp32part.py";


        }else if(os.startsWith("Windows")) //da verificare
        {
            boardConfig.setPort("COM1"); //use runtime value
            //makePath="make";
            makePath = this.fullPaths.get("hardwareDir") + "/bin/make";
            esptoolPath = this.fullPaths.get("hardwareDir") + "/bin/esptool";
            genEsp32Path = this.fullPaths.get("hardwareDir") + "/bin/gen_esp32part";

        }else{
            //OS not supported -> raise error

        }

        makeSPIFFS = this.fullPaths.get("hardwareDir") + "/bin/mkspiffs";

        partitionsScheme =  new BoardPartitionsScheme();

    }

    @Override
    public void printEnvOnConsole() {
        super.printEnvOnConsole();
        System.out.println("### ENV Target VARS ###");
        System.out.println(" esptoolPath = "+ this.esptoolPath);
        System.out.println(" makePath = "+ this.makePath);
        System.out.println(" genEsp32Path = "+ this.genEsp32Path);
        System.out.println("### END Target VARS ###");
    }

    public void generatePartitionsBin(String partitionsCsvPath, String outputBinPath) throws IOException {
        String command= genEsp32Path +
                " -q " +
                partitionsCsvPath +
                " " +
                outputBinPath;

        //System.out.println(command);
        ProcessBuilder pb = new ProcessBuilder(
                //"python",
                genEsp32Path,
                partitionsCsvPath,
                outputBinPath
        );
        //ProcessBuilder pb = new ProcessBuilder("ls", "-l");
        pb.directory(new File(this.fullPaths.get("targetOutDir")));
        pb.redirectErrorStream(true);
        pb.inheritIO();
        Process p = pb.start();
        try {
            p.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(p.exitValue());

    }

    public void generateFirmware(String elfPath, String outputBinPath) throws IOException {

        ProcessBuilder pb = new ProcessBuilder(
                //"python",
                esptoolPath,
                "--chip", boardConfig.chip,
                "elf2image",
                "--flash_mode", boardConfig.flashMode,
                "--flash_freq", boardConfig.flashFreq,
                "--flash_size", boardConfig.flashSize,
                "-o", outputBinPath,
                elfPath
        );
        //ProcessBuilder pb = new ProcessBuilder("ls", "-l");
        pb.directory(new File(this.fullPaths.get("targetOutDir")));
        pb.redirectErrorStream(true);
        pb.inheritIO();
        Process p = pb.start();
        try {
            p.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(p.exitValue());

    }

    public void uploadFirmware(String flashBinPath, String partitionBinPath) throws IOException {
        //String command= esp
        ProcessBuilder pb = new ProcessBuilder(
                //"python",
                esptoolPath,
                "--chip", boardConfig.chip,
                "--port", boardConfig.port,
                "--baud", boardConfig.baud,
                "--before", "default_reset",
                "--after", "hard_reset",
                "write_flash","-z",
                "--flash_mode", boardConfig.flashMode,
                "--flash_freq", boardConfig.flashFreq,
                "--flash_size", "detect",
                "0xe000", this.fullPaths.get("hardwareDir") + "/arduino-esp32/tools/partitions/boot_app0.bin",
                "0x1000", this.fullPaths.get("hardwareDir") + "/arduino-esp32/tools/sdk/bin/bootloader_qio_40m.bin",
                "0x10000", flashBinPath,
                "0x8000", partitionBinPath
        );
        //ProcessBuilder pb = new ProcessBuilder("ls", "-l");
        //pb.directory(new File(this.fullPaths.get("targetOutDir")));
        pb.redirectErrorStream(true);
        pb.inheritIO();
        Process p = pb.start();
        try {
            p.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(p.exitValue());
    }

    public void startCompilation() throws IOException {
        ProcessBuilder pb = new ProcessBuilder(makePath, "all");
        pb.directory(new File(this.fullPaths.get("targetOutDir")));
        pb.redirectErrorStream(true);
        pb.inheritIO();
        //pb.redirectOutput(ProcessBuilder.Redirect.PIPE);
        Process p = pb.start();

        System.out.println("Compilation Started...  ");
        try {
            p.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.print(p.exitValue());
    }

    public void generateSPIFFS(String dataDirectory, String outputBinPath) throws IOException {
//       ProcessBuilder pb = new ProcessBuilder(
//               makeSPIFFS,
//               "-c", dataDirectory,
//               "-p", "256",
//               "-b", "4096",
//               "-s", "1507328",
//               outputBinPath
//       );
         ProcessBuilder pb = new ProcessBuilder(
               makeSPIFFS,
               "-c", dataDirectory,
               "-p", String.valueOf(partitionsScheme.dataPartition.fsPageSizeBytes),
               "-b", String.valueOf(partitionsScheme.dataPartition.fsBlockSizeBytes),
               "-s", String.valueOf(partitionsScheme.dataPartition.fsPartitionSizeBytes),
                 //"-s", "3080192",
               outputBinPath
       );

        //pb.directory(new File(this.fullPaths.get("targetOutDir")));
        pb.redirectErrorStream(true);
        pb.inheritIO();
        Process p = pb.start();
        try {
            p.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(p.exitValue());
    }

    public void uploadSPIFF(String flashSPIFFSPath) throws IOException {
        ProcessBuilder pb = new ProcessBuilder(
                //"python",
                esptoolPath,
                "--chip", boardConfig.chip,
                "--port", boardConfig.port,
                "--baud", boardConfig.baud,
                "--before", "default_reset",
                "--after", "hard_reset",
                "write_flash","-z",
                "--flash_mode", boardConfig.flashMode,
                "--flash_freq", boardConfig.flashFreq,
                "--flash_size", "detect",
                partitionsScheme.dataPartition.fsHexOffset, flashSPIFFSPath
               // "0x110000", flashSPIFFSPath
        );
        System.out.println(pb.command().toString());
        //ProcessBuilder pb = new ProcessBuilder("ls", "-l");
        //pb.directory(new File(this.fullPaths.get("targetOutDir")));
        pb.redirectErrorStream(true);
        pb.inheritIO();
        Process p = pb.start();
        try {
            p.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(p.exitValue());

    }

    public abstract void preExecution();
    public abstract void targetBuildExecution();
    public abstract void postExecution();
}
