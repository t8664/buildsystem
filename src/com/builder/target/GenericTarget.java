package com.builder.target;

import com.builder.core.Target;

import java.io.IOException;

public class GenericTarget extends Target implements Runnable{


    public GenericTarget(String target) throws IOException {
        super(target);
    }

    @Override
    public  void preExecution(){ };

    @Override
    public  void targetBuildExecution(){

        this.makeConfig();

        try {
            this.startCompilation();
        } catch (IOException e) {
            e.printStackTrace();
        }
    };

    @Override
    public  void postExecution(){
        try {
            this.generatePartitionsBin(

                    this.fullPaths.get("targetOutDir") + "/partitions.csv",
                    //"/tmp/" + this.target.toLowerCase() + ".partitions.bin"
                    this.target.toLowerCase() + ".partitions.bin"
            );

            this.generateFirmware(
                    this.fullPaths.get("targetOutDir") + "/"+ this.target.toLowerCase() + ".elf",
                    this.target.toLowerCase() + ".bin"
            );

            this.uploadFirmware(
                    this.fullPaths.get("targetOutDir") + "/" + this.target.toLowerCase() + ".bin",
                    this.fullPaths.get("targetOutDir") + "/" + this.target.toLowerCase() + ".partitions.bin"
            );

        }catch (Exception e) {
            e.printStackTrace();
        }
    };


    @Override
    public void run(){
        preExecution();
        targetBuildExecution();
        postExecution();
    }
}
