package com.builder.target;

import com.builder.core.Target;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class UploadSPIFFSTarget extends Target implements Runnable {

    private String folderFullPath;

    public UploadSPIFFSTarget() throws IOException {
        super("Dontcare");
        folderFullPath=null;
    }

    @Override
    public void preExecution() {

    }

    @Override
    public void targetBuildExecution() {

    }

    @Override
    public void postExecution() {

            try {
                if( folderFullPath == null )
                    throw new NullPointerException("Parameters not defined");

                Path tempDirWithPrefix = Files.createTempDirectory("spifOut");
                System.out.println(tempDirWithPrefix.toAbsolutePath().toString());
                this.generateSPIFFS(folderFullPath, tempDirWithPrefix.toAbsolutePath().toString() + "/" +"spiff_image.bin");
                this.uploadSPIFF(tempDirWithPrefix.toAbsolutePath().toString() + "/" +"spiff_image.bin");

            } catch (IOException e) {
                e.printStackTrace();
            }

    }

    @Override
    public void run() {
        preExecution();
        targetBuildExecution();
        postExecution();

    }

    public String getFolderFullPath() {
        return folderFullPath;
    }

    public void setFolderFullPath(String folderFullPath) {
        this.folderFullPath = folderFullPath;
    }
}
