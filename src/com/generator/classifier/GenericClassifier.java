package com.generator.classifier;

import java.nio.file.Path;

public abstract class GenericClassifier {

    public final String declarationClassifierTag = "_DECLARATION_CLASSIFIER_";
    public final String initClassifierTag = "_INIT_CLASSIFIER_";
    public final String inferenceTag = "_INFERENCE_";
    protected Path workingDirPath;


    public abstract String getDeclarationCode();
    public abstract String getInitCode();
    public abstract String getInferenceCode();
    public abstract Path getWorkingDirPath();
    public abstract int getnInput();

    protected String getExecutionPath() {
        String absolutePath = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
        absolutePath = absolutePath.substring(0, absolutePath.lastIndexOf("/"));
        absolutePath = absolutePath.replaceAll("%20", " ");
        //windows test
        //absolutePath = absolutePath.replaceAll(":", "");
        if(System.getProperty("os.name").startsWith("Windows"))
        {
            absolutePath = absolutePath.substring(1);
        }
        return absolutePath;
    }
}
