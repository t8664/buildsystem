package com.generator.classifier;

import com.util.CopyDir;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class NNClassifier extends GenericClassifier {

    private String declarationCode;
    private String initCode;
    private String inferenceCode;

    private String cName;
    private String templateDirPath;


    private String weightsFilePath;

    private int nInput;
    private int nHidden;
    private int nOutput;

    private double[][] inputWeights;		// Pesi tra input-hidden
    private double[][] outputWeights;		// Pesi tra hidden-output

    private double[] inputBias;				// Bias tra input-hidden
    private double[] outputBias;			// Bias tra hidden-output

    private boolean needUpdate;

    public NNClassifier() {
        weightsFilePath = null;
        try {
            workingDirPath =  Files.createTempDirectory("classifierSource");
            //System.out.println("CLASSIFIER WD: "+ workingDirPath.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        cName = "nn_classifier";
        templateDirPath = getExecutionPath() + "/com/builder/ToolChain/Template/classifier/" + cName;
        setDefaultConfiguration();
        needUpdate=false;
    }

    public NNClassifier(String weightsFilePath)
    {
        this();
        setWeightsFilePath(weightsFilePath);
        needUpdate = true;

    }

    public void setDefaultConfiguration(){
        //Default configuration
        nInput = 8;
        nHidden  = 16;
        nOutput = 6;

        try {

            Files.walkFileTree(Paths.get(templateDirPath), new CopyDir(Paths.get(templateDirPath), workingDirPath, true));
        } catch (IOException e) {
            e.printStackTrace();
        }

        //configurazione pesi e spostamento file in una cartella di lavoro?
        setCodes(nInput);


    }

    public void updateConfig(){
        if(!needUpdate) return;

        try {
            if (weightsFilePath == null)
                throw new Exception("Define weight file first.");

            String s = readFileToString(weightsFilePath);

            inputWeights = new double[nInput][nHidden];
            outputWeights = new double[nHidden][nOutput];

            inputBias = new double[nHidden];
            outputBias = new double[nOutput];

            loadNeuralNetworkFromString( s );


            generateWeightsFile(workingDirPath.toString()+"/weights.h");

            setCodes(nInput);

        }catch(Exception e)
        {
            e.printStackTrace();
        }


    }

    private void setCodes(int inputVectorSize){

        declarationCode = String.join("\n",
                "extern \"C\" {",
                "#include \"classifier/nn.h\"",
                "}",
                "neural_network my_nn;",
                "neural_network * nn = &(my_nn);",
                "nn_data input_vect["+inputVectorSize+"];"
        );

        initCode = String.join("\n",
                "init_neural_network(nn);"
        );

        inferenceCode = String.join("\n",
                "set_input(nn, &input_vect[0] );",
                "predict(nn);",
                "int found_class = compute_class_found(nn);"
        );
    }

    // Crea un oggetto rete neurale, con i pesi definiti nella stringa "s"
    private  void loadNeuralNetworkFromString(String s) {
        @SuppressWarnings("resource")
        Scanner scanner = new Scanner(s).useDelimiter(",");

        int input = Integer.parseInt(scanner.next());
        int hidden = Integer.parseInt(scanner.next());
        int output = Integer.parseInt(scanner.next());

        for (int i=0; i < input; i++) {
            for (int j=0; j < hidden; j++) {
                inputWeights[i][j] = Double.parseDouble(scanner.next());
            }
        }

        for (int i=0; i<hidden; i++) inputBias[i] = Double.parseDouble(scanner.next());

        for (int i=0; i < hidden; i++) {
            for (int j=0; j < output; j++) {
                outputWeights[i][j] = Double.parseDouble(scanner.next());
            }
        }

        for (int i=0; i<output; i++) outputBias[i] = Double.parseDouble(scanner.next());

        scanner.close();
    }


    // Copia il testo del file "filename" all'interno di una stringa
    public static String readFileToString(String filename) throws Exception {
        BufferedReader br = new BufferedReader(new FileReader(filename));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            return sb.toString();
        } finally {
            br.close();
        }
    }

    // Genera il file "weights.h" all'interno del percorso path
    public void generateWeightsFile(String path) {

        StringBuilder s = new StringBuilder();

        s.append("#ifndef _WEIGHTS_H_");
        s.append("\n");
        s.append("#define _WEIGHTS_H_");
        s.append("\n\n");

        s.append("#define INPUT_SIZE");
        s.append("\t");
        s.append(String.valueOf(this.nInput));
        s.append("\n");
        s.append("#define HIDDEN_SIZE");
        s.append("\t");
        s.append(String.valueOf(this.nHidden));
        s.append("\n");
        s.append("#define OUTPUT_SIZE");
        s.append("\t");
        s.append(String.valueOf(this.nOutput));

        s.append("\n\n");
        s.append("const double weights1[INPUT_SIZE][HIDDEN_SIZE] =  \n{");
        for(int i=0; i < this.nInput;i++) {
            s.append("{");
            for(int j=0; j < this.nHidden; j++) {
                s.append(String.valueOf(this.inputWeights[i][j]));
                if (j != (this.nHidden - 1)) s.append(",");
            }
            if (i != (this.nInput -1)) s.append("},\n");
        }
        s.append("}};");

        s.append("\n\n");
        s.append("const double weights2[HIDDEN_SIZE][OUTPUT_SIZE] =  \n{");
        for(int i=0; i < this.nHidden;i++) {
            s.append("{");
            for(int j=0; j < this.nOutput; j++) {
                s.append(String.valueOf(this.outputWeights[i][j]));
                if (j != (this.nOutput - 1)) s.append(",");
            }
            if (i != (this.nHidden -1)) s.append("},\n");
        }
        s.append("}};");

        s.append("\n\n");
        s.append("const double biases1[HIDDEN_SIZE] = \n{");
        for(int i=0; i<this.nHidden; i++) {
            s.append(String.valueOf(this.inputBias[i]));
            if (i != (this.nHidden - 1)) s.append(",");
        }
        s.append("};");

        s.append("\n\n");
        s.append("const double biases2[OUTPUT_SIZE] = \n{");
        for(int i=0; i<this.nOutput; i++) {
            s.append(String.valueOf(this.outputBias[i]));
            if (i != (this.nOutput - 1)) s.append(",");
        }
        s.append("};");

        s.append("\n\n");
        s.append("#endif");

        String string = s.toString();

        Writer writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(path), "utf-8"));
            writer.write(string);
        } catch (IOException ex) {
            System.out.println("Errore creazione file in " + path);
        } finally {
            try {writer.close();} catch (Exception ex) {/*ignore*/}
        }
    }

    @Override
    public String getDeclarationCode() {
        updateConfig();
        return declarationCode;
    }

    @Override
    public String getInitCode() {
        updateConfig();
        return initCode;
    }

    @Override
    public String getInferenceCode() {
        updateConfig();
        return inferenceCode;
    }

    @Override
    public Path getWorkingDirPath() {
        return workingDirPath;
    }


    // Getter/Setter

    @Override
    public int getnInput() {
        return nInput;
    }

    public void setnInput(int nInput) {
        needUpdate=true;
        this.nInput = nInput;
    }

    public int getnHidden() {
        return nHidden;
    }

    public void setnHidden(int nHidden) {
        needUpdate=true;
        this.nHidden = nHidden;
    }

    public int getnOutput() {
        return nOutput;
    }

    public void setnOutput(int nOutput) {
        needUpdate=true;
        this.nOutput = nOutput;
    }

    public String getTemplateDirPath() {
        return templateDirPath;
    }

    public void setTemplateDirPath(String templateDirPath) {
        this.templateDirPath = templateDirPath;
    }


    public String getWeightsFilePath() {
        return weightsFilePath;
    }

    public void setWeightsFilePath(String weightsFilePath) {
        this.weightsFilePath = weightsFilePath;
    }
}
