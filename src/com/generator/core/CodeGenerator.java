package com.generator.core;

import com.builder.target.GenericTarget;
import com.generator.classifier.GenericClassifier;
import com.generator.sensor.GenericSensor;
import com.util.CopyDir;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CodeGenerator {

    private final String resultTag="_RESULT_";
    private String TemplateSrcDir;
    private GenericClassifier classifier;
    private GenericSensor sensor;
    private Path workingDirPath;
    private String sourceMainFile;
    public GenericTarget target;

    public CodeGenerator(){
        classifier = null;
        sensor = null;
        sourceMainFile = "generic_classifier.ino";
        TemplateSrcDir = getExecutionPath() + "/com/builder/ToolChain/ProjectSource/generic_classifier/";
        try {
            workingDirPath =  Files.createTempDirectory("codeProject");
            target =  new GenericTarget("generic_classifier");
            target.setProjSrcDir(workingDirPath.toString());
            //workingDirPath = Paths.get(workingDirPath.toString(),"generic_classifier");

            //System.out.println("CLASSIFIER WD: "+ workingDirPath.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public CodeGenerator(GenericClassifier classifier, GenericSensor sensor) {
        this();
        this.classifier = classifier;
        this.sensor = sensor;
    }

    public CodeGenerator(String templateDir) {
        this();
        TemplateSrcDir = templateDir;
    }

    public void generateCode() throws Exception {
        if( classifier == null || sensor == null)
            throw new Exception("Configuration needed");

        try{
            workingDirPath = Paths.get(workingDirPath.toString(),"generic_classifier");

            //copia template in working dir
            Files.walkFileTree(Paths.get(TemplateSrcDir), new CopyDir(Paths.get(TemplateSrcDir), workingDirPath, true));

            //effettua modifiche relative al classificatore ed ai sensori
            String content;
            content = new String(Files.readAllBytes(Paths.get(workingDirPath.toString(),sourceMainFile)), StandardCharsets.UTF_8);

            //_DECLARATION_CLASSIFIER_
            content = content.replaceAll(classifier.declarationClassifierTag, classifier.getDeclarationCode());

            //_INIT_SENSOR_
            content = content.replaceAll(sensor.initSensorTag, sensor.getInitSensorCode());

            //_INIT_CLASSIFIER_
            content = content.replaceAll(classifier.initClassifierTag, classifier.getInitCode());

            //_MEASURE_ qui va generato il ciclo for
            content = content.replaceAll(sensor.measureTag, getMeasureLoopCode(String.valueOf(classifier.getnInput()),sensor.getMeasureCode()));

            //_INFERENCE_
            content = content.replaceAll(classifier.inferenceTag, classifier.getInferenceCode());

            //_RESULT_
            content = content.replaceAll(this.resultTag, this.getResultCode());


            //copia il codice del classificatore
            Files.walkFileTree(classifier.getWorkingDirPath(), new CopyDir(classifier.getWorkingDirPath(), Paths.get(workingDirPath.toString(), "classifier"), true));

            //salvo le modifiche
            Files.write(Paths.get(workingDirPath.toString(),sourceMainFile), content.getBytes(StandardCharsets.UTF_8));
            //nella workingdir ci sarà il sorgente pronto per la compilazione da passare al builder

        }catch(Exception e){
            e.printStackTrace();
        }




    }

    private String getMeasureLoopCode(String inputSize, String measureCode)
    {
        String code= String.join("\n",
        "for(int i =0; i<"+inputSize+"; i++){",
                measureCode,
                "input_vect[i] = outValues.outTEMP[0][0];",
                "}"
        );
        return code;
    }



    private String getResultCode(){
        return new String("Serial.print(\"Predicted Class: \");Serial.println(found_class);");
    }

    protected String getExecutionPath() {
        String absolutePath = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
        absolutePath = absolutePath.substring(0, absolutePath.lastIndexOf("/"));
        absolutePath = absolutePath.replaceAll("%20", " ");
        //windows test
        //absolutePath = absolutePath.replaceAll(":", "");
        if(System.getProperty("os.name").startsWith("Windows"))
        {
            absolutePath = absolutePath.substring(1);
        }
        return absolutePath;
    }

    // get/set

    public String getTemplateSrcDir() {
        return TemplateSrcDir;
    }

    public void setTemplateSrcDir(String templateSrcDir) {
        TemplateSrcDir = templateSrcDir;
    }

    public GenericClassifier getClassifier() {
        return classifier;
    }

    public void setClassifier(GenericClassifier classifier) {
        this.classifier = classifier;
    }

    public GenericSensor getSensor() {
        return sensor;
    }

    public void setSensor(GenericSensor sensor) {
        this.sensor = sensor;
    }

    public Path getWorkingDirPath() {
        return workingDirPath;
    }

    public void setWorkingDirPath(Path workingDirPath) {
        this.workingDirPath = workingDirPath;
    }

    public void setTarget(GenericTarget target) {
        this.target = target;
    }
}
