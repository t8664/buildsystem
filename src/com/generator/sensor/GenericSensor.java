package com.generator.sensor;

public abstract class GenericSensor {

    public final String initSensorTag = "_INIT_SENSOR_";
    public final String measureTag = "_MEASURE_";

    public abstract String getInitSensorCode();
    public abstract String getMeasureCode();

}
