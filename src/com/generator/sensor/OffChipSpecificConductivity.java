package com.generator.sensor;

public class OffChipSpecificConductivity  extends GenericSensor {

    private String initSensorCode;
    private String measureCode;


    public OffChipSpecificConductivity() {
        initSensorCode = String.join("\n",
                "instanceID = 0;",
                "strcpy(sensorName, \"OFFCHIP_SPECIFIC_CONDUCTIVITY\");",
                "strcpy(spParamSENSOR.sensorName, sensorName);",
                "SPSensingElementOnChip* seOnChip = spConfiguration.searchSPSensingElementOnChip(&spConfiguration, sensorName);",
                "SPSensingElement* sensingElement = (seOnChip->spSensingElementOnFamily->spSensingElement);",
                "strcpy(spParamSENSOR.port, seOnChip->spSensingElementOnFamily->port->portValue);",
                "strcpy(spParamSENSOR.filter, \"16\");",
                "spParamSENSOR.paramInternalEIS = NULL;",
                "qistatus[instanceID].status.dimBuffer = atoi(spParamSENSOR.filter);",
                "spMeasurementRUNX[instanceID].setSENSOR(&spMeasurementRUNX[instanceID], &spParamSENSOR, NULL);"
        );

        measureCode = String.join("\n",
                "spMeasurementRUNX[instanceID].getSENSOR(&spMeasurementRUNX[instanceID], outValues.outTEMP, &spParamSENSOR);"
        );
    }

    @Override
    public String getInitSensorCode() {
        return initSensorCode;
    }

    @Override
    public String getMeasureCode() {
        return measureCode;
    }
}
