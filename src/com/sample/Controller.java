package com.sample;

import com.builder.target.GenericTarget;
import com.builder.core.MakeConfigGenerator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

import java.io.*;
import java.util.Scanner;

public class Controller {

    @FXML
    private TextField textInCode;
    @FXML
    private TextArea stdOutErrExt;
    @FXML
    private VBox vBoxTextArea;

    private TextArea logTextArea = new TextArea() {
        @Override
        public void replaceText(int start, int end, String text) {
            super.replaceText(start, end, text);
            while(getText().split("\n", -1).length >= 1000) {
                int fle = getText().indexOf("\n");
                super.replaceText(0, fle+1, "");
            }
            positionCaret(getText().length());
        }
    };


    public void runBuildTask(ActionEvent actionEvent) throws IOException {
        logTextArea.setPrefHeight(750);
        logTextArea.setEditable(false);
        vBoxTextArea.getChildren().addAll(logTextArea);
        String s=null;
        textInCode.setText("prova");
        //ProcessBuilder pb = new ProcessBuilder("./slow_out.sh");
        ProcessBuilder pb = new ProcessBuilder("make", "all");
        //Map<String, String> env = pb.environment();
        //pb.directory(new File("/home/zen/Desktop"));
        pb.directory(new File("/home/zen/Documents/intellij-project/BuildSystem/src/Builder/RawCode/Release"));
        pb.redirectErrorStream(true);
        Process p = pb.start();


/*        BufferedReader stdInput = new BufferedReader(new
                InputStreamReader(p.getInputStream()));

        while ((s = stdInput.readLine()) != null) {
            System.out.println(s);
            stdOutErrExt.appendText(s);
            stdOutErrExt.appendText("\n");
        }*/

        Scanner in = new Scanner(p.getInputStream());
        new Thread(new Runnable() {
            public void run() {
                try {
                    while (p.isAlive()) {
                        while (in.hasNextLine()) {
                            String s = in.nextLine()+"\n";
                            //logTextArea.setText(logTextArea.getText()+s);
                            //stdOutErrExt.appendText(s);
                            System.out.println(s);
                        }
                    }
                }
                catch (NullPointerException e)
                {
                    System.out.print("NULL ERROR");
                }
            }
        }).start();

    }

    public void runUploadTask(ActionEvent actionEvent) throws IOException {

        MakeConfigGenerator config= new MakeConfigGenerator();
        config.makeConfig();
        //config.cleanConfig();
    }

    private static void inheritIO(final InputStream src, final PrintStream dest) {
        new Thread(new Runnable() {
            public void run() {
                Scanner sc = new Scanner(src);
                while (sc.hasNextLine()) {
                    dest.println(sc.nextLine());
                }
            }
        }).start();
    }

    public void runCleanTask(ActionEvent actionEvent) throws IOException {
        String s=null;
        textInCode.setText("clean");
        //ProcessBuilder pb = new ProcessBuilder("./slow_out.sh");
        ProcessBuilder pb = new ProcessBuilder("make", "clean");
        //Map<String, String> env = pb.environment();
        //pb.directory(new File("/home/zen/Desktop"));
        pb.directory(new File("/home/zen/Documents/intellij-project/BuildSystem/src/Builder/RawCode/Release"));
        pb.redirectErrorStream(true);
        pb.inheritIO();
        Process p = pb.start();
    }


    public void runPrintENVTask(ActionEvent actionEvent) throws IOException {

        MakeConfigGenerator conf= new MakeConfigGenerator();
        conf.printEnvOnConsole();

        System.out.println(" ### JAVA PROPERTIES ###");
        System.getProperties().list(System.out);
    }


    public void runTargetTask(ActionEvent actionEvent) throws IOException {

        GenericTarget testTarget = new GenericTarget("Test");

        Thread t = new Thread(testTarget, "TestTarget");

        t.start();
    }
}
