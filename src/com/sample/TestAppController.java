package com.sample;

import com.builder.target.GenericTarget;
import com.builder.target.UploadFirmwareTarget;
import com.builder.target.UploadSPIFFSTarget;
import com.generator.classifier.GenericClassifier;
import com.generator.classifier.NNClassifier;
import com.generator.core.CodeGenerator;
import com.generator.sensor.GenericSensor;
import com.generator.sensor.OffChipSpecificConductivity;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.util.ResourceBundle;
import jssc.SerialPortList;


public class TestAppController implements Initializable{

    //@FXML
    //private TextField consoleOutErrTextArea;
    //  <TextField id="consoleOutErrTextArea" layoutX="22.0" layoutY="254.0" prefHeight="122.0" prefWidth="561.0" />

    @FXML
    private TextArea consoleOutErrTextArea;
    @FXML
    private ChoiceBox selectTargetChoiceBox;
    @FXML
    private ChoiceBox portList;
    @FXML
    private Button selectFirmware;
    @FXML
    private Button selectPartitions;
    @FXML
    private Button runTaskButton;
    @FXML
    private Button upPorts;
    @FXML
    private Button selectSPIFFS;
    @FXML
    private ChoiceBox choiceBoxTarget;
    @FXML
    private Label targetLabel;

    private String firmwareFullPath;
    private String partitionsFullPath;
    private String folderFullPath;
    private String serialPort;
    private String target;


    public void initialize(URL url, ResourceBundle resources) {
        // Initialization code can go here.
        // The parameters url and resources can be omitted if they are not needed
        //

        OutputStream out = new OutputStream() {
            @Override
            public void write(int b) throws IOException {
                appendText(String.valueOf((char) b));
            }
        };
        System.setOut(new PrintStream(out, true));

        populatePorts();

        firmwareFullPath = null;
        partitionsFullPath = null;
        serialPort = null;
        folderFullPath = null;
        target = null;

        selectTargetChoiceBox.getItems().add("FullTestBuild");
        selectTargetChoiceBox.getItems().add("Update Firmware");
        selectTargetChoiceBox.getItems().add("Update SPIFFS");

        choiceBoxTarget.getItems().add("Test");
        choiceBoxTarget.getItems().add("iotmat_giova");
        choiceBoxTarget.getItems().add("iotmat_test");
        choiceBoxTarget.getItems().add("generic_classifier");

        selectFirmware.setVisible(false);
        selectPartitions.setVisible(false);
        runTaskButton.setVisible(false);
        portList.setVisible(false);
        upPorts.setVisible(false);
        selectSPIFFS.setVisible(false);
        choiceBoxTarget.setVisible(false);
        targetLabel.setVisible(false);

        //test env windows


        System.out.println("Init");

    }


    public void appendText(String str) {
        Platform.runLater(() -> consoleOutErrTextArea.appendText(str));
    }


    public void runTaskFunc(ActionEvent actionEvent) throws IOException {

        if(selectTargetChoiceBox.getValue().toString().equals("FullTestBuild"))
        {
            if(target == null ){
                System.out.println("Please select correct target!");
            }else {
                //GenericTarget testTarget = new GenericTarget("Test");
                GenericTarget testTarget = new GenericTarget(this.target);
                testTarget.boardConfig.setPort(this.serialPort);
                //testTarget.printEnvOnConsole();
                Thread t = new Thread(testTarget, "TestTarget");
                t.start();
            }
        }else if(selectTargetChoiceBox.getValue().toString().equals("Update Firmware")){

            if(firmwareFullPath == null || partitionsFullPath == null || serialPort == null)
            {
                System.out.println("Please select correct files!");
            }
            else {
                UploadFirmwareTarget uploadTarget = new UploadFirmwareTarget();
                uploadTarget.setFirmwareFullPath(this.firmwareFullPath);
                uploadTarget.setPartitionsFullPath(this.partitionsFullPath);
                uploadTarget.boardConfig.setPort(this.serialPort);
                Thread t = new Thread(uploadTarget, "UploadTarget");
                t.start();
            }
        }else if(selectTargetChoiceBox.getValue().toString().equals("Update SPIFFS"))
        {
            if(folderFullPath == null)
            {
                System.out.println("Please select correct files!");
            }else {
                UploadSPIFFSTarget spiffTarget = new UploadSPIFFSTarget();
                spiffTarget.setFolderFullPath(this.folderFullPath);
                spiffTarget.boardConfig.setPort(this.serialPort);
                Thread t = new Thread(spiffTarget, "UploadSPIFFTarget");
                t.start();
            }
        }
    }

    public void changeStatus(ActionEvent actionEvent) {

        if(selectTargetChoiceBox.getValue().toString().equals("FullTestBuild"))
        {
            runTaskButton.setVisible(true);
            selectFirmware.setVisible(false);
            selectPartitions.setVisible(false);
            portList.setVisible(true);
            upPorts.setVisible(true);
            selectSPIFFS.setVisible(false);
            choiceBoxTarget.setVisible(true);
            targetLabel.setVisible(true);





        }else if(selectTargetChoiceBox.getValue().toString().equals("Update Firmware")){

            selectFirmware.setVisible(true);
            selectPartitions.setVisible(true);
            runTaskButton.setVisible(true);
            portList.setVisible(true);
            upPorts.setVisible(true);
            selectSPIFFS.setVisible(false);
            choiceBoxTarget.setVisible(false);
            targetLabel.setVisible(false);



        }else if(selectTargetChoiceBox.getValue().toString().equals("Update SPIFFS")){

            selectSPIFFS.setVisible(true);
            runTaskButton.setVisible(true);
            portList.setVisible(true);
            upPorts.setVisible(true);
            selectFirmware.setVisible(false);
            selectPartitions.setVisible(false);
            choiceBoxTarget.setVisible(false);
            targetLabel.setVisible(false);

        }
        else{
            selectFirmware.setVisible(false);
            selectPartitions.setVisible(false);
            runTaskButton.setVisible(false);
            portList.setVisible(false);
            upPorts.setVisible(false);
            selectSPIFFS.setVisible(false);
            choiceBoxTarget.setVisible(false);
            targetLabel.setVisible(false);



        }
    }

    public void selectPartitionsFunc(ActionEvent actionEvent) {
        
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Partitions File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Bin Files", "*.bin")
        );

        File selectedFile = fileChooser.showOpenDialog(selectPartitions.getScene().getWindow());
        if (selectedFile != null) {
           // mainStage.display(selectedFile);
            partitionsFullPath = selectedFile.getAbsolutePath().toString();
            System.out.println("Selected partitions file: \n\t" + partitionsFullPath);

        }

    }

    public void selectFirmwareFunc(ActionEvent actionEvent) {

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Firmware File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Bin Files", "*.bin")
        );

        File selectedFile = fileChooser.showOpenDialog(selectFirmware.getScene().getWindow());
        if (selectedFile != null) {
            // mainStage.display(selectedFile);
            firmwareFullPath = selectedFile.getAbsolutePath().toString();
            System.out.println("Selected firmware file: \n\t" + firmwareFullPath);

        }
    }

    public void changePort(ActionEvent actionEvent) {
        this.serialPort = portList.getValue().toString();
        System.out.println("Changed port : \n\t" + serialPort);
    }

    public void uPortsFunc(ActionEvent actionEvent) {
        populatePorts();
    }

    private void populatePorts(){
        String[] portNames = SerialPortList.getPortNames();
        portList.getItems().clear();
        for(int i = 0; i < portNames.length; i++){
            portList.getItems().add(portNames[i]);
        }
    }

    public void selectSPIFFSFunc(ActionEvent actionEvent) {
        DirectoryChooser dirChooser = new DirectoryChooser();
        dirChooser.setTitle("Open SPIFFS Folder");

        File selectedFile = dirChooser.showDialog(selectFirmware.getScene().getWindow());
        if (selectedFile != null) {
            // mainStage.display(selectedFile);
            folderFullPath = selectedFile.getAbsolutePath().toString();
            System.out.println("Selected folder: \n\t" + folderFullPath);

        }
    }

    public void changeTarget(ActionEvent actionEvent) {
        this.target = choiceBoxTarget.getValue().toString();
        System.out.println("Selected Target : \n\t" + this.target);
    }

    public void presetNNFunc(ActionEvent actionEvent) {
        GenericClassifier c = new NNClassifier("/home/zen/Desktop/TESI_LM/neuralnetworks_sensichips/nn_gen/java/weights.txt");
        GenericSensor s = new OffChipSpecificConductivity();
        //GenericSensor s = new OffChipORP();
        //GenericSensor s = new OffChipPracticalSalinity();

        try {
            CodeGenerator generator= new CodeGenerator(c, s);
            GenericTarget  gt = new GenericTarget("generic_classifier");
            generator.setTarget(gt);
            generator.target.setProjSrcDir(generator.getWorkingDirPath().toString());
            generator.target.boardConfig.setPort(this.serialPort);
            generator.target.cleanConfig();
            generator.generateCode();
            Thread t = new Thread(generator.target, "TestTarget");
            t.start();
        } catch (Exception e) {
           e.printStackTrace();
        }

}
}

