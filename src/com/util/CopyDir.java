package com.util;


import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;


//https://docs.oracle.com/javase/tutorial/essential/io/walk.html

public class CopyDir extends SimpleFileVisitor<Path> {
    private Path sourceDir;
    private Path targetDir;
    private boolean overwriteFiles;

    public CopyDir(Path sourceDir, Path targetDir) {
        this.sourceDir = sourceDir;
        this.targetDir = targetDir;
        this.overwriteFiles = false;

    }

    public CopyDir(Path sourceDir, Path targetDir, boolean overwriteFiles) {
        this.sourceDir = sourceDir;
        this.targetDir = targetDir;
        this.overwriteFiles = overwriteFiles;
    }

    @Override
    public FileVisitResult visitFile(Path file,
                                     BasicFileAttributes attributes) {

        try {
            Path targetFile = targetDir.resolve(sourceDir.relativize(file));

            if(overwriteFiles){
                Files.copy(file, targetFile, StandardCopyOption.REPLACE_EXISTING);
            }else
                Files.copy(file, targetFile);

        } catch (IOException ex) {
            System.err.println(ex);
        }

        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir,
                                             BasicFileAttributes attributes) {
        try {
            Path newDir = targetDir.resolve(sourceDir.relativize(dir));
            Files.createDirectory(newDir);
        } catch (IOException ex) {
            System.err.println(ex);
        }

        return FileVisitResult.CONTINUE;
    }

}